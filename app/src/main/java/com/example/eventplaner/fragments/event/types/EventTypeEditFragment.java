package com.example.eventplaner.fragments.event.types;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.SubcategoryAdapter;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.dto.EventCategoryDto;
import com.example.eventplaner.model.dto.EventTypeDto;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventTypeEditFragment extends Fragment {
    private RecyclerView recyclerView;
    private SubcategoryAdapter adapter;
    private FirebaseFirestore db;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event_type_edit, container, false);
        db = FirebaseFirestore.getInstance();
        Bundle bundle = getArguments();
        EventTypeDto eventTypeDto = (EventTypeDto) bundle.getSerializable("eventType");

        recyclerView = view.findViewById(R.id.event_types_edit_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        EditText typeDescription = view.findViewById(R.id.event_type_edit_description);
        typeDescription.setText(eventTypeDto.getDescription());
        fetchSubcategories(eventTypeDto.getSuggestedSubcategories());
        Button confirmButton = view.findViewById(R.id.event_type_edit_button);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> eventSubcategoriesIds = new ArrayList<>();
                for (EventSubcategory subcategory:
                     adapter.getSelectedSubcategories()) {
                    eventSubcategoriesIds.add(subcategory.getId());
                }
                eventTypeDto.setSuggestedSubcategories(eventSubcategoriesIds);
                eventTypeDto.setDescription(typeDescription.getText().toString());
                updateEventType(eventTypeDto);
            }
        });

        return view;
    }

    private void fetchSubcategories(List<String> eventSubcategoriesIds) {
        db.collection("eventSubcategories")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<EventSubcategory> subcategories = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String subcategoryId = document.getId();
                            EventSubcategory subcategory = document.toObject(EventSubcategory.class);
                            subcategory.setId(subcategoryId);
                            subcategories.add(subcategory);
                        }

                        updateRecyclerView(subcategories, eventSubcategoriesIds);
                    } else {

                    }
                });

    }


    private void updateRecyclerView(List<EventSubcategory> subcategories, List<String> eventTypeSubcategoryIds) {
        if(!eventTypeSubcategoryIds.isEmpty()){
            db.collection("eventSubcategories")
                    .whereIn(FieldPath.documentId(), eventTypeSubcategoryIds)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            List<EventSubcategory> selectedSubcategories = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                String subcategoryId = document.getId();
                                EventSubcategory subcategory = document.toObject(EventSubcategory.class);
                                subcategory.setId(subcategoryId);
                                selectedSubcategories.add(subcategory);
                            }

                            // Pass selected subcategories here
                            adapter = new SubcategoryAdapter(subcategories);
                            adapter.setSelectedSubcategories(selectedSubcategories);
                            recyclerView.setAdapter(adapter);
                        }
                    });
        }
        else{
            List<EventSubcategory> selectedSubcategories = new ArrayList<>();
            adapter = new SubcategoryAdapter(subcategories);
            adapter.setSelectedSubcategories(selectedSubcategories);
            recyclerView.setAdapter(adapter);
        }



    }

    private void updateEventType(EventTypeDto eventTypeDto) {
        Map<String, Object> updates = new HashMap<>();
        updates.put("description", eventTypeDto.getDescription());
        updates.put("suggestedSubcategories", eventTypeDto.getSuggestedSubcategories());

        db.collection("eventTypes")
                .document(eventTypeDto.getId())
                .update(updates)
                .addOnSuccessListener(aVoid -> {
                    // Handle success
                })
                .addOnFailureListener(e -> {
                    // Handle failure
                });
    }


}