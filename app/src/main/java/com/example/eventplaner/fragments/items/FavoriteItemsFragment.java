package com.example.eventplaner.fragments.items;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.ItemViewAdapter;
import com.example.eventplaner.viewmodels.FavoriteItemViewModel;


public class FavoriteItemsFragment extends Fragment {

    private FavoriteItemViewModel viewModel;

    public FavoriteItemsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(FavoriteItemViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite_items, container, false);


        RecyclerView recyclerView = view.findViewById(R.id.favorite_items_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        viewModel.getData().observe(getViewLifecycleOwner(),data -> {
            recyclerView.setAdapter(new ItemViewAdapter(getContext(),viewModel.getData().getValue(),viewModel));
        });


        return view;
    }
}