package com.example.eventplaner.fragments.event.categories;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.eventplaner.R;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.SubcategoryType;
import com.example.eventplaner.model.dto.EventCategoryDto;
import com.example.eventplaner.model.dto.EventSubcategoryDto;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class EventSubcategoryCreationFragment extends Fragment {

    private FirebaseFirestore db;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event_subcategory_creation, container, false);
        db = FirebaseFirestore.getInstance();

        EditText subcategoryName = view.findViewById(R.id.subcategory_creation_name);
        EditText subcategoryDescription = view.findViewById(R.id.subcategory_creation_description);
        Spinner spinner = view.findViewById(R.id.subcategorie_spinner);
        String[] types = {"Usluga", "Proizvod"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, types);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });



        Bundle bundle = getArguments();
        EventCategoryDto eventCategoryDto = (EventCategoryDto) bundle.getSerializable("eventCategory");

        Button createButton = view.findViewById(R.id.subcategory_creation_button);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubcategoryType type;
                String selectedType = spinner.getSelectedItem().toString();
                if(selectedType.equals("Proizvod")){
                    type = SubcategoryType.PROIZVOD;
                }else {
                    type = SubcategoryType.USLUGA;
                }

                String subcategoryNameData = subcategoryName.getText().toString();
                String subcategoryDescriptionData = subcategoryDescription.getText().toString();

                if(!subcategoryNameData.equals("") && !subcategoryDescriptionData.equals(""))
                {
                    EventSubcategory eventSubcategory = new EventSubcategory();
                    eventSubcategory.setName(subcategoryNameData);
                    eventSubcategory.setDescription(subcategoryDescriptionData);
                    eventSubcategory.setCategoryId(eventCategoryDto.getId());
                    eventSubcategory.setType(type);
                    db.collection("eventSubcategories")
                            .add(eventSubcategory)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Bundle newBundle = new Bundle();
                                    newBundle.putSerializable("eventCategory", eventCategoryDto);
                                    Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                    EditEventCategoriesFragment nextFragment = new EditEventCategoriesFragment();
                                    nextFragment.setArguments(newBundle);
                                    FragmentManager fragmentManager = getParentFragmentManager();
                                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                                    transaction.replace(R.id.navigation_fragment_container, nextFragment);
                                    transaction.addToBackStack(null);
                                    transaction.commit();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error adding document", e);
                                }
                            });
                }
            }
        });

        return view;
    }
}