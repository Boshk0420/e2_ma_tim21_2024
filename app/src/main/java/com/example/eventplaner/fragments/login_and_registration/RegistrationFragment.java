package com.example.eventplaner.fragments.login_and_registration;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplaner.R;
import com.example.eventplaner.model.dto.UserRegistrationDto;
import com.example.eventplaner.model.users.User;
import com.example.eventplaner.model.users.UserRole;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class RegistrationFragment extends Fragment {

    Spinner spinner;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private static final int PICK_IMAGE_REQUEST = 1;
    private Uri profilePicture;
    private ImageView profileImageView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        if(mAuth.getCurrentUser() != null){
            mAuth.signOut();
        }


        Button nextButton = view.findViewById(R.id.registerButtonNext);
        spinner = view.findViewById(R.id.registerSpinerRole);
        String[] roles = {"Organizator dogadjaja", "Pruzalac usluga i proizvoda"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, roles);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        // Handle spinner item selection
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // Handle item selection
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Do nothing
            }
        });

        ImageButton pickImageButton = view.findViewById(R.id.register_action_imageButton);
        profileImageView = view.findViewById(R.id.imageViewProfile);
        pickImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImagePicker();
            }
        });


        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String selectedItem = spinner.getSelectedItem().toString();


                EditText firstName = view.findViewById(R.id.registerFirstName);
                EditText lastName = view.findViewById(R.id.registerLastName);
                EditText email = view.findViewById(R.id.registerEmail);
                EditText password = view.findViewById(R.id.registerPassword);
                EditText confirmPassword = view.findViewById(R.id.registerConfirmPassword);
                EditText phone = view.findViewById(R.id.registerPhone);
                EditText street = view.findViewById(R.id.registerStreet);
                String nameData = firstName.getText().toString();
                String lastNameData = lastName.getText().toString();
                String emailData = email.getText().toString();
                String passwordData = password.getText().toString();
                String confirmPasswordData = confirmPassword.getText().toString();
                String phoneData = phone.getText().toString();
                String streetData = street.getText().toString();

                LoginFragment nextFragment = new LoginFragment();






                if(nameData.isEmpty() || lastNameData.isEmpty() || emailData.isEmpty() || passwordData.isEmpty() || confirmPasswordData.isEmpty() || phoneData.isEmpty() || streetData.isEmpty() || !passwordData.equals(confirmPasswordData)){
                    Toast.makeText(getContext(), "Please fill all fields and make sure passwords match.",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                    FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    if (selectedItem.equals("Organizator dogadjaja")) {
                        registerOd(nameData, lastNameData, phoneData, streetData, emailData, passwordData, profilePicture);
                        transaction.replace(R.id.notLogedInLayout, nextFragment);
                    } else if (selectedItem.equals("Pruzalac usluga i proizvoda")) {

                        registerPup(transaction, nameData, lastNameData, passwordData, emailData, phoneData, streetData, profilePicture);
                    }

                    transaction.addToBackStack(null);
                    transaction.commit();
                }

            }
        });


        return view;
    }

    private void registerPup(FragmentTransaction transaction, String firstName, String lastName, String password, String email, String phone, String street, Uri profilePicture){
        Bundle bundle = new Bundle();

        UserRegistrationDto userRegistrationDto = new UserRegistrationDto(firstName, lastName, email, password, phone, street, profilePicture);

        // Put data into the Bundle
        bundle.putSerializable("userRegistration", userRegistrationDto);
        RegistrationCompanyInfoFragment companyInfoFragment = new RegistrationCompanyInfoFragment();

        companyInfoFragment.setArguments(bundle);
        transaction.replace(R.id.notLogedInLayout, companyInfoFragment);
    }


    private void registerOd(String name, String lastName, String phone, String street, String email, String password, Uri profilePicture){
        User newUser = new User(name, lastName, phone, street, UserRole.OD, profilePicture, null);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            user.sendEmailVerification()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d(TAG, "Email sent.");
                                            }
                                        }
                                    });
                            db.collection("users").document(mAuth.getCurrentUser().getUid())
                                    .set(newUser)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d(TAG, "User data added successfully");
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w(TAG, "Error adding user data", e);
                                        }
                                    });

                        } else {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(getContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }


    private void openImagePicker() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            profilePicture = data.getData();
            profileImageView.setBackgroundResource(0);
            profileImageView.setImageURI(profilePicture);
        }
    }
}