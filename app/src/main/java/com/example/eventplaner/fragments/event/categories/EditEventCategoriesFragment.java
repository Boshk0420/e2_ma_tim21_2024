package com.example.eventplaner.fragments.event.categories;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.EventSubcategoryHandlerAdapter;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.dto.EventCategoryDto;
import com.example.eventplaner.model.dto.EventSubcategoryDto;
import com.example.eventplaner.model.dto.UserRegistrationDto;
import com.example.eventplaner.utils.NotificationSender;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditEventCategoriesFragment extends Fragment {

    private RecyclerView recyclerView;
    private EventSubcategoryHandlerAdapter adapter;
    private List<EventSubcategory> subcategories;
    private FirebaseFirestore db;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_event_categories, container, false);
        db = FirebaseFirestore.getInstance();
        Bundle bundle = getArguments();
        EventCategoryDto eventCategoryDto = (EventCategoryDto) bundle.getSerializable("eventCategory");

        EditText editTextName = view.findViewById(R.id.edit_event_categorie_name);
        EditText editTextDescription = view.findViewById(R.id.edit_event_categorie_description);

        editTextName.setText(eventCategoryDto.getName());
        editTextDescription.setText(eventCategoryDto.getDescription());

        recyclerView = view.findViewById(R.id.subcategories_handler_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        fetchSubcategories(eventCategoryDto.getId());

        FloatingActionButton createNewSubcategoryButton = view.findViewById(R.id.create_new_subcategory_floating);
        createNewSubcategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle newBundle = new Bundle();
                newBundle.putSerializable("eventCategory", eventCategoryDto);
                EventSubcategoryCreationFragment nextFragment = new EventSubcategoryCreationFragment();
                nextFragment.setArguments(newBundle);
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.navigation_fragment_container, nextFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        Button confirmButton = view.findViewById(R.id.editCategoryConfirmButton);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationSender notificationSender = new NotificationSender();
                notificationSender.sendNotificationToPupv("Event categorie has been updated. Category " + eventCategoryDto.getName() + " is now called " + editTextName.getText().toString() + " and description is " + editTextDescription.getText().toString(), "Category updated");
                eventCategoryDto.setName(editTextName.getText().toString());
                eventCategoryDto.setDescription(editTextDescription.getText().toString());
                updateEvent(eventCategoryDto);
            }
        });



        return view;
    }

    private void fetchSubcategories(String categoryId) {
        db.collection("eventSubcategories")
                .whereEqualTo("categoryId", categoryId)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<EventSubcategory> subcategories = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String subcategoryId = document.getId();
                            EventSubcategory subcategory = document.toObject(EventSubcategory.class);
                            subcategory.setId(subcategoryId);
                            subcategories.add(subcategory);
                        }

                        updateRecyclerView(subcategories);
                    } else {

                    }
                });
    }
    private void updateRecyclerView(List<EventSubcategory> subcategories) {
        adapter = new EventSubcategoryHandlerAdapter(subcategories);
        recyclerView.setAdapter(adapter);
    }

    private void updateEvent(EventCategoryDto eventCategoryDto) {
        Map<String, Object> updates = new HashMap<>();
        updates.put("name", eventCategoryDto.getName());
        updates.put("description", eventCategoryDto.getDescription());

        db.collection("eventCategories")
                .document(eventCategoryDto.getId())
                .update(updates)
                .addOnSuccessListener(aVoid -> {
                    // Handle success
                })
                .addOnFailureListener(e -> {
                    // Handle failure
                });
    }


}