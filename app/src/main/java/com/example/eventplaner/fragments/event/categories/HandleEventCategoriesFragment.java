package com.example.eventplaner.fragments.event.categories;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.EventCategoryHandlerAdapter;
import com.example.eventplaner.adapters.EventCategoryListAdapter;
import com.example.eventplaner.fragments.login_and_registration.RegistrationCompanyScheduleFragment;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventSubcategory;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;


public class HandleEventCategoriesFragment extends Fragment {
    private RecyclerView recyclerView;
    private EventCategoryHandlerAdapter adapter;
    private List<EventCategory> categories;
    private FirebaseFirestore db;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_handle_event_categories, container, false);
        db = FirebaseFirestore.getInstance();
        recyclerView = view.findViewById(R.id.categories_handler_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        fetchEventCategories();

        FloatingActionButton createNewCategoryButton = view.findViewById(R.id.categories_handler_floating_button);
        FloatingActionButton checkSugestionsButton = view.findViewById(R.id.subcategories_sugestion_button);
        createNewCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventCategorieCreationFragment nextFragment = new EventCategorieCreationFragment();
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.navigation_fragment_container, nextFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        checkSugestionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubcategorySugestionsFragment nextFragment = new SubcategorySugestionsFragment();
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.navigation_fragment_container, nextFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        return view;
    }



    private void fetchEventCategories() {
        db.collection("eventCategories")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<EventCategory> eventCategories = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String categoryId = document.getId();
                            EventCategory category = document.toObject(EventCategory.class);
                            category.setId(categoryId);
                            eventCategories.add(category);
                        }
                        // Populate RecyclerView with fetched data
                        adapter = new EventCategoryHandlerAdapter(eventCategories);
                        recyclerView.setAdapter(adapter);
                    } else {
                        // Log error or handle failure
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }
}