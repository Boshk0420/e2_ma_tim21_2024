package com.example.eventplaner.fragments.login_and_registration;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.eventplaner.R;
import com.example.eventplaner.fragments.event.categories.SubcategorySugestionsFragment;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventType;
import com.example.eventplaner.model.company.Company;
import com.example.eventplaner.model.dto.CompanyProfileRegistrationDto;
import com.example.eventplaner.model.dto.UserRegistrationDto;
import com.example.eventplaner.model.users.User;
import com.example.eventplaner.model.users.UserRole;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class RegistrationRequestDetails extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    private static FirebaseAuth mAuth;
    private CompanyProfileRegistrationDto registrationInfo;
    private FirebaseFirestore db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_registration_request_details, container, false);
        Bundle bundle = getArguments();
        registrationInfo = new CompanyProfileRegistrationDto();
        registrationInfo= (CompanyProfileRegistrationDto) bundle.getSerializable("registrationInfo");
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        TextView firstName = view.findViewById(R.id.text_first_name);
        TextView lastName = view.findViewById(R.id.text_last_name);
        TextView email = view.findViewById(R.id.text_email);
        TextView address = view.findViewById(R.id.text_address);
        TextView phone = view.findViewById(R.id.text_phone);
        TextView companyName = view.findViewById(R.id.text_company_name);
        TextView companyDescription = view.findViewById(R.id.text_company_description);
        TextView companyEmail = view.findViewById(R.id.text_company_email);
        TextView companyAddress = view.findViewById(R.id.text_company_address);

        ChipGroup chipTypes = view.findViewById(R.id.chipGroupTypes);
        ChipGroup chipCategories = view.findViewById(R.id.chipGroupCategories);

        Button approveButton = view.findViewById(R.id.confirm_button);
        Button denyButton = view.findViewById(R.id.deny_button);

        firstName.setText(registrationInfo.getFirstName());
        lastName.setText(registrationInfo.getLastName());
        email.setText(registrationInfo.getEmail());
        address.setText(registrationInfo.getAddress());
        phone.setText(registrationInfo.getPhone());
        companyName.setText(registrationInfo.getCompanyName());
        companyDescription.setText(registrationInfo.getCompanyDescription());
        companyEmail.setText(registrationInfo.getCompanyEmail());
        companyAddress.setText(registrationInfo.getCompanyAddress());

        createCategoryChips(chipCategories, registrationInfo.getCategories());
        createTypeChips(chipTypes, registrationInfo.getTypes());



        approveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Company company = new Company(registrationInfo.getCompanyEmail(), registrationInfo.getCompanyDescription(), registrationInfo.getCompanyName(), registrationInfo.getPhone(), registrationInfo.getCompanyAddress(), null);
                User newUser = new User(registrationInfo.getFirstName(), registrationInfo.getLastName(), registrationInfo.getPhone(), registrationInfo.getAddress(), UserRole.PUP, null, company);
                mAuth.createUserWithEmailAndPassword(registrationInfo.getEmail(), registrationInfo.getPassword())
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Log.d(TAG, "createUserWithEmail:success");
                                    FirebaseUser user = task.getResult().getUser();
                                    user.sendEmailVerification()
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        deleteRegistrationRequest(registrationInfo.getId());
                                                        Log.d(TAG, "Email sent.");
                                                    }
                                                }
                                            });
                                    db.collection("users").document(user.getUid())
                                            .set(newUser)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Log.d(TAG, "User data added successfully");
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.w(TAG, "Error adding user data", e);
                                                }
                                            });

                                } else {
                                    Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                }
                            }
                        });
            }
        });

        denyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteRegistrationRequest(registrationInfo.getId());
            }
        });

        return view;
    }


    private void createCategoryChips(ChipGroup chipGroup, List<EventCategory> categories){
        for(EventCategory eventCategorie : categories)
        {
            Chip chip = new Chip(getContext());
            chip.setText(eventCategorie.getName());
            chip.setClickable(false);
            chip.setCheckable(false);
            chipGroup.addView(chip);
        }
    }
    private void createTypeChips(ChipGroup chipGroup, List<EventType> types){
        for(EventType eventType : types)
        {
            Chip chip = new Chip(getContext());
            chip.setText(eventType.getName());
            chip.setClickable(false);
            chip.setCheckable(false);
            chipGroup.addView(chip);
        }
    }
    private void deleteRegistrationRequest(String requestId){
        db.collection("companyRegistrationRequests").document(requestId)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "Registration request deleted successfully");
                        ConfirmCompanyRegistration nextFragment = new ConfirmCompanyRegistration();
                        FragmentManager fragmentManager = getParentFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.navigation_fragment_container, nextFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error deleting registration request", e);
                    }
                });
    }
}