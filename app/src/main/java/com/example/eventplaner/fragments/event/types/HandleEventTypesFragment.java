package com.example.eventplaner.fragments.event.types;

import static android.content.ContentValues.TAG;

import android.media.metrics.Event;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.EventCategoryHandlerAdapter;
import com.example.eventplaner.adapters.EventTypeHandlerAdapter;
import com.example.eventplaner.fragments.event.categories.EventCategorieCreationFragment;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventType;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public class HandleEventTypesFragment extends Fragment {

    private RecyclerView recyclerView;
    private EventTypeHandlerAdapter adapter;
    private FirebaseFirestore db;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_handle_event_types, container, false);
        db = FirebaseFirestore.getInstance();
        recyclerView = view.findViewById(R.id.event_types_handler_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        fetchEventTypes();

        FloatingActionButton createNewCategoryButton = view.findViewById(R.id.event_types_handler_floating_button);
        createNewCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventTypeCreationFragment nextFragment = new EventTypeCreationFragment();
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.navigation_fragment_container, nextFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        return view;
    }


    private void fetchEventTypes() {
        db.collection("eventTypes")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<EventType> eventCategories = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String typeId = document.getId();
                            EventType type = document.toObject(EventType.class);
                            type.setId(typeId);
                            eventCategories.add(type);
                        }
                        // Populate RecyclerView with fetched data
                        adapter = new EventTypeHandlerAdapter(eventCategories);
                        recyclerView.setAdapter(adapter);
                    } else {
                        // Log error or handle failure
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }


}