package com.example.eventplaner.fragments.commenting;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.eventplaner.CommentsAdapter;
import com.example.eventplaner.R;
import com.example.eventplaner.model.Comment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class CommentsFragmentWithFilters extends Fragment {

    private RecyclerView commentsRecyclerView;
    private CommentsAdapter commentsAdapter;
    private Button selectDateButton;
    private Spinner selectRatingSpinner;
    private String filterRating;
    private List<Comment> allComments;
    private FirebaseFirestore firestore;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_comments_company, container, false);

        firestore = FirebaseFirestore.getInstance();

        commentsRecyclerView = view.findViewById(R.id.commentsRecyclerView);
        selectDateButton = view.findViewById(R.id.selectDateButton);
        selectRatingSpinner = view.findViewById(R.id.ratingFilterSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(requireContext(),
                R.array.rating_filter_options, android.R.layout.simple_spinner_item);

// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

// Apply the adapter to the spinner
        selectRatingSpinner.setAdapter(adapter);

// Set an OnItemSelectedListener to listen for item selection
        selectRatingSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item from the spinner
                filterRating = parent.getItemAtPosition(position).toString();
                if(!filterRating.equals("All"))
                    filterCommentsByRating(Integer.parseInt(filterRating));
                else
                    fetchComments();
                // Do something with the selected item
                Log.d("Selected Item", filterRating);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case where nothing is selected
            }
        });

        allComments = new ArrayList<>();  // Initialize comments list
        commentsAdapter = new CommentsAdapter(getActivity(), new ArrayList<>(allComments));
        commentsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        commentsRecyclerView.setAdapter(commentsAdapter);

        fetchComments(); // Fetch comments from Firestore

        selectDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        return view;
    }

    private void fetchComments() {
        firestore.collection("comments")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            allComments.clear();
                            for (DocumentSnapshot document : task.getResult()) {
                                Comment comment = document.toObject(Comment.class);
                                allComments.add(comment);
                            }
                            commentsAdapter.setComments(allComments);
                        } else {
                            // Handle errors
                        }
                    }
                });
    }

    private void showDatePickerDialog() {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        // Handle the selected date
                        calendar.set(year, month, dayOfMonth);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        String selectedDate = dateFormat.format(calendar.getTime());
                        filterCommentsByDate(selectedDate);
                    }
                }, year, month, day);
        datePickerDialog.show();
    }

    private void filterCommentsByDate(String selectedDate) {
        List<Comment> filteredComments = new ArrayList<>(allComments);

        // Filter by selected date
        filteredComments.removeIf(comment -> !comment.getDate().equals(selectedDate));

        commentsAdapter.setComments(filteredComments);
    }
    private void filterCommentsByRating(int rating) {
        List<Comment> filteredComments = new ArrayList<>(allComments);

        // Filter by selected date
        filteredComments.removeIf(comment -> comment.getRating()!=rating);

        commentsAdapter.setComments(filteredComments);
    }
}
