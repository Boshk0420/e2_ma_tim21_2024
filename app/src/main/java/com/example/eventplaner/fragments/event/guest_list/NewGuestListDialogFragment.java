package com.example.eventplaner.fragments.event.guest_list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.DialogFragment;

import com.example.eventplaner.R;

public class NewGuestListDialogFragment extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_dialog_display, container, false);

        getChildFragmentManager().beginTransaction()
                .replace(R.id.layoutDialog, new NewGuestListFragment())
                .commit();

        return view;
    }
}
