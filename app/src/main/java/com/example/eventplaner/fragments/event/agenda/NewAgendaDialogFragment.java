package com.example.eventplaner.fragments.event.agenda;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplaner.R;


public class NewAgendaDialogFragment extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_dialog_display, container, false);

        getChildFragmentManager().beginTransaction()
                .replace(R.id.layoutDialog, new NewAgendaFragment())
                .commit();

        return view;
    }

}