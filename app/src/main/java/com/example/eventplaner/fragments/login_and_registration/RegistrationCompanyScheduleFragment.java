package com.example.eventplaner.fragments.login_and_registration;

import static android.content.ContentValues.TAG;

import android.app.Notification;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplaner.R;
import com.example.eventplaner.clients.FirebaseUtils;
import com.example.eventplaner.clients.NotificationService;
import com.example.eventplaner.model.company.Days;
import com.example.eventplaner.model.company.WorkingHours;
import com.example.eventplaner.model.dto.CompanyProfileRegistrationDto;
import com.example.eventplaner.model.dto.CompnyRegistrationDto;
import com.example.eventplaner.model.dto.NotificationDto;
import com.example.eventplaner.model.dto.NotificationInfoDto;
import com.example.eventplaner.model.dto.UserRegistrationDto;
import com.example.eventplaner.utils.NotificationSender;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;


import org.checkerframework.checker.units.qual.N;

import java.io.Console;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegistrationCompanyScheduleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegistrationCompanyScheduleFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    private LinearLayout parentLayout;
    private List<WorkingHours> workingHours;
    private FirebaseFirestore db;
    private NotificationSender notificationSender;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_registration_company_schedule, container, false);
        db = FirebaseFirestore.getInstance();
        workingHours = new ArrayList<>();
        notificationSender = new NotificationSender();
        parentLayout = rootView.findViewById(R.id.parent_layout);

        // Days of the week
        Days[] daysOfWeek = {Days.MONDAY, Days.TUESDAY, Days.WEDNESDAY, Days.THURSDAY, Days.FRIDAY, Days.SATURDAY, Days.FRIDAY};

        for (Days day : daysOfWeek) {
            addCardView(day);
        }

        Button finishRegistrationButton = rootView.findViewById(R.id.button_finish_registration);

        finishRegistrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = getArguments();
                UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
                CompnyRegistrationDto companyRegistrationDto = new CompnyRegistrationDto();

                if (bundle != null) {
                    userRegistrationDto= (UserRegistrationDto) bundle.getSerializable("userRegistration");
                    companyRegistrationDto = (CompnyRegistrationDto) bundle.getSerializable("companyRegistration");
                }
                companyRegistrationDto.setWorkingHours(workingHours);

                if(!isWorkingHoursCorrect()){
                    Toast.makeText(getContext(), "Enter valid working hours.",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                    CompanyProfileRegistrationDto companyProfileRegistrationDto = new CompanyProfileRegistrationDto("",userRegistrationDto.getFirstName(), userRegistrationDto.getLastName(), userRegistrationDto.getEmail(), userRegistrationDto.getPassword(), userRegistrationDto.getStreet(), userRegistrationDto.getPhone(), companyRegistrationDto.getName(), companyRegistrationDto.getDescription(), companyRegistrationDto.getEmail(), companyRegistrationDto.getPhone(), companyRegistrationDto.getStreet(), companyRegistrationDto.getEventCategories(), companyRegistrationDto.getEventTypes(), companyRegistrationDto.getWorkingHours(), new Date());

                    db.collection("companyRegistrationRequests")
                            .add(companyProfileRegistrationDto)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    notificationSender.sendNotificationToAdmins("There is a new company registration request, check it out. " + companyProfileRegistrationDto.getFirstName() + " wants to registrate new company called " + companyProfileRegistrationDto.getCompanyName() + ". For more information checkout registration request screen.", "Company registration request");
                                    Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error adding document", e);
                                }
                            });


                    LoginFragment nextFragment = new LoginFragment();
                    FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.notLogedInLayout, nextFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }


            }
        });

        return rootView;
    }

    private void addCardView(Days day) {
        LayoutInflater inflater = LayoutInflater.from(requireContext());
        View cardViewLayout = inflater.inflate(R.layout.schedule_card, parentLayout, false);
        WorkingHours newWorkingHours = new WorkingHours();
        EditText editTextFrom = cardViewLayout.findViewById(R.id.editText_from);
        EditText editTextTo = cardViewLayout.findViewById(R.id.editText_to);
        CheckBox checkboxDay = cardViewLayout.findViewById(R.id.checkbox_day);
        checkboxDay.setText(day.toString());
        checkboxDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editTextFrom.setEnabled(true);
                    editTextTo.setEnabled(true);
                    newWorkingHours.setDay(day);
                    newWorkingHours.setStartHour(0);
                    newWorkingHours.setStartMinute(0);
                    newWorkingHours.setEndHour(0);
                    newWorkingHours.setEndMinute(0);

                    workingHours.add(newWorkingHours);

                } else {
                    editTextFrom.setEnabled(false);
                    editTextTo.setEnabled(false);
                    for (WorkingHours workingHour:
                         workingHours) {
                        if(workingHour.getDay().equals(day)){
                            workingHours.remove(workingHour);
                        }
                    }
                }
            }
        });

        editTextFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker(editTextFrom, day, true);
            }
        });

        editTextTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker(editTextTo, day, false);
            }
        });

        parentLayout.addView(cardViewLayout);
    }

    private void showTimePicker(final EditText editText, Days day, Boolean isStartHours) {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {

                        String time = String.format("%02d:%02d", hourOfDay, minute);
                        editText.setText(time);
                        for (WorkingHours workingHour:
                             workingHours) {
                            if(workingHour.getDay().equals(day))
                            {
                                if (isStartHours)
                                {
                                    workingHour.setStartHour(hourOfDay);
                                    workingHour.setStartMinute(minute);
                                }
                                else {
                                    workingHour.setEndHour(hourOfDay);
                                    workingHour.setEndMinute(minute);
                                }
                            }
                        }
                    }
                }, hour, minute, true);
        timePickerDialog.show();
    }

    private boolean isWorkingHoursCorrect(){
        if(workingHours.isEmpty())
        {
            return false;
        }
        for (WorkingHours workingHour:
             workingHours) {
            if(workingHour.getStartHour() > workingHour.getEndHour()){
                return false;
            }
            if(workingHour.getStartHour() == workingHour.getEndHour() && workingHour.getStartMinute() >= workingHour.getEndMinute()){
                return false;
            }

        }
        return true;
    }
}