package com.example.eventplaner.fragments.budget_planer;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.Budget.BudgetCategoryAdapter;
import com.example.eventplaner.adapters.Budget.BudgetSpentAdapter;
import com.example.eventplaner.model.Budget;
import com.example.eventplaner.model.BudgetCategory;
import com.example.eventplaner.model.BudgetSubCategory;
import com.example.eventplaner.viewmodels.BudgetViewModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BudgetSpentFragment extends Fragment{


    private BudgetViewModel viewModel;
    private Budget budget;

    private NewItemDialogFragment dialogFragment;

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    public BudgetSpentFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(BudgetViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        View view = inflater.inflate(R.layout.fragment_budget_spent, container, false);
        ExpandableListView expandableListView = view.findViewById(R.id.expandableListViewBudgetSpent);

        viewModel.getData().observe(getViewLifecycleOwner(), newBudget -> {
            budget = newBudget;
            expandableListView.setAdapter(new BudgetSpentAdapter(budget.getSpentBudget()));
        });

        FloatingActionButton fabButton = view.findViewById(R.id.fab_add_item);


        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showItemCreationDialog();
            }
        });



        return view;
    }

    private void showItemCreationDialog() {
        dialogFragment = new NewItemDialogFragment();
        dialogFragment.show(getParentFragmentManager(), "ItemCreationDialog");
    }


}