package com.example.eventplaner.fragments.login_and_registration;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eventplaner.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegistrationPhotoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegistrationPhotoFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration_photo, container, false);

        // Retrieve data from arguments
        Bundle bundle = getArguments();
        if (bundle != null) {
            String nameData = bundle.getString("nameData");
            String lastNameData = bundle.getString("lastNameData");
            String phoneData = bundle.getString("phoneData");
            String streetData = bundle.getString("streetData");

            TextView textName = view.findViewById(R.id.textName);
            TextView textLastName = view.findViewById(R.id.textLastName);
            TextView textPhone = view.findViewById(R.id.textPhone);
            TextView textStreet = view.findViewById(R.id.textStreet);

            textName.setText(nameData);
            textLastName.setText(lastNameData);
            textPhone.setText(phoneData);
            textStreet.setText(streetData);
        }

        return view;
    }
}