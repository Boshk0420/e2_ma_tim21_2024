package com.example.eventplaner.fragments.items;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.ItemViewAdapter;
import com.example.eventplaner.fragments.event.categories.SubcategorySugestionsFragment;
import com.example.eventplaner.fragments.reservations.BuyProductFragment;
import com.example.eventplaner.fragments.reservations.ReserveBundleFragment;
import com.example.eventplaner.fragments.reservations.ReserveServiceFragment;
import com.example.eventplaner.model.Service;
import com.example.eventplaner.model.mocked.BundleMock;
import com.example.eventplaner.model.mocked.ItemMock;
import com.example.eventplaner.model.mocked.ProductMock;
import com.example.eventplaner.model.mocked.ServiceMocked;
import com.example.eventplaner.viewmodels.BudgetViewModel;
import com.example.eventplaner.viewmodels.FavoriteItemViewModel;
import com.google.android.material.chip.Chip;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ItemDetailsFragment extends Fragment {

    ProductMock product;
    ServiceMocked service;

    FirebaseUser currentUser;
    FirebaseAuth mAuth;

    FavoriteItemViewModel viewModel;

    BundleMock bundle;

    public ItemDetailsFragment(ProductMock product, ServiceMocked service, BundleMock bundle) {
            this.service = service;
            this.product = product;
            this.bundle = bundle;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(FavoriteItemViewModel.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_item_details, container, false);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference userRef = db.collection("users").document(currentUser.getUid());

        Button sendMessageButton = view.findViewById(R.id.send_message_details_button);
        ListenerRegistration registration = userRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    return;
                }

                if (documentSnapshot != null && documentSnapshot.exists()) {
                    String role = documentSnapshot.getString("role");

                    if(role != null && !role.equals("OD")){
                        sendMessageButton.setVisibility(View.GONE);
                    }


                }
            }
        });

        ImageView image = view.findViewById(R.id.item_details_image);
        TextView name = view.findViewById(R.id.item_details_name);
        TextView desc = view.findViewById(R.id.item_details_description);
        TextView category = view.findViewById(R.id.item_details_category);
        TextView subcategory = view.findViewById(R.id.item_details_subcategory);
        Chip typeChip = view.findViewById(R.id.item_details_chip);
        TextView price = view.findViewById(R.id.item_details_price);
        TextView discount = view.findViewById(R.id.item_details_price_dicounted);
        View crossedPrice = view.findViewById(R.id.item_details_price_cross);


        TextView specifics = view.findViewById(R.id.service_details_specifics);
        TextView location = view.findViewById(R.id.service_details_location);
        TextView deadlineCancel = view.findViewById(R.id.service_details_deadline_cancel);
        TextView deadlineReservation = view.findViewById(R.id.service_details_deadline_reservation);

        RecyclerView recyclerView = view.findViewById(R.id.bundle_recycler_view);

        CheckBox fav = view.findViewById(R.id.fav_checkbox_details);


        if(service == null) sendMessageButton.setVisibility(View.GONE);


        fav.setChecked(
                (product != null && viewModel.getData().getValue().stream().anyMatch(x -> x.getName().equals(product.getName())) ) ||
                (service != null && viewModel.getData().getValue().stream().anyMatch(x -> x.getName().equals(service.getName())) ) ||
                (bundle != null && viewModel.getData().getValue().stream().anyMatch(x -> x.getName().equals(bundle.getName())) )
        );
        fav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    if(product != null) viewModel.addItem(product);
                    if(service != null) viewModel.addItem(service);
                    if(bundle != null) viewModel.addItem(bundle);
                }else {
                    if(product != null) viewModel.removeItem(product);
                    if(service != null) viewModel.removeItem(service);
                    if(bundle != null) viewModel.removeItem(bundle);
                }
            }
        });
        Button buyReserveButton = view.findViewById(R.id.button_buy_reserve);


        if(product != null){

            Picasso.get().load(product.getGallery().get(0)).into(image);
            name.setText(product.getName());
            desc.setText(product.getDescription());
            category.setText(product.getCategory().getName());
            subcategory.setText(product.getSubcategory().getName());
            typeChip.setText("Product");
            String priceStr = (int)product.getPrice() + "$";
            price.setText(priceStr);

            crossedPrice.setVisibility( product.getDiscount() != 0.0 ? View.VISIBLE : View.INVISIBLE );
            String discountedPrice = "" +  (product.getDiscount() != 0.0 ? (int)(product.getPrice() - product.getPrice()*product.getDiscount()/100) + "$" : "");
            discount.setText(discountedPrice);

            specifics.setVisibility(View.GONE);
            location.setVisibility(View.GONE);
            deadlineCancel.setVisibility(View.GONE);
            deadlineReservation.setVisibility(View.GONE);


            buyReserveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentActivity activity = (FragmentActivity) getContext();


                    BuyProductFragment fragment = new BuyProductFragment(product);

                    activity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.logged_in_fragment_container, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            });

        }else if (service != null){

            Picasso.get().load(service.getGallery().get(0)).into(image);
            name.setText(service.getName());
            desc.setText(service.getDescription());
            category.setText(service.getCategory().getName());
            subcategory.setText(service.getSubcategory().getName());
            typeChip.setText("Service");
            String priceStr = (int)service.getPrice() + "$";
            price.setText(priceStr);

            crossedPrice.setVisibility( service.getDiscount() != 0.0 ? View.VISIBLE : View.INVISIBLE );
            String discountedPrice = "" +  (service.getDiscount() != 0.0 ? (int)(service.getPrice() - service.getPrice()*service.getDiscount()/100) + "$" : "");
            discount.setText(discountedPrice);

            specifics.setText(service.getSpecifics());
            location.setText(service.getLocation());
            String cancelStr = "You have to cancel " + service.getCancelDeadline() + " days before!";
            deadlineCancel.setText(cancelStr);
            String reserveStr = "You have to reserve " + service.getReservationDeadline() + " days before!";
             deadlineReservation.setText(reserveStr);


            buyReserveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentActivity activity = (FragmentActivity) getContext();


                    ReserveServiceFragment fragment = new ReserveServiceFragment(service);

                    activity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.logged_in_fragment_container, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            });

        }else if (bundle != null){
            Picasso.get().load(bundle.getGallery().get(0)).into(image);

            name.setText(bundle.getName());
            desc.setText(bundle.getDescription());
            category.setText(bundle.getCategory().getName());
            subcategory.setText(bundle.getSubcategory().getName());
            typeChip.setText("Bundle");
            String priceStr = (int)bundle.getPrice() + "$";
            price.setText(priceStr);

            crossedPrice.setVisibility( bundle.getDiscount() != 0.0 ? View.VISIBLE : View.INVISIBLE );
            String discountedPrice = "" +  (bundle.getDiscount() != 0.0 ? (int)(bundle.getPrice() - bundle.getPrice()*bundle.getDiscount()/100) + "$" : "");
            discount.setText(discountedPrice);


            specifics.setVisibility(View.GONE);
            location.setVisibility(View.GONE);
            deadlineCancel.setVisibility(View.GONE);
            deadlineReservation.setVisibility(View.GONE);

            List<ItemMock> items = new ArrayList<>();
            items.addAll(bundle.getProducts());
            items.addAll(bundle.getServices());

            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(new ItemViewAdapter(getContext(),items,viewModel));

            buyReserveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentActivity activity = (FragmentActivity) getContext();


                    ReserveBundleFragment fragment = new ReserveBundleFragment(bundle);

                    activity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.logged_in_fragment_container, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            });

        }



        return view;
    }
}