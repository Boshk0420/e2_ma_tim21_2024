package com.example.eventplaner.fragments.management;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.ServiceAdapter;
import com.example.eventplaner.model.Service;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class FragmentServiceManagement extends Fragment {

    private FirebaseFirestore mDatabase;
    private RecyclerView mRecyclerView;
    private ServiceAdapter mAdapter;
    private EditText mEditTextSearch;
    private Button mButtonSearch;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = FirebaseFirestore.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service_management, container, false);

        mEditTextSearch = view.findViewById(R.id.editTextSearch);
        mButtonSearch = view.findViewById(R.id.buttonSearch);
        mRecyclerView = view.findViewById(R.id.recyclerViewServices);

        mButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchServices();
            }
        });

        Button buttonAddService = view.findViewById(R.id.buttonAddService);
        buttonAddService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragmentAddService = new FragmentAddService();
                FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
                transaction.replace(R.id.navigation_fragment_container, fragmentAddService);
                transaction.commit();
            }
        });

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new ServiceAdapter(new ArrayList<Service>());
        mRecyclerView.setAdapter(mAdapter);

        loadServices();

        return view;
    }

    private void loadServices() {
        mDatabase.collection("services")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<Service> serviceList = new ArrayList<>();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            Service service = document.toObject(Service.class);
                            serviceList.add(service);
                        }
                        mAdapter.updateServiceList(serviceList);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to load services: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void searchServices() {
        String searchQuery = mEditTextSearch.getText().toString().trim().toLowerCase();

        // Query Firestore to filter services by name
        mDatabase.collection("services")
                .whereEqualTo("name", searchQuery)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<Service> serviceList = new ArrayList<>();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            Service service = document.toObject(Service.class);
                            serviceList.add(service);
                        }
                        mAdapter.updateServiceList(serviceList);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to search services: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
