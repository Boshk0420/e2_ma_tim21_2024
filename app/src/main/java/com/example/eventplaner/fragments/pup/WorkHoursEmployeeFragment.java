package com.example.eventplaner.fragments.pup;

import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.eventplaner.R;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WorkHoursEmployeeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WorkHoursEmployeeFragment extends Fragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    private LinearLayout parentLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_work_hours_employee, container, false);
        parentLayout = rootView.findViewById(R.id.parent_layout_employee);

        // Days of the week
        String[] daysOfWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

        for (String day : daysOfWeek) {
            addCardView(day);
        }
        return rootView;
    }

    private void addCardView(String day) {
        LayoutInflater inflater = LayoutInflater.from(requireContext());
        View cardViewLayout = inflater.inflate(R.layout.schedule_card, parentLayout, false);

        EditText editTextFrom = cardViewLayout.findViewById(R.id.editText_from);
        EditText editTextTo = cardViewLayout.findViewById(R.id.editText_to);
        CheckBox checkboxDay = cardViewLayout.findViewById(R.id.checkbox_day);
        checkboxDay.setText(day);

        editTextFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker(editTextFrom);
            }
        });

        editTextTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker(editTextTo);
            }
        });

        parentLayout.addView(cardViewLayout);
    }

    private void showTimePicker(final EditText editText) {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
                        String time = String.format("%02d:%02d", hourOfDay, minute);
                        editText.setText(time);
                    }
                }, hour, minute, true);
        timePickerDialog.show();
    }
}