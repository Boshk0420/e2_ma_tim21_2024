package com.example.eventplaner.fragments.login_and_registration;

import static android.content.ContentValues.TAG;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplaner.LoggedInActivity;
import com.example.eventplaner.MainActivity;
import com.example.eventplaner.NotLogedInActivity;
import com.example.eventplaner.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        if (mAuth.getCurrentUser() != null){




            Intent newIntent = new Intent(getContext(), LoggedInActivity.class);
            startActivity(newIntent);
        }

        TextView registerNowTextView = view.findViewById(R.id.textView2);

        Button button = view.findViewById(R.id.loginButton);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText email = view.findViewById(R.id.loginEmail);
                EditText password = view.findViewById(R.id.loginPassword);
                String emailData = email.getText().toString();
                String passwordData = password.getText().toString();

                mAuth.signInWithEmailAndPassword(emailData, passwordData)
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    Log.d(TAG, "signInWithEmail:success");
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    if(user.isEmailVerified()){
                                        updateMessageToken();
                                        Toast.makeText(getActivity(), "Authentication succesfull." + user.getEmail(),
                                                Toast.LENGTH_SHORT).show();
                                        Intent newIntent = new Intent(getContext(), LoggedInActivity.class);
                                        startActivity(newIntent);
                                    }


                                } else {
                                    // If sign in fails, display a message to the user.
                                    Log.w(TAG, "signInWithEmail:failure", task.getException());
                                    Toast.makeText(getActivity(), "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();

                                }
                            }
                        });


            }
        });

        registerNowTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.notLogedInLayout, new RegistrationFragment());
                transaction.addToBackStack(null); // Optional: Add the transaction to the back stack
                transaction.commit();
            }
        });


        return view;
    }



    private void updateMessageToken(){
        DocumentReference userRef = db.collection("users").document(mAuth.getCurrentUser().getUid());
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();


                        // Update the token field in the user document
                        userRef.update("messageToken", token)
                                .addOnSuccessListener(aVoid -> {
                                    // Token updated successfully
                                    Log.d(TAG, "Token updated for user: " + mAuth.getCurrentUser().getUid());
                                })
                                .addOnFailureListener(e -> {
                                    // Error updating token
                                    Log.e(TAG, "Error updating token for user: " + mAuth.getCurrentUser().getUid(), e);
                                });
                    }
                });
    }
}