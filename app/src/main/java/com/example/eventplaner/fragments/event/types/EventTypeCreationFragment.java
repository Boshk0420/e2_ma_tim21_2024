package com.example.eventplaner.fragments.event.types;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.EventSubcategoryHandlerAdapter;
import com.example.eventplaner.adapters.EventTypeHandlerAdapter;
import com.example.eventplaner.adapters.SubcategoryAdapter;
import com.example.eventplaner.fragments.event.categories.HandleEventCategoriesFragment;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.EventType;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;


public class EventTypeCreationFragment extends Fragment {
    private RecyclerView recyclerView;
    private SubcategoryAdapter adapter;
    private FirebaseFirestore db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event_type_creation, container, false);
        db = FirebaseFirestore.getInstance();
        recyclerView = view.findViewById(R.id.event_types_creation_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        fetchSubcategories();


        EditText typeName = view.findViewById(R.id.create_type_name);
        EditText typeDescription = view.findViewById(R.id.create_type_description);

        Button createTypeButton = view.findViewById(R.id.createTypeButton);

        createTypeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventType eventType = new EventType();
                eventType.setName(typeName.getText().toString());
                eventType.setDescription(typeDescription.getText().toString());
                eventType.setActive(true);
                List<String> selectedSubcategoriesIds = new ArrayList<>();
                for (EventSubcategory selectedSubcategory:
                     adapter.getSelectedSubcategories()) {
                    selectedSubcategoriesIds.add(selectedSubcategory.getId());
                }
                eventType.setSuggestedSubcategories(selectedSubcategoriesIds);

                db.collection("eventTypes")
                        .add(eventType)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                HandleEventTypesFragment nextFragment = new HandleEventTypesFragment();
                                FragmentManager fragmentManager = getParentFragmentManager();
                                FragmentTransaction transaction = fragmentManager.beginTransaction();
                                transaction.replace(R.id.navigation_fragment_container, nextFragment);
                                transaction.addToBackStack(null);
                                transaction.commit();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error adding document", e);
                            }
                        });
            }
        });

        return view;
    }

    private void fetchSubcategories() {
        db.collection("eventSubcategories")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<EventSubcategory> subcategories = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String subcategoryId = document.getId();
                            EventSubcategory subcategory = document.toObject(EventSubcategory.class);
                            subcategory.setId(subcategoryId);
                            subcategories.add(subcategory);
                        }

                        updateRecyclerView(subcategories);
                    } else {

                    }
                });

    }
    private void updateRecyclerView(List<EventSubcategory> subcategories) {
        adapter = new SubcategoryAdapter(subcategories);
        recyclerView.setAdapter(adapter);
    }


}