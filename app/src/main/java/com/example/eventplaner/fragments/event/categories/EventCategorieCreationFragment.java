package com.example.eventplaner.fragments.event.categories;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.eventplaner.R;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.dto.EventCategoryDto;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class EventCategorieCreationFragment extends Fragment {
    private FirebaseFirestore db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_categorie_creation, container, false);
        db = FirebaseFirestore.getInstance();

        Button createButton = view.findViewById(R.id.createCategoryButton);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText categoryName = view.findViewById(R.id.editCategoryName);
                EditText categoryDescription = view.findViewById(R.id.editCategoryDescription);

                String categoryNameData = categoryName.getText().toString();
                String categoryDescriptionData = categoryDescription.getText().toString();

                if(!categoryDescriptionData.equals("") && !categoryDescriptionData.equals(""))
                {
                    EventCategoryDto eventCategoryDto = new EventCategoryDto();
                    eventCategoryDto.setName(categoryNameData);
                    eventCategoryDto.setDescription(categoryDescriptionData);
                    eventCategoryDto.setSubcategories(new ArrayList<>());
                    db.collection("eventCategories")
                            .add(eventCategoryDto)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                    HandleEventCategoriesFragment nextFragment = new HandleEventCategoriesFragment();
                                    FragmentManager fragmentManager = getParentFragmentManager();
                                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                                    transaction.replace(R.id.navigation_fragment_container, nextFragment);
                                    transaction.addToBackStack(null);
                                    transaction.commit();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error adding document", e);
                                }
                            });
                }
            }
        });

        return view;
    }
}