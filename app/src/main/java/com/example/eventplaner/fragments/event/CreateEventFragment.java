package com.example.eventplaner.fragments.event;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.Budget.BudgetCategoryAdapter;
import com.example.eventplaner.adapters.EventTypeArrayAdapter;
import com.example.eventplaner.fragments.budget_planer.BudgetPlanerTabFragment;
import com.example.eventplaner.fragments.event.agenda.AgendaCreationFragment;
import com.example.eventplaner.fragments.event.guest_list.GuestListFragment;
import com.example.eventplaner.fragments.items.ItemListFragment;
import com.example.eventplaner.model.Budget;
import com.example.eventplaner.model.BudgetCategory;
import com.example.eventplaner.model.BudgetSubCategory;
import com.example.eventplaner.model.Event;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.EventType;
import com.example.eventplaner.model.dto.EventSubcategoryDto;
import com.example.eventplaner.viewmodels.AgendaViewModel;
import com.example.eventplaner.viewmodels.BudgetViewModel;
import com.example.eventplaner.viewmodels.GuestViewModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.materialswitch.MaterialSwitch;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import lombok.val;
import lombok.var;

public class CreateEventFragment extends Fragment {


    private Event createdEvent;
    private EventType eventType;
    List<EventType> events;

    private BudgetViewModel viewModel;

    private AgendaViewModel agendaViewModel;

    private GuestViewModel guestViewModel;

    private List<EventCategory> eventCategories;

    private Budget budget;

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    private final FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public CreateEventFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(BudgetViewModel.class);
        agendaViewModel = new ViewModelProvider(requireActivity()).get(AgendaViewModel.class);
        guestViewModel = new ViewModelProvider(requireActivity()).get(GuestViewModel.class);
        viewModel.setBudget(new Budget("","",mAuth.getCurrentUser().getUid(),new ArrayList<>(),new ArrayList<>()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_event, container, false);

        viewModel.getData().observe(getViewLifecycleOwner(), newBudget -> {
            budget = newBudget;
        });

        eventCategories = new ArrayList<>();

        TextInputEditText eventDate = view.findViewById(R.id.editTextEventDate);
        TextInputEditText eventTime = view.findViewById(R.id.editTextEventTime);
        AutoCompleteTextView eventTypeSelect = view.findViewById(R.id.editTextEventType);

        initData(view, eventTypeSelect);


        TextInputEditText eventName = view.findViewById(R.id.editTextEventName);
        TextInputEditText description = view.findViewById(R.id.editTextEventDescription);
        TextInputEditText attendees = view.findViewById(R.id.editTextAttendees);
        TextInputEditText location = view.findViewById(R.id.editTextLocation);
        TextInputEditText locRadius = view.findViewById(R.id.editTextLocationRadius);

        MaterialSwitch swPrivate = view.findViewById(R.id.switchPrivate);

        Button createButton = view.findViewById(R.id.buttonCreate);
        Button editBudget = view.findViewById(R.id.buttonEditBudget);
        Button editAgenda = view.findViewById(R.id.buttonEditAgenda);
        Button editGuestList = view.findViewById(R.id.buttonEditGuestList);

        editAgenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentFragmentManager().beginTransaction()
                        .replace(R.id.logged_in_fragment_container, new AgendaCreationFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        editBudget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(eventType == null){
                    eventTypeSelect.setError("Must select a event type to edit budget!");
                }else{
                getParentFragmentManager().beginTransaction()
                        .replace(R.id.logged_in_fragment_container, new BudgetPlanerTabFragment())
                        .addToBackStack(null)
                        .commit();
                }
            }
        });

        editGuestList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentFragmentManager().beginTransaction()
                        .replace(R.id.logged_in_fragment_container,new GuestListFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });


        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isValidForm(eventName,description,attendees,location,locRadius,eventDate,eventTime,eventTypeSelect)) return;

                Map<String,Object> eventMap = new HashMap<>();
                eventMap.put("name",eventName.getText().toString());
                eventMap.put("description",description.getText().toString());
                eventMap.put("maxAttendees",Integer.parseInt(attendees.getText().toString()));
                eventMap.put("location",location.getText().toString());
                eventMap.put("locationRadius",Integer.parseInt(locRadius.getText().toString()));
                eventMap.put("private",swPrivate.isChecked());
                eventMap.put("date",parseDate(eventDate,eventTime));
                eventMap.put("eventType",eventType.getId());
                eventMap.put("userId",mAuth.getCurrentUser().getUid());

                db.collection("events")
                        .add(eventMap)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                saveBudget(documentReference);
                                saveAgenda(documentReference);
                                saveGuests(documentReference);
                                getParentFragmentManager().popBackStack();
                                Snackbar.make(view,"Event successfully created!",Snackbar.LENGTH_SHORT).show();
                            }
                        });
                
            }
        });


        onClickShowDatePicker(eventDate);
        onClickShowTimePicker(eventTime);




        return view;
    }

    private void saveGuests(DocumentReference documentReference){
        Map<String, Object> guestMap = new HashMap<>();
        guestMap.put("eventId",documentReference.getId());
        guestMap.put("guests",guestViewModel.getData().getValue());

        db.collection("guest")
                .add(guestMap)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                });


    }

    private void saveAgenda(DocumentReference documentReference){
        Map<String, Object> agendaMap = new HashMap<>();
        agendaMap.put("eventId",documentReference.getId());
        agendaMap.put("agenda",agendaViewModel.getData().getValue());

        db.collection("agenda")
                .add(agendaMap)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                });
    }
    private void saveBudget(DocumentReference documentReference) {
        Map<String, Object> budgetMap = new HashMap<>();
        budgetMap.put("spentBudget", budget.getSpentBudget());
        budgetMap.put("planedBudget", budget.getPlanedBudget());
        budgetMap.put("eventId", documentReference.getId());
        budgetMap.put("userId", budget.getUserId());

        db.collection("budget")
                .add(budgetMap)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                        budget.setId(documentReference.getId());
                    }
                });
    }

    private boolean isValidForm(TextInputEditText  eventName,TextInputEditText description,
                                TextInputEditText attendees,TextInputEditText location,
                                TextInputEditText locRadius,TextInputEditText eventDate,
                                TextInputEditText eventTime, AutoCompleteTextView eventTypeSelect ){

        boolean isValid = true;

        if(eventName.getText().toString().isEmpty())
        {
            eventName.setError("Name cannot be empty!");
            isValid = false;
        }

        if(description.getText().toString().isEmpty())
        {
            description.setError("Description cannot be empty!");
            isValid = false;
        }

        if(attendees.getText().toString().isEmpty()){
            attendees.setError("Must enter number of attendees!");
            isValid = false;
        }

        if(location.getText().toString().isEmpty()){
            location.setError("Must enter number of attendees!");
            isValid = false;
        }


        if(locRadius.getText().toString().isEmpty()){
            locRadius.setError("Must enter location radius!");
            isValid = false;
        }

        if(eventDate.getText().toString().isEmpty()){
            eventDate.setError("Must enter event date!");
            isValid = false;
        }

        if(eventTime.getText().toString().isEmpty()){
            eventTime.setError("Must enter event time!");
            isValid = false;
        }

        if(eventType == null){
            eventTypeSelect.setError("Must select a event type!");
            isValid = false;
        }


        return isValid;
    }

    private void initData(View view, AutoCompleteTextView eventTypeSelect) {
        events = new ArrayList<>();

        db.collection("eventTypes").get()
                .addOnCompleteListener(task -> {
                if ( task.isSuccessful()){
                    for ( DocumentSnapshot document : task.getResult()){
                        EventType eventType1 = document.toObject(EventType.class);
                        eventType1.setId(document.getId());
                        events.add(eventType1);
                    }
                    EventTypeArrayAdapter typeArrayAdapter = new EventTypeArrayAdapter(view.getContext(),events);
                    eventTypeSelect.setAdapter(typeArrayAdapter);
                    registerTypeSelect(eventTypeSelect);
                } else {
                    Log.d("Firestore","Error getting documents: ", task.getException());
                }
                });
    }

    private void registerTypeSelect(AutoCompleteTextView eventTypeSelect){
        eventTypeSelect.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        eventType = (EventType) parent.getItemAtPosition(position);
                        eventTypeSelect.setError(null);
                        initPlannedBudget();
                    }
                }
        );
    }

    private Date parseDate(TextInputEditText eventDate, TextInputEditText eventTime){
        String dateStr = eventDate.getText().toString();
        String timeStr = eventTime.getText().toString();


        Calendar calendar = Calendar.getInstance();


        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
        try {
            Date date = dateFormat.parse(dateStr);
            calendar.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        try {
            Date time = timeFormat.parse(timeStr);
            Calendar timeCalendar = Calendar.getInstance();
            timeCalendar.setTime(time);

            calendar.set(Calendar.HOUR_OF_DAY, timeCalendar.get(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, timeCalendar.get(Calendar.MINUTE));
        } catch (ParseException e) {
            e.printStackTrace();

        }


        return calendar.getTime();

    }

    private void onClickShowDatePicker(TextInputEditText eventDate){
        eventDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MaterialDatePicker<Long> datePicker = MaterialDatePicker.Builder.datePicker().build();
                datePicker.addOnPositiveButtonClickListener(selection -> {

                    eventDate.setText(datePicker.getHeaderText());
                    eventDate.setError(null);

                });
                datePicker.show(getParentFragmentManager(), "DATE_PICKER");
            }
        });
    }

    private void onClickShowTimePicker(TextInputEditText eventTime){
        eventTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MaterialTimePicker timePicker = new MaterialTimePicker.Builder()
                        .setTimeFormat(TimeFormat.CLOCK_24H)
                        .build();
                timePicker.addOnPositiveButtonClickListener(view -> {
                    eventTime.setText(String.format("%02d:%02d", timePicker.getHour(), timePicker.getMinute()));
                    eventTime.setError(null);
                });
                timePicker.show(getParentFragmentManager(), "TIME_PICKER");
            }
        });
    }

    private void initPlannedBudget()
    {
        eventCategories = new ArrayList<>();
        budget.setSpentBudget(new ArrayList<>());
        if(!eventType.getSuggestedSubcategories().isEmpty()) {
            db.collection("eventSubcategories")
                    .whereIn(FieldPath.documentId(), eventType.getSuggestedSubcategories())
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            List<EventSubcategoryDto> subcategories = new ArrayList<>();
                            for (DocumentSnapshot document : task.getResult()) {
                                EventSubcategoryDto eventSubcategory = document.toObject(EventSubcategoryDto.class);
                                eventSubcategory.setId(document.getId());
                                subcategories.add(eventSubcategory);
                            }

                            getCategories(subcategories);

                        } else {
                            Log.d("Firestore", "Error getting documents: ", task.getException());
                        }
                    });
        }else{
            budget.setPlanedBudget(new ArrayList<>());
            viewModel.setBudget(budget);
        }
    }

    private void getCategories(List<EventSubcategoryDto> subcategories){
        List<String> categoryIds = new ArrayList<>();
        for(EventSubcategoryDto subcategoryDto: subcategories){
            if(!categoryIds.contains(subcategoryDto.getCategoryId()))
                categoryIds.add(subcategoryDto.getCategoryId());
        }


        db.collection("eventCategories")
                .whereIn(FieldPath.documentId(),categoryIds)
                .get()
                .addOnCompleteListener(task -> {
                    if ( task.isSuccessful()){
                        for ( DocumentSnapshot document : task.getResult()){
                            EventCategory eventCategory = document.toObject(EventCategory.class);
                            eventCategory.setId(document.getId());
                            eventCategories.add(eventCategory);
                        }

                        for(EventSubcategoryDto subcategoryDto: subcategories){

                            Optional<EventCategory> catg = eventCategories
                                    .stream()
                                    .filter(
                                            category -> category
                                                            .getId()
                                                            .equals(subcategoryDto.getCategoryId())
                                            )
                                    .findFirst();

                            if(catg.isPresent()){
                                    if(catg.get().getSubcategories() == null) catg.get().setSubcategories(new ArrayList<>());
                                    catg.get().getSubcategories().add(new EventSubcategory(subcategoryDto.getId(), subcategoryDto.getName(),subcategoryDto.getDescription(),subcategoryDto.getType(), subcategoryDto.getCategoryId()));
                            }
                        }
                        setPlannedBudget();
                    } else {
                        Log.d("Firestore","Error getting documents: ", task.getException());
                    }
        });




    }

    private void setPlannedBudget(){
        List<BudgetCategory> plannedBudget = new ArrayList<>();
        for(EventCategory category: eventCategories){
            BudgetCategory budgetCategory = new BudgetCategory(category.getId(), category.getName(), new ArrayList<>(),0);
            for(EventSubcategory subcategory: category.getSubcategories()){
                budgetCategory.getSubCategories().add(new BudgetSubCategory(subcategory.getId(), subcategory.getName(),100));
                budgetCategory.setEstimatedPrice(budgetCategory.getEstimatedPrice() + 100);
            }
            plannedBudget.add(budgetCategory);
        }
        budget.setPlanedBudget(plannedBudget);
        viewModel.setBudget(budget);
    }




}