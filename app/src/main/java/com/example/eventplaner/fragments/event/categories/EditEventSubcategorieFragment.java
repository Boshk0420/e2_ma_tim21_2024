package com.example.eventplaner.fragments.event.categories;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.eventplaner.R;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.SubcategoryType;
import com.example.eventplaner.model.dto.EventCategoryDto;
import com.example.eventplaner.model.dto.EventSubcategoryDto;
import com.example.eventplaner.utils.NotificationSender;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EditEventSubcategorieFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditEventSubcategorieFragment extends Fragment {
    FirebaseFirestore db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_event_subcategorie, container, false);
        db = FirebaseFirestore.getInstance();
        Bundle bundle = getArguments();
        EventSubcategoryDto eventSubcategoryDto = (EventSubcategoryDto) bundle.getSerializable("eventSubcategory");
        EditText editName = view.findViewById(R.id.edit_subcategory_creation_name);
        EditText editDescription = view.findViewById(R.id.edit_subcategory_creation_description);
        Button confirmButton = view.findViewById(R.id.edit_subcategory_creation_button);


        Spinner spinner = view.findViewById(R.id.subcategorie_edit_spinner);
        String[] types = {"Usluga", "Proizvod"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, types);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });




        editName.setText(eventSubcategoryDto.getName());
        editDescription.setText(eventSubcategoryDto.getDescription());
        if(eventSubcategoryDto.getType().equals(SubcategoryType.USLUGA)){
            spinner.setSelection(0);
        }
        else{
            spinner.setSelection(1);
        }


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationSender notificationSender = new NotificationSender();
                notificationSender.sendNotificationToPupv("Event subcategorie has been updated. Subcategory " + eventSubcategoryDto.getName() + " is now called " + editName.getText().toString() + " and description is " + editDescription.getText().toString(), "Subcategory updated");
                eventSubcategoryDto.setName(editName.getText().toString());
                eventSubcategoryDto.setDescription(editDescription.getText().toString());
                if(spinner.getSelectedItem().toString().equals("Proizvod")){
                    eventSubcategoryDto.setType(SubcategoryType.PROIZVOD);
                }else {
                    eventSubcategoryDto.setType(SubcategoryType.USLUGA);
                }
                updateSubcategory(eventSubcategoryDto);

            }
        });

        return view;
    }


    private void updateSubcategory(EventSubcategoryDto eventsubcategoryDto) {
        Map<String, Object> updates = new HashMap<>();
        updates.put("name", eventsubcategoryDto.getName());
        updates.put("description", eventsubcategoryDto.getDescription());
        updates.put("type", eventsubcategoryDto.getType());

        db.collection("eventSubcategories")
                .document(eventsubcategoryDto.getId())
                .update(updates)
                .addOnSuccessListener(aVoid -> {
                    // Handle success
                })
                .addOnFailureListener(e -> {
                    // Handle failure
                });
    }
}