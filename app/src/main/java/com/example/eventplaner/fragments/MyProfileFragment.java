package com.example.eventplaner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.eventplaner.MainActivity;
import com.example.eventplaner.R;
import com.example.eventplaner.model.users.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;

import java.util.HashMap;
import java.util.Map;

public class MyProfileFragment extends Fragment {

    FirebaseUser currentUser;
    FirebaseAuth mAuth;

    User user;

    private boolean isEditable = false;
    public MyProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        TextInputEditText name = view.findViewById(R.id.editTextProfileName);
        TextInputEditText surname = view.findViewById(R.id.editTextProfileSurname);
        TextInputEditText email = view.findViewById(R.id.editTextProfileEmail);
        TextInputEditText phone = view.findViewById(R.id.editTextProfilePhone);
        TextInputEditText street = view.findViewById(R.id.editTextProfileStreet);

        TextInputEditText newPass = view.findViewById(R.id.editTextProfileNewPassword);
        TextInputEditText confPass = view.findViewById(R.id.editTextProfileConfirmPass);



        setReadOnly(name);
        setReadOnly(surname);
        setReadOnly(email);
        setReadOnly(phone);
        setReadOnly(street);



        Button editProfile = view.findViewById(R.id.buttonEditProfile);
        Button saveProfile = view.findViewById(R.id.buttonSaveProfile);
        Button changePass = view.findViewById(R.id.buttonPasswordProfile);


        changePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(newPass.getText().toString().equals(confPass.getText().toString())){
                    if (currentUser != null) {
                        currentUser.updatePassword(newPass.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Snackbar.make(view,"Password updated!",Snackbar.LENGTH_SHORT).show();
                                } else {
                                    Snackbar.make(view,"Password update failed",Snackbar.LENGTH_SHORT).show();
                                }
                            }
                        });
                    } else {
                        Snackbar.make(view,"No user is signed in",Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });

        saveProfile.setClickable(false);

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEditable = !isEditable;
                if (isEditable) {
                    setEditable(name);
                    setEditable(surname);
                    setEditable(street);
                    setEditable(phone);
                    saveProfile.setClickable(true);
                } else {
                    setReadOnly(name);
                    setReadOnly(surname);
                    setReadOnly(street);
                    setReadOnly(phone);
                    saveProfile.setClickable(false);
                }
            }
        });



        email.setText(currentUser.getEmail());


        DocumentReference userRef = db.collection("users").document(currentUser.getUid());

        userRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    user = document.toObject(User.class);

                    name.setText(user.getFirstName());
                    surname.setText(user.getLastName());
                    phone.setText(user.getPhone());
                    street.setText(user.getStreet());


                } else {
                    Log.d("MainActivity", "No such document");
                }
            } else {
                Log.d("MainActivity", "get failed with ", task.getException());
            }
        });

        saveProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String,Object> updates = new HashMap<>();
                updates.put("lastName",name.getText().toString());
                updates.put("firstName",surname.getText().toString());
                updates.put("phone",phone.getText().toString());
                updates.put("street",street.getText().toString());

                userRef.update(updates).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Snackbar.make(view,"Profile updated!",Snackbar.LENGTH_SHORT).show();
                        } else {
                            Snackbar.make(view,"Error while updating profile!",Snackbar.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });


        return view;
    }

    private void setReadOnly(TextInputEditText editText) {
        editText.setFocusable(false);
        editText.setFocusableInTouchMode(false);
        editText.setClickable(false);
        editText.setLongClickable(false);
        editText.setCursorVisible(false);
        editText.setInputType(0); // InputType.TYPE_NULL
    }

    private void setEditable(TextInputEditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.setClickable(true);
        editText.setLongClickable(true);
        editText.setCursorVisible(true);
        editText.setInputType(android.text.InputType.TYPE_CLASS_TEXT); // or any input type you need
    }

    private void updatePassword(String newPassword,View view) {
        FirebaseUser user = mAuth.getCurrentUser();


    }

}