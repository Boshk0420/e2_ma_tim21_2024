package com.example.eventplaner.fragments.budget_planer;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.Budget.BudgetCategoryArrayAdapter;
import com.example.eventplaner.adapters.Budget.BudgetItemsArrayAdapter;
import com.example.eventplaner.adapters.Budget.BudgetSubCategoryArrayAdapter;
import com.example.eventplaner.adapters.SubcategoryAdapter;
import com.example.eventplaner.model.Budget;
import com.example.eventplaner.model.BudgetCategory;
import com.example.eventplaner.model.BudgetSpent;
import com.example.eventplaner.model.BudgetSubCategory;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.SubcategoryType;
import com.example.eventplaner.model.mocked.BundleMock;
import com.example.eventplaner.model.mocked.ItemMock;
import com.example.eventplaner.model.mocked.ProductMock;
import com.example.eventplaner.model.mocked.ServiceMocked;
import com.example.eventplaner.viewmodels.BudgetViewModel;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class CreateBudgetItemFragment extends Fragment {

    private BudgetViewModel viewModel;
    private Budget budget = new Budget();

    private List<ItemMock> items;

    private BudgetCategory category;

    private ItemMock item;

    private DialogFragment dialogFragment;

    public CreateBudgetItemFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(BudgetViewModel.class);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        items = new ArrayList<>();
        items.addAll(initProduct());
        items.addAll(initService());
        items.addAll(initBundle());

        View view = inflater.inflate(R.layout.fragment_create_budget_item, container, false);

        dialogFragment = (DialogFragment) getParentFragment();

        Button button = view.findViewById(R.id.addPlanedBudgetItem);

        AutoCompleteTextView categorySelect = view.findViewById(R.id.editTextCategory);
        AutoCompleteTextView itemSelect = view.findViewById(R.id.editTextItem);


        viewModel.getData().observe(getViewLifecycleOwner(), newBudget -> {
            budget = newBudget;
            BudgetCategoryArrayAdapter categoryArrayAdapter = new BudgetCategoryArrayAdapter(view.getContext(),budget.getPlanedBudget());
            categorySelect.setAdapter(categoryArrayAdapter);
        });

        registerCategorySelect(categorySelect,itemSelect);
        registerItemSelect(itemSelect);
        registerButtonClick(button);


        return view;
    }

    public void registerItemSelect(AutoCompleteTextView itemSelect){
        itemSelect.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                item = (ItemMock) parent.getItemAtPosition(position);
            }
        });

    }

    private void registerButtonClick(Button button) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(budget.getSpentBudget().stream().anyMatch(x -> x.getSubCategory().equals(item.getSubcategory().getName())))
                {
                    BudgetSpent spent = budget.getSpentBudget().stream().filter(x -> x.getSubCategory().equals(item.getSubcategory().getName())).findFirst().get();
                    spent.getItemList().add(item);
                    spent.setSpent(spent.getSpent()+item.getPrice());
                }else{
                    List<ItemMock> items = new ArrayList<>();
                    items.add(item);
                    BudgetSpent spent = new BudgetSpent(item.getSubcategory().getName(),items,item.getPrice());
                    budget.getSpentBudget().add(spent);
                }

                viewModel.setBudget(budget);
                dialogFragment.dismiss();
            }
        });
    }


    private void registerCategorySelect(AutoCompleteTextView categorySelect,AutoCompleteTextView itemSelect) {
        categorySelect.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                category = (BudgetCategory) parent.getItemAtPosition(position);

                List<ItemMock> itemsFiltered = new ArrayList<>(
                        items.stream().filter(item -> item.getCategory().equals(category.getCategory()) || true ).collect(Collectors.toList())
                );
                itemSelect.setAdapter(new BudgetItemsArrayAdapter(getContext(),itemsFiltered));
            }
        });
    }

    private List<ProductMock> initProduct() {
        List<ProductMock> list = new ArrayList<>();

        List<String> images1 = new ArrayList<>();
        images1.add("https://www.artnews.com/wp-content/uploads/2020/09/AdobeStock_1557441.jpeg");

        List<String> images2 = new ArrayList<>();
        images2.add("https://ae01.alicdn.com/kf/HTB1SkTQX._rK1Rjy0Fcq6zEvVXa3/Photo-Album-Scrapbook-Loose-leaf-Photos-Albums-Large-Size-Picture-Album-Wedding-Photograph-Baby-Memory-Albums.jpg");


        list.add(new ProductMock(new ItemMock("",
                "Album sa 50 fotografija",
                "Album sa 50 izradjenih fotografija",
                new EventCategory("","Foto i video", "Fotografije i Albumi", new ArrayList<>()),
                new EventSubcategory("","Fotografije i Albumi","Fotografije i Albumi", SubcategoryType.PROIZVOD,""),
                images1,
                new ArrayList<>(),
                2000,
                5,
                true,
                true
        )));

        list.add(new ProductMock(new ItemMock("",
                "Foto book",
                "Foto book sa vasim fotografijama",
                new EventCategory("","Foto i video", "Fotografije i Albumi", new ArrayList<>()),
                new EventSubcategory("","Fotografije i Albumi","Fotografije i Albumi",SubcategoryType.PROIZVOD,""),
                images2,
                new ArrayList<>(),
                5000,
                0,
                true,
                true
        )));

        return list;
    }

    private List<ServiceMocked> initService(){
        List<ServiceMocked> list = new ArrayList<>();

        List<String> images1 = new ArrayList<>();
        images1.add("https://blogs.icrc.org/law-and-policy/wp-content/uploads/sites/102/2022/03/Drone-image-1096x620.jpg");
        List<String> images2 = new ArrayList<>();
        images2.add("https://cdn.tamaggo.com/1667003511745.jpg");

        ItemMock product1 = new ItemMock("",
                "Snimanje dronom",
                "Ovo je snimanje iz vazduha sa dronom",
                new EventCategory("","Foto i video", "Fotografije i Albumi", new ArrayList<>()),
                new EventSubcategory("","Snimanje dronom","Fotografije i Albumi",SubcategoryType.USLUGA,""),
                images1,
                new ArrayList<>(),
                6000,
                0,
                true,
                true
        );

        list.add(new ServiceMocked(product1,"ne radimo praznicima",6000,2,"Novi Sad",new ArrayList<>(),12,2,true));

        ItemMock product2 = new ItemMock("",
                "Snimanje kamerom 4k",
                "Ovo je snimanje u 4k rezoluciji",
                new EventCategory("","Foto i video", "Fotografije i Albumi", new ArrayList<>()),
                new EventSubcategory("","Videografija","Fotografije i Albumi",SubcategoryType.USLUGA,""),
                images2,
                new ArrayList<>(),
                7000,
                0,
                true,
                true
        );

        list.add(new ServiceMocked(product2,"/",5000,1,"Novi Sad",new ArrayList<>(),12,2,true));

        return list;
    }

    private List<BundleMock> initBundle(){
        List<BundleMock> list = new ArrayList<>();

        List<String> images1 = new ArrayList<>();
        images1.add("https://www.artnews.com/wp-content/uploads/2020/09/AdobeStock_1557441.jpeg");

        ItemMock item1 = new ItemMock("",
                "Snimanje dogadaja",
                "Ovaj paket ukljucuje sve potrebne stavke za dekoraciju vaseg vencanja",
                new EventCategory("","Foto i video", "Fotografije i Albumi", new ArrayList<>()),
                new EventSubcategory("","Videografija","Fotografije i Albumi",SubcategoryType.USLUGA,""),
                images1,
                new ArrayList<>(),
                18000,
                10,
                true,
                true
        );

        list.add(new BundleMock(item1,initProduct(),initService()));

        return list;
    }
}