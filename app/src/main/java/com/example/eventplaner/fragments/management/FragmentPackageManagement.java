package com.example.eventplaner.fragments.management;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.PackageAdapter;
import com.example.eventplaner.fragments.management.FragmentAddPackage;
import com.example.eventplaner.model.Packages;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class FragmentPackageManagement extends Fragment {

    private FirebaseFirestore mDatabase;
    private RecyclerView mRecyclerView;
    private PackageAdapter mAdapter;
    private EditText mEditTextSearch;
    private Button mButtonSearch;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = FirebaseFirestore.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_package_management, container, false);

        mEditTextSearch = view.findViewById(R.id.editTextSearch);
        mButtonSearch = view.findViewById(R.id.buttonSearch);
        mRecyclerView = view.findViewById(R.id.recyclerViewPackages);

        mButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchPackages();
            }
        });

        Button buttonAddPackage = view.findViewById(R.id.buttonAddPackage);
        buttonAddPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragmentAddPackage = new FragmentAddPackage();
                FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
                transaction.replace(R.id.navigation_fragment_container, fragmentAddPackage);
                transaction.commit();
            }
        });

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new PackageAdapter(new ArrayList<Packages>());
        mRecyclerView.setAdapter(mAdapter);

        loadPackages();

        return view;
    }

    private void loadPackages() {
        mDatabase.collection("packages")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<Packages> packageList = new ArrayList<>();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            Packages packageItem = document.toObject(Packages.class);
                            packageList.add(packageItem);
                        }
                        mAdapter.updatePackageList(packageList);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to load packages: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void searchPackages() {
        String searchQuery = mEditTextSearch.getText().toString().trim().toLowerCase();

        // Query Firestore to filter packages by name
        mDatabase.collection("packages")
                .whereEqualTo("name", searchQuery)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<Packages> packageList = new ArrayList<>();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            Packages packageItem = document.toObject(Packages.class);
                            packageList.add(packageItem);
                        }
                        mAdapter.updatePackageList(packageList);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to search packages: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}