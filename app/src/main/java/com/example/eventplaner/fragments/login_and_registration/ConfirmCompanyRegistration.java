package com.example.eventplaner.fragments.login_and_registration;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.EventCategoryHandlerAdapter;
import com.example.eventplaner.adapters.EventTypeHandlerAdapter;
import com.example.eventplaner.adapters.LoginAndRegistration.CompanyRegistrationRequestAdapter;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventType;
import com.example.eventplaner.model.dto.CompanyProfileRegistrationDto;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.search.SearchView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ConfirmCompanyRegistration extends Fragment {

    private final List<String> tags = new ArrayList<>();
    private List<CompanyProfileRegistrationDto> registrationRequests;
    private List<CompanyProfileRegistrationDto> filteredRegistrationRequests;
    private List<String> selectedTypesAndCategories;
    private List<EventType> eventTypes;
    List<EventCategory> eventCategories;
    FirebaseFirestore db;
    private RecyclerView recyclerView;
    private CompanyRegistrationRequestAdapter adapter;
    private Date dateFromSearch;
    private Date dateToSearch;

    public ConfirmCompanyRegistration() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_confirm_company_registration, container, false);
        db = FirebaseFirestore.getInstance();
        recyclerView = view.findViewById(R.id.registration_requests_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        eventCategories = new ArrayList<>();
        eventTypes = new ArrayList<>();

        fetchRegistrationRequests();



        ChipGroup chipGroup = view.findViewById(R.id.chipGroup);
        ChipGroup chipGroupCategory = view.findViewById(R.id.chipGroupCategory);
        ChipGroup chipGroupTypes = view.findViewById(R.id.chipGroupTypes);
        SearchView searchView = view.findViewById(R.id.search_view_requests);
        TextInputEditText dateFrom = view.findViewById(R.id.editTextDateFrom);
        TextInputEditText dateTo = view.findViewById(R.id.editTextDateTo);

        selectedTypesAndCategories = new ArrayList<>();

        getCategoryChips(chipGroupCategory);
        getTypeChips(chipGroupTypes);


        onClickShowDatePicker(dateFrom);
        onClickShowDatePicker(dateTo);


        searchView.
                getEditText()
                .setOnEditorActionListener(
                        (v, actionId, event) -> {
                            if(!searchView.getText().toString().isEmpty()) {
                                addChipToChipGroup(searchView.getText(),chipGroup);
                                tags.add(searchView.getText().toString());
                                searchView.setText("");
                            }
                            return false;
                        });


        searchView.addTransitionListener(new SearchView.TransitionListener() {
            @Override
            public void onStateChanged(@NonNull SearchView searchView, @NonNull SearchView.TransitionState transitionState, @NonNull SearchView.TransitionState transitionState1) {
                if(transitionState1 == SearchView.TransitionState.HIDING){
                    filteredRegistrationRequests.clear();
                    filteredRegistrationRequests.addAll(registrationRequests);


                    if(!tags.isEmpty())
                        filteredRegistrationRequests = filterRegistrationRequestsByWords(filteredRegistrationRequests, tags);

                    if(!selectedTypesAndCategories.isEmpty())
                        filteredRegistrationRequests = filterByTypesAndCategories(filteredRegistrationRequests, selectedTypesAndCategories);
                    if(dateFrom != null || dateTo != null) {
                        try {
                            filteredRegistrationRequests = filterByDates(filteredRegistrationRequests, dateFrom.getText().toString(), dateTo.getText().toString());
                        } catch (ParseException e) {
                            Log.d("DateParser", "Can't parse this dates");
                        }
                    }

                    adapter = new CompanyRegistrationRequestAdapter(filteredRegistrationRequests);
                    recyclerView.setAdapter(adapter);
                }
            }
        });

        return view;
    }

    private static List<CompanyProfileRegistrationDto> filterRegistrationRequestsByWords(List<CompanyProfileRegistrationDto> registrationRequests, List<String> tags) {

        return registrationRequests.stream()
                .filter(request ->
                        tags.stream().anyMatch(tag ->
                                request.getFirstName().toLowerCase().contains(tag.toLowerCase()) ||
                                        request.getLastName().toLowerCase().contains(tag.toLowerCase()) ||
                                        request.getCompanyName().toLowerCase().contains(tag.toLowerCase()) ||
                                        request.getEmail().toLowerCase().contains(tag.toLowerCase()) ||
                                        request.getCompanyEmail().toLowerCase().contains(tag.toLowerCase())
                        )
                )
                .collect(Collectors.toList());
    }

    private static List<CompanyProfileRegistrationDto> filterByTypesAndCategories(
            List<CompanyProfileRegistrationDto> registrationRequests,
            List<String> selectedTypesAndCategories) {

        List<CompanyProfileRegistrationDto> filteredRegistrationRequests = new ArrayList<>();

        for (CompanyProfileRegistrationDto registrationRequest : registrationRequests) {
            boolean allMatchFound = true;

            for (String selTypReq : selectedTypesAndCategories) {
                boolean matchFound = false;

                for (EventType eventType : registrationRequest.getTypes()) {
                    if (eventType.getName().equals(selTypReq)) {
                        matchFound = true;
                        break;
                    }
                }

                if (!matchFound) {
                    for (EventCategory eventCategory : registrationRequest.getCategories()) {
                        if (eventCategory.getName().equals(selTypReq)) {
                            matchFound = true;
                            break;
                        }
                    }
                }

                if (!matchFound) {
                    allMatchFound = false;
                    break;
                }
            }

            if (allMatchFound) {
                filteredRegistrationRequests.add(registrationRequest);
            }
        }

        return filteredRegistrationRequests;
    }

    private static List<CompanyProfileRegistrationDto> filterByDates(List<CompanyProfileRegistrationDto> registrationRequests, String dateFrom, String dateTo) throws ParseException {
        List<CompanyProfileRegistrationDto> filteredRequests = new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");

        Date dateToSearch = new Date();
        Date dateFromSearch = new Date();
        if(!dateTo.equals("")){
            dateToSearch = formatter.parse(dateTo);
        }else{
            dateToSearch = null;
        }
        if(!dateFrom.equals("")){
            dateFromSearch = formatter.parse(dateFrom);
        }else{
            dateFromSearch = null;
        }
        if(dateFromSearch != null && dateToSearch != null)
        {
            for(CompanyProfileRegistrationDto request : registrationRequests)
            {
                if(request.getCreatedAt().before(dateToSearch) && request.getCreatedAt().after(dateFromSearch))
                {
                    filteredRequests.add(request);
                }
            }
        } else if (dateToSearch != null) {
            for(CompanyProfileRegistrationDto request : registrationRequests)
            {
                if(request.getCreatedAt().before(dateToSearch))
                {
                    filteredRequests.add(request);
                }
            }
        } else if (dateFromSearch != null) {
            for(CompanyProfileRegistrationDto request : registrationRequests)
            {
                if(request.getCreatedAt().after(dateFromSearch))
                {
                    filteredRequests.add(request);
                }
            }
        }
        else {
            return registrationRequests;
        }

        return filteredRequests;

    }


    private void fetchRegistrationRequests() {
        db.collection("companyRegistrationRequests")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        registrationRequests = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String categoryId = document.getId();
                            CompanyProfileRegistrationDto registrationRequest = document.toObject(CompanyProfileRegistrationDto.class);
                            registrationRequest.setId(categoryId);
                            registrationRequests.add(registrationRequest);
                        }
                        filteredRegistrationRequests = new ArrayList<>();
                        filteredRegistrationRequests.addAll(registrationRequests);
                        // Populate RecyclerView with fetched data
                        adapter = new CompanyRegistrationRequestAdapter(filteredRegistrationRequests);
                        recyclerView.setAdapter(adapter);
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }

    private void onClickShowDatePicker(TextInputEditText eventDate){
        eventDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MaterialDatePicker<Long> datePicker = MaterialDatePicker.Builder.datePicker().build();
                datePicker.addOnPositiveButtonClickListener(selection -> {
                    eventDate.setText(datePicker.getHeaderText());
                });
                datePicker.show(getParentFragmentManager(), "DATE_PICKER");
            }
        });
    }
    private void addChipToChipGroup(Editable chipText, ChipGroup chipGroup){
        Chip chip = new Chip(getContext());
        chip.setText(chipText);
        chip.setClickable(true);
        chip.setCloseIconVisible(true);
        registerChipCloseEvent(chip,chipGroup);
        chipGroup.addView(chip);
    }
    private void registerChipCloseEvent(Chip chip,ChipGroup chipGroup){
        chip.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tags.remove(chip.getText().toString());
                chipGroup.removeView(chip);
            }
        });
    }

    private void createCategoryChips(ChipGroup chipGroup){
        for(EventCategory eventCategorie : eventCategories)
        {
            Chip chip = new Chip(getContext());
            chip.setText(eventCategorie.getName());
            chip.setClickable(true);
            chip.setCheckable(true);
            registerChipCheckedEvent(chip, chipGroup);
            chipGroup.addView(chip);
        }
    }

    private void createTypeChips(ChipGroup chipGroup){
        for(EventType eventType : eventTypes)
        {
            Chip chip = new Chip(getContext());
            chip.setText(eventType.getName());
            chip.setClickable(true);
            chip.setCheckable(true);
            registerChipCheckedEvent(chip, chipGroup);
            chipGroup.addView(chip);
        }
    }

    private void registerChipCheckedEvent(Chip chip,ChipGroup chipGroup){
        chip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!selectedTypesAndCategories.contains((String) chip.getText()))
                {
                    selectedTypesAndCategories.add((String) chip.getText());
                }
                else{
                    selectedTypesAndCategories.remove((String) chip.getText());
                }

            }
        });
    }

    private void  getCategoryChips(ChipGroup chipGroup)  {
        db.collection("eventCategories")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String categoryId = document.getId();
                            EventCategory category = document.toObject(EventCategory.class);
                            category.setId(categoryId);
                            eventCategories.add(category);
                        }
                        createCategoryChips(chipGroup);
                        // Populate RecyclerView with fetched data
                    } else {
                        // Log error or handle failure
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }
    private void getTypeChips(ChipGroup chipGroup) {
        db.collection("eventTypes")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {

                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String typeId = document.getId();
                            EventType type = document.toObject(EventType.class);
                            type.setId(typeId);
                            eventTypes.add(type);
                        }
                        // Populate RecyclerView with fetched data
                        createTypeChips(chipGroup);

                    } else {
                        // Log error or handle failure
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }
}