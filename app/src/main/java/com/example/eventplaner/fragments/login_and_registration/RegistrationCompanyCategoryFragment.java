package com.example.eventplaner.fragments.login_and_registration;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.EventCategoryListAdapter;
import com.example.eventplaner.adapters.EventSubcategoryHandlerAdapter;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.dto.CompnyRegistrationDto;
import com.example.eventplaner.model.dto.UserRegistrationDto;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;


public class RegistrationCompanyCategoryFragment extends Fragment {

    private RecyclerView recyclerView;
    private EventCategoryListAdapter adapter;
    private List<EventCategory> categories;
    private FirebaseFirestore db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_registration_company_category, container, false);
        db = FirebaseFirestore.getInstance();
        recyclerView = rootView.findViewById(R.id.recycler_category_view);
        //categories = new ArrayList<>();
        fetchCategories();





        Button nextButton = rootView.findViewById(R.id.category_selection_button_next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<EventCategory> selectedCategories = adapter.getSelectedCategories();
                if(selectedCategories.isEmpty()){
                    Toast.makeText(getContext(), "Please select at least 1 category.",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                    Bundle bundle = getArguments();
                    UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
                    CompnyRegistrationDto companyRegistrationDto = new CompnyRegistrationDto();

                    if (bundle != null) {
                        userRegistrationDto= (UserRegistrationDto) bundle.getSerializable("userRegistration");
                        companyRegistrationDto = (CompnyRegistrationDto) bundle.getSerializable("companyRegistration");
                    }
                    companyRegistrationDto.setEventCategories(selectedCategories);
                    Bundle newDataBundle = new Bundle();
                    newDataBundle.putSerializable("userRegistration", userRegistrationDto);
                    newDataBundle.putSerializable("companyRegistration", companyRegistrationDto);

                    RegistrationCompanyInfoEventTypeFragment nextFragment = new RegistrationCompanyInfoEventTypeFragment();
                    nextFragment.setArguments(newDataBundle);
                    FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.notLogedInLayout, nextFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }

            }
        });

        return rootView;
    }


    private void fetchCategories() {
        db.collection("eventCategories")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<EventCategory> categories = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String categoryId = document.getId();
                            EventCategory category = document.toObject(EventCategory.class);
                            category.setId(categoryId);
                            categories.add(category);
                        }

                        updateRecyclerView(categories);
                    } else {

                    }
                });
    }
    private void updateRecyclerView(List<EventCategory> categories) {
        adapter = new EventCategoryListAdapter(categories);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }




}