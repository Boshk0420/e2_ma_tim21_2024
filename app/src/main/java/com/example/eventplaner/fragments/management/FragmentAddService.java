package com.example.eventplaner.fragments.management;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.eventplaner.R;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventSubcategory;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FragmentAddService extends Fragment {

    private FirebaseFirestore mDatabase;
    private List<EventCategory> categories;
    private List<EventSubcategory> subcategories;
    private Spinner spinnerCategory;
    private Spinner spinnerSubcategory;

    public FragmentAddService() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_service, container, false);
        mDatabase = FirebaseFirestore.getInstance();
        categories = new ArrayList<>();
        subcategories = new ArrayList<>();
        spinnerCategory = view.findViewById(R.id.spinnerServiceCategory);
        spinnerSubcategory = view.findViewById(R.id.spinnerServiceSubcategory);
        loadCategories();
        Button addButton = view.findViewById(R.id.buttonSubmitService);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String category = spinnerCategory.getSelectedItem() != null ? spinnerCategory.getSelectedItem().toString() : "";
                    String subcategory = spinnerSubcategory.getSelectedItem() != null ? spinnerSubcategory.getSelectedItem().toString() : "";
                    EditText editTextServiceName = view.findViewById(R.id.editTextServiceName);
                    EditText editTextServiceDescription = view.findViewById(R.id.editTextServiceDescription);
                    EditText editTextServicePrice = view.findViewById(R.id.editTextServicePrice);
                    EditText editTextServiceDiscount = view.findViewById(R.id.editTextServiceDiscount);
                    EditText editTextServiceSpecifics = view.findViewById(R.id.editTextServiceSpecifics);
                    //EditText editTextServiceEventTypes = view.findViewById(R.id.editTextServiceEventTypes);
                    EditText editTextServiceDuration = view.findViewById(R.id.editTextServiceDuration);
                    EditText editTextServiceReservationDeadline = view.findViewById(R.id.editTextServiceReservationDeadline);
                    EditText editTextServiceCancelDeadline = view.findViewById(R.id.editTextServiceCancelDeadline);
                    CheckBox checkBoxServiceVisibility = view.findViewById(R.id.checkBoxServiceVisibility);
                    CheckBox checkBoxServiceAvailability = view.findViewById(R.id.checkBoxServiceAvailability);

                    String name = editTextServiceName.getText().toString();
                    String description = editTextServiceDescription.getText().toString();
                    double price = Double.parseDouble(editTextServicePrice.getText().toString());
                    double discount = Double.parseDouble(editTextServiceDiscount.getText().toString());
                    List<String> gallery = new ArrayList<>(); // Add logic to get gallery images if needed
                    List<String> eventTypes = new ArrayList<>();
                    //eventTypes.add(editTextServiceEventTypes.getText().toString());
                    String specifics = editTextServiceSpecifics.getText().toString();
                    String duration = editTextServiceDuration.getText().toString();
                    String reservationDeadline = editTextServiceReservationDeadline.getText().toString();
                    String cancelDeadline = editTextServiceCancelDeadline.getText().toString();
                    boolean available = checkBoxServiceAvailability.isChecked();
                    boolean visible = checkBoxServiceVisibility.isChecked();

                    addServiceToDatabase(category, subcategory, name, description, price, discount, gallery, eventTypes, specifics, duration, reservationDeadline, cancelDeadline, available, visible);
                } catch (Exception e) {
                    Log.e("FragmentAddService", "Error in onClick: " + e.getMessage());
                }
            }
        });
        return view;
    }

    private void loadCategories() {
        mDatabase.collection("eventCategories").get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            categories.clear();
                            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                EventCategory category = documentSnapshot.toObject(EventCategory.class);
                                categories.add(category);
                            }
                            populateCategoriesSpinner();
                        } else {
                            Log.d("Firestore", "No categories found");
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Firestore", "Error fetching categories: " + e.getMessage());
                    }
                });
    }

    private void populateCategoriesSpinner() {
        List<String> categoryNames = new ArrayList<>();
        for (EventCategory category : categories) {
            categoryNames.add(category.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, categoryNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(adapter);

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                loadSubcategories();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing
            }
        });
    }

    private void loadSubcategories() {
        mDatabase.collection("eventSubcategories").get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            subcategories.clear();
                            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                EventSubcategory subcategory = documentSnapshot.toObject(EventSubcategory.class);
                                subcategories.add(subcategory);
                            }
                            populateSubcategoriesSpinner();
                        } else {
                            Log.d("Firestore", "No subcategories found");
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Firestore", "Error fetching subcategories: " + e.getMessage());
                    }
                });
    }

    private void populateSubcategoriesSpinner() {
        List<String> subcategoryNames = new ArrayList<>();
        for (EventSubcategory subcategory : subcategories) {
            subcategoryNames.add(subcategory.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, subcategoryNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSubcategory.setAdapter(adapter);
    }

    private void addServiceToDatabase(String category, String subcategory, String name, String description, double price, double discount, List<String> gallery, List<String> eventTypes, String specifics, String duration, String reservationDeadline, String cancelDeadline, boolean available, boolean visible) {
        try {
            CollectionReference servicesRef = mDatabase.collection("services");

            Map<String, Object> serviceData = new HashMap<>();
            serviceData.put("category", category);
            serviceData.put("subcategory", subcategory);
            serviceData.put("name", name);
            serviceData.put("description", description);
            serviceData.put("price", price);
            serviceData.put("discount", discount);
            serviceData.put("gallery", gallery);
            serviceData.put("eventTypes", eventTypes);
            serviceData.put("specifics", specifics);
            serviceData.put("duration", duration);
            serviceData.put("reservationDeadline", reservationDeadline);
            serviceData.put("cancelDeadline", cancelDeadline);
            serviceData.put("available", available);
            serviceData.put("visible", visible);

            servicesRef.add(serviceData)
                    .addOnSuccessListener(documentReference -> Log.d("Firestore", "Service added successfully"))
                    .addOnFailureListener(e -> Log.e("Firestore", "Error adding service: " + e.getMessage()));
        } catch (Exception e) {
            Log.e("Firestore", "Error adding service: " + e.getMessage());
        }
    }
}
