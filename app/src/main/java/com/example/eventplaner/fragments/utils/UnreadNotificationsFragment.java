package com.example.eventplaner.fragments.utils;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.EventCategoryHandlerAdapter;
import com.example.eventplaner.adapters.NotificationAdapter;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.util.Notification;
import com.example.eventplaner.model.util.NotificationInfo;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public class UnreadNotificationsFragment extends Fragment {

    public UnreadNotificationsFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private List<Notification> unreadNotifications = new ArrayList<>();
    private FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    private NotificationAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_unread_notifications, container, false);

        recyclerView = view.findViewById(R.id.recyclerViewUnread);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        fetchUnreadNotifications(currentUser.getUid());

        return view;
    }



    private void fetchUnreadNotifications(String userId) {
        db.collection("notifications")
                .whereEqualTo("userId", userId)
                .whereEqualTo("read", false)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Notification notification = document.toObject(Notification.class);
                            notification.setId(document.getId());
                            unreadNotifications.add(notification);
                        }
                        // Populate RecyclerView with fetched data
                        adapter = new NotificationAdapter(unreadNotifications);
                        recyclerView.setAdapter(adapter);
                    } else {
                        // Log error or handle failure
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }
}