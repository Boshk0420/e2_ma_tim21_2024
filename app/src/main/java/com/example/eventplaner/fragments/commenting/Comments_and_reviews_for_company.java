package com.example.eventplaner.fragments.commenting;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.eventplaner.R;
import com.example.eventplaner.model.Comment;
import com.example.eventplaner.model.users.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Comments_and_reviews_for_company#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Comments_and_reviews_for_company extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private static final String TAG = "CommentsFragment";


    public Comments_and_reviews_for_company() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment comments_and_reviews_for_company.
     */
    // TODO: Rename and change types and number of parameters
    public static Comments_and_reviews_for_company newInstance(String param1, String param2) {
        Comments_and_reviews_for_company fragment = new Comments_and_reviews_for_company();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_comments_and_reviews_for_company, container, false);
        Button submit = rootView.findViewById(R.id.submitButton);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String commentText = ((EditText) rootView.findViewById(R.id.commentEditText)).getText().toString();
                float rating = ((RatingBar) rootView.findViewById(R.id.ratingBar)).getRating();

                FirebaseUser user=mAuth.getCurrentUser();


                if(user!=null)
                {


                    // Create a new comment object
                    Comment comment = new Comment(generateUniqueId(),commentText, getCurrentDate(), (int) rating,"Firma1",user.getEmail());

                    // Save the comment to Firebase
                    saveCommentToFirebase(comment);
                    Toast.makeText(getContext(), "Comment submitted!", Toast.LENGTH_SHORT).show();
                    getParentFragmentManager().popBackStack();
                }
            }
        });

        return rootView;
    }

    private void saveCommentToFirebase(Comment comment) {
        // Get the Firestore instance
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // Create a new document with a generated ID
        db.collection("comments")
                .add(comment)
                .addOnSuccessListener(documentReference -> {
                    // Comment added successfully
                    Log.d(TAG, "Comment added with ID: " + documentReference.getId());
                    // You can also display a success message or navigate to another screen
                })
                .addOnFailureListener(e -> {
                    // Handle any errors
                    Log.e(TAG, "Error adding comment", e);
                    // You can also display an error message to the user
                });
    }

    private String getCurrentDate() {
        // Get current date and time
        Calendar calendar = Calendar.getInstance();

        // Format the date as "YYYY-MM-DD"
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }
    private int generateUniqueId() {
        // Get current timestamp
        long timestamp = System.currentTimeMillis();

        // Convert timestamp to int (you may need a long if your application generates IDs very frequently)
        int uniqueId = (int) timestamp;

        return uniqueId;
    }
}