package com.example.eventplaner.fragments.management;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.eventplaner.R;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventSubcategory;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FragmentAddProduct extends Fragment {

    private FirebaseFirestore mDatabase;
    private List<EventCategory> categories;
    private List<EventSubcategory> subcategories;
    private Spinner spinnerCategory;
    private Spinner spinnerSubcategory;

    public FragmentAddProduct() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_product, container, false);
        mDatabase = FirebaseFirestore.getInstance();

        // Initialize lists for categories and subcategories
        categories = new ArrayList<>();
        subcategories = new ArrayList<>();

        // Find UI elements by their IDs
        spinnerCategory = view.findViewById(R.id.spinnerCategory);
        spinnerSubcategory = view.findViewById(R.id.spinnerSubcategory);
        EditText editTextProductName = view.findViewById(R.id.editTextProductName);
        EditText editTextProductDescription = view.findViewById(R.id.editTextProductDescription);
        EditText editTextProductPrice = view.findViewById(R.id.editTextProductPrice);
        EditText editTextProductDiscount = view.findViewById(R.id.editTextProductDiscount);
        CheckBox checkBoxVisibility = view.findViewById(R.id.checkBoxVisibility);
        CheckBox checkBoxAvailability = view.findViewById(R.id.checkBoxAvailability);

        // Find the "Add Product" button
        Button addButton = view.findViewById(R.id.buttonAddProduct);
        loadCategories();
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String category = spinnerCategory.getSelectedItem() != null ? spinnerCategory.getSelectedItem().toString() : "";
                    String subcategory = spinnerSubcategory.getSelectedItem() != null ? spinnerSubcategory.getSelectedItem().toString() : "";
                    String name = editTextProductName.getText().toString();
                    String description = editTextProductDescription.getText().toString();
                    double price = Double.parseDouble(editTextProductPrice.getText().toString());
                    double discount = Double.parseDouble(editTextProductDiscount.getText().toString());
                    boolean available = checkBoxAvailability.isChecked();
                    boolean visible = checkBoxVisibility.isChecked();

                    addProductToDatabase(category, subcategory, name, description, price, discount, available, visible);
                } catch (Exception e) {
                    Log.e("FragmentAddProduct", "Error in onClick: " + e.getMessage());
                }
            }
        });

        return view;
    }

    private void loadCategories() {
        mDatabase.collection("eventCategories").get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            categories.clear();
                            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                EventCategory category = documentSnapshot.toObject(EventCategory.class);
                                categories.add(category);
                            }
                            populateCategoriesSpinner();
                        } else {
                            Log.d("Firestore", "No categories found");
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Firestore", "Error fetching categories: " + e.getMessage());
                    }
                });
    }


    private void populateCategoriesSpinner() {
        List<String> categoryNames = new ArrayList<>();
        for (EventCategory category : categories) {
            categoryNames.add(category.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, categoryNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(adapter);

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                loadSubcategories();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing
            }
        });
    }

    private void loadSubcategories() {
        mDatabase.collection("eventSubcategories").get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            subcategories.clear();
                            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                EventSubcategory subcategory = documentSnapshot.toObject(EventSubcategory.class);
                                subcategories.add(subcategory);
                            }
                            populateSubcategoriesSpinner();
                        } else {
                            Log.d("Firestore", "No subcategories found");
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Firestore", "Error fetching subcategories: " + e.getMessage());
                    }
                });
    }



    private void populateSubcategoriesSpinner() {
        List<String> subcategoryNames = new ArrayList<>();
        for (EventSubcategory subcategory : subcategories) {
            subcategoryNames.add(subcategory.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, subcategoryNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSubcategory.setAdapter(adapter);
    }

    private void addProductToDatabase(String category, String subcategory, String name, String description, double price, double discount, boolean available, boolean visible) {
        try {
            CollectionReference productsRef = mDatabase.collection("products");

            Map<String, Object> productData = new HashMap<>();
            productData.put("category", category);
            productData.put("subcategory", subcategory);
            productData.put("name", name);
            productData.put("description", description);
            productData.put("price", price);
            productData.put("discount", discount);
            productData.put("available", available);
            productData.put("visible", visible);

            productsRef.add(productData)
                    .addOnSuccessListener(documentReference -> Log.d("Firestore", "Product added successfully"))
                    .addOnFailureListener(e -> Log.e("Firestore", "Error adding product: " + e.getMessage()));
        } catch (Exception e) {
            Log.e("Firestore", "Error adding product: " + e.getMessage());
        }
    }

}
