package com.example.eventplaner.fragments.pup;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.eventplaner.R;
import com.example.eventplaner.fragments.event.CreateEventFragment;
import com.example.eventplaner.model.Employee;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WorkCalendarEmployeeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WorkCalendarEmployeeFragment extends Fragment {

    private LinearLayout parentLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =inflater.inflate(R.layout.fragment_work_calendar_employee, container, false);
        parentLayout = rootView.findViewById(R.id.parent_layout_employees_event);
        for(int i=0;i<5;i++)
        {
            addCardView(i);
        }
        Button addEvent=rootView.findViewById(R.id.button_add_event_employee);
        addEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateEventFragment nextFragment = new CreateEventFragment();
                FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.logged_in_fragment_container, nextFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        return rootView;
    }

    private void addCardView(int i) {
        LayoutInflater inflater = LayoutInflater.from(requireContext());
        View cardViewLayout = inflater.inflate(R.layout.fragment_event_card_for_employee, parentLayout, false);
        TextView tvName=cardViewLayout.findViewById(R.id.eventNameTextView);
        TextView tvDate=cardViewLayout.findViewById(R.id.eventDateTextView);
        TextView tvDuration=cardViewLayout.findViewById(R.id.eventDurationTextView);
        TextView tvType=cardViewLayout.findViewById(R.id.eventTypeTextView);
        tvName.setText("Name "+i);
        tvDate.setText("28/8/2024");
        tvDuration.setText("15:00 - 16:00");
        tvType.setText("Type "+i);


        parentLayout.addView(cardViewLayout);
    }
}