package com.example.eventplaner.fragments.event.guest_list;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.example.eventplaner.R;
import com.example.eventplaner.model.Guest;
import com.example.eventplaner.viewmodels.GuestViewModel;
import com.google.android.material.materialswitch.MaterialSwitch;
import com.google.android.material.textfield.TextInputEditText;

public class NewGuestListFragment extends Fragment {

    private GuestViewModel viewModel;
    private DialogFragment dialogFragment;


    public NewGuestListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(GuestViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_guest_list, container, false);

        dialogFragment = (DialogFragment) getParentFragment();

        TextInputEditText fullName = view.findViewById(R.id.editTextGuestFullName);
        TextInputEditText note = view.findViewById(R.id.editTextGuestNote);
        AutoCompleteTextView age = view.findViewById(R.id.editTextGuestAge);
        MaterialSwitch invited = view.findViewById(R.id.switchInvited);
        MaterialSwitch inviteAccepted = view.findViewById(R.id.switchInviteAccepted);

        String[] options = {"0-3","3-10","10-18","18-30","30-50","50-70","70+"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getContext()
                ,android.R.layout.simple_dropdown_item_1line,
                options);
        age.setAdapter(adapter);


        Button addGuest = view.findViewById(R.id.addNewGuest);

        addGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.addGuest(new Guest(
                        fullName.getText().toString(),
                        age.getText().toString(),
                        invited.isChecked(),
                        inviteAccepted.isChecked(),
                        note.getText().toString()
                ));
                dialogFragment.dismiss();
            }
        });



        return view;
    }
}