package com.example.eventplaner.fragments.event.agenda;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.AgendaViewAdapter;
import com.example.eventplaner.adapters.ItemViewAdapter;
import com.example.eventplaner.model.Agenda;
import com.example.eventplaner.viewmodels.AgendaViewModel;
import com.example.eventplaner.viewmodels.BudgetViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;


public class AgendaCreationFragment extends Fragment {


    private AgendaViewModel viewModel;
    private NewAgendaDialogFragment dialogFragment;

    public AgendaCreationFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(AgendaViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_agenda_creation, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.agenda_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        viewModel.getData().observe(getViewLifecycleOwner(), agenda -> {
            recyclerView.setAdapter(new AgendaViewAdapter(getContext(),agenda));
        });


        FloatingActionButton addAgenda = view.findViewById(R.id.fab_add_agenda);

        addAgenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNewAgendaDialog();
            }
        });



        return view;
    }

    private void showNewAgendaDialog() {
        dialogFragment = new NewAgendaDialogFragment();
        dialogFragment.show(getParentFragmentManager(), "NewAgendaDialog");
    }
}