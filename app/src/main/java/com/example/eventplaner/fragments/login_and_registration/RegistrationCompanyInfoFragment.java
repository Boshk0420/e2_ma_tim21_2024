package com.example.eventplaner.fragments.login_and_registration;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eventplaner.R;
import com.example.eventplaner.model.dto.CompnyRegistrationDto;
import com.example.eventplaner.model.dto.UserRegistrationDto;
import com.example.eventplaner.model.users.User;


public class RegistrationCompanyInfoFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration_company_info, container, false);
        Button selectCategoryButton = view.findViewById(R.id.companyInfoNextButton);
        selectCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegistrationCompanyCategoryFragment nextFragment = new RegistrationCompanyCategoryFragment();

                EditText companyName = view.findViewById(R.id.companyInfoName);
                EditText companyDescription = view.findViewById(R.id.companyInfoDescription);
                EditText companyEmail = view.findViewById(R.id.companyInfoEmail);
                EditText companyAddress = view.findViewById(R.id.companyInfoAddress);
                EditText companyPhone = view.findViewById(R.id.companyInfoPhone);

                String companyNameData = companyName.getText().toString();
                String companyDescriptionData = companyDescription.getText().toString();
                String companyEmailData = companyEmail.getText().toString();
                String companyAddressData = companyAddress.getText().toString();
                String companyPhoneData = companyPhone.getText().toString();

                UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
                CompnyRegistrationDto companyRegistrationDto = new CompnyRegistrationDto(companyNameData, companyDescriptionData, companyEmailData, companyPhoneData, companyAddressData);

                Bundle bundle = getArguments();
                if (bundle != null) {
                    userRegistrationDto= (UserRegistrationDto) bundle.getSerializable("userRegistration");
                }
                Bundle newDataBundle = new Bundle();
                newDataBundle.putSerializable("userRegistration", userRegistrationDto);
                newDataBundle.putSerializable("companyRegistration", companyRegistrationDto);

                if(companyNameData.isEmpty() || companyDescriptionData.isEmpty() || companyEmailData.isEmpty() || companyAddressData.isEmpty() ||companyPhoneData.isEmpty()){
                    Toast.makeText(getContext(), "Please fill all fields.",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                    FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    nextFragment.setArguments(newDataBundle);
                    transaction.replace(R.id.notLogedInLayout, nextFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }

            }
        });

        return view;
    }
}