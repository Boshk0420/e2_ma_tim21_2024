package com.example.eventplaner.fragments.management;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.eventplaner.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FragmentAddPackage extends Fragment {

    private FirebaseFirestore mDatabase;
    private Spinner spinnerCategory;
    private EditText editTextPackageName;
    private EditText editTextPackageDescription;
    private EditText editTextPackagePrice;
    private EditText editTextPackageDiscount;
    private CheckBox checkBoxVisibility;
    private CheckBox checkBoxAvailability;

    public FragmentAddPackage() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_package, container, false);
        mDatabase = FirebaseFirestore.getInstance();

        // Find UI elements by their IDs
        spinnerCategory = view.findViewById(R.id.spinnerPackageCategory);
        editTextPackageName = view.findViewById(R.id.editTextPackageName);
        editTextPackageDescription = view.findViewById(R.id.editTextPackageDescription);
        editTextPackagePrice = view.findViewById(R.id.editTextPackagePrice);
        editTextPackageDiscount = view.findViewById(R.id.editTextPackageDiscount);
        checkBoxVisibility = view.findViewById(R.id.checkBoxPackageVisibility);
        checkBoxAvailability = view.findViewById(R.id.checkBoxPackageAvailability);

        // Find the "Add Package" button
        Button addButton = view.findViewById(R.id.buttonSubmitPackage);
        loadCategories();
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String category = spinnerCategory.getSelectedItem() != null ? spinnerCategory.getSelectedItem().toString() : "";
                    String name = editTextPackageName.getText().toString();
                    String description = editTextPackageDescription.getText().toString();
                    double price = Double.parseDouble(editTextPackagePrice.getText().toString());
                    double discount = Double.parseDouble(editTextPackageDiscount.getText().toString());
                    boolean available = checkBoxAvailability.isChecked();
                    boolean visible = checkBoxVisibility.isChecked();

                    addPackageToDatabase(category, name, description, price, discount, available, visible);
                } catch (Exception e) {
                    Log.e("FragmentAddPackage", "Error in onClick: " + e.getMessage());
                }
            }
        });

        return view;
    }

    private void loadCategories() {
        mDatabase.collection("eventCategories").get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            List<String> categoryNames = new ArrayList<>();
                            for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                String categoryName = documentSnapshot.getString("name");
                                categoryNames.add(categoryName);
                            }
                            populateCategoriesSpinner(categoryNames);
                        } else {
                            Log.d("Firestore", "No categories found");
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Firestore", "Error fetching categories: " + e.getMessage());
                    }
                });
    }

    private void populateCategoriesSpinner(List<String> categoryNames) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, categoryNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(adapter);
    }

    private void addPackageToDatabase(String category, String name, String description, double price, double discount, boolean available, boolean visible) {
        try {
            CollectionReference packagesRef = mDatabase.collection("packages");

            Map<String, Object> packageData = new HashMap<>();
            packageData.put("category", category);
            packageData.put("name", name);
            packageData.put("description", description);
            packageData.put("price", price);
            packageData.put("discount", discount);
            packageData.put("available", available);
            packageData.put("visible", visible);

            packagesRef.add(packageData)
                    .addOnSuccessListener(documentReference -> Log.d("Firestore", "Package added successfully"))
                    .addOnFailureListener(e -> Log.e("Firestore", "Error adding package: " + e.getMessage()));
        } catch (Exception e) {
            Log.e("Firestore", "Error adding package: " + e.getMessage());
        }
    }
}
