package com.example.eventplaner.fragments.event.agenda;

import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.eventplaner.R;
import com.example.eventplaner.model.Agenda;
import com.example.eventplaner.viewmodels.AgendaViewModel;
import com.example.eventplaner.viewmodels.BudgetViewModel;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;


public class NewAgendaFragment extends Fragment {

    private AgendaViewModel viewModel;

    private DialogFragment dialogFragment;

    public NewAgendaFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(AgendaViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_agenda, container, false);

        dialogFragment = (DialogFragment) getParentFragment();

        TextInputEditText name = view.findViewById(R.id.editTextAgendaName);
        TextInputEditText description = view.findViewById(R.id.editTextAgendaDescription);
        TextInputEditText from = view.findViewById(R.id.editTextAgendaTimeFrom);
        TextInputEditText to = view.findViewById(R.id.editTextAgendaTimeTo);

        Button button = view.findViewById(R.id.addNewAgenda);

        onClickShowTimePicker(from);
        onClickShowTimePicker(to);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                String fromStr = "2024-06-01 " + from.getText();
                String toStr = "2024-06-01 " + to.getText();

                try {
                    viewModel.addAgenda(new Agenda(
                            name.getText().toString(),
                            description.getText().toString(),
                            dateTimeFormat.parse(fromStr),
                            dateTimeFormat.parse(toStr)

                    ));
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
                dialogFragment.dismiss();
            }
        });




        return view;
    }

    private void onClickShowTimePicker(TextInputEditText eventTime){
        eventTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create and show time picker
                MaterialTimePicker timePicker = new MaterialTimePicker.Builder()
                        .setTimeFormat(TimeFormat.CLOCK_24H)
                        .build();
                timePicker.addOnPositiveButtonClickListener(view -> {
                    // Update EditText with selected time
                    eventTime.setText(String.format("%02d:%02d", timePicker.getHour(), timePicker.getMinute()));
                    eventTime.setError(null);
                });
                timePicker.show(getParentFragmentManager(), "TIME_PICKER");
            }
        });
    }
}