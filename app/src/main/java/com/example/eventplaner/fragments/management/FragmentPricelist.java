package com.example.eventplaner.fragments.management;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.example.eventplaner.R;
import com.example.eventplaner.adapters.PricelistAdapter;
import com.example.eventplaner.model.Pricelist;
import com.example.eventplaner.model.Product;
import com.example.eventplaner.model.Service;
import com.example.eventplaner.model.Packages;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.List;

public class FragmentPricelist extends Fragment {
    private FirebaseFirestore mDatabase;
    private RecyclerView mRecyclerView;
    private PricelistAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = FirebaseFirestore.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pricelist, container, false);

        mRecyclerView = view.findViewById(R.id.listViewPricelist);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new PricelistAdapter(new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);

        loadItems();

        return view;
    }

    private void loadItems() {
        mDatabase.collection("services").get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<Pricelist> itemList = new ArrayList<>();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            Service service = document.toObject(Service.class);
                            itemList.add(new Pricelist((int) service.getDiscount(), service.getName(), service.getPrice(), service.getDiscount(), service.getPrice()));
                        }
                        loadProducts(itemList);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to load services: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void loadProducts(List<Pricelist> itemList) {
        mDatabase.collection("products").get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            Product product = document.toObject(Product.class);
                            itemList.add(new Pricelist((int) product.getDiscount(), product.getName(), product.getPrice(), product.getDiscount(), product.getPrice()));
                        }
                        loadPackages(itemList);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to load products: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void loadPackages(List<Pricelist> itemList) {
        mDatabase.collection("packages").get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            Packages packageItem = document.toObject(Packages.class);
                            itemList.add(new Pricelist((int) packageItem.getDiscount(), packageItem.getName(), packageItem.getPrice(), packageItem.getDiscount(), packageItem.getPrice()));
                        }
                        mAdapter.updateItemList(itemList);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to load packages: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}

