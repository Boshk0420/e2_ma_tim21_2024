package com.example.eventplaner.fragments.pup;

import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.eventplaner.R;
import com.example.eventplaner.fragments.login_and_registration.LoginFragment;
import com.example.eventplaner.model.Employee;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EmployeeManagementFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmployeeManagementFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    private LinearLayout parentLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_employee_management, container, false);
        parentLayout = rootView.findViewById(R.id.parent_layout_employees);

        List<Employee> employees=generateEmployees();

        for (Employee e : employees) {
            addCardView(e);
        }

        Button finishRegistrationButton = rootView.findViewById(R.id.button_worker_registration);

        finishRegistrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PupRegistrationFragment nextFragment = new PupRegistrationFragment();
                FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.logged_in_fragment_container, nextFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        ImageButton toggleSearchButton = rootView.findViewById(R.id.toggleSearchButton);
        LinearLayout searchFieldsLayout = rootView.findViewById(R.id.searchFieldsLayout);

        toggleSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchFieldsLayout.getVisibility() == View.GONE) {
                    searchFieldsLayout.setVisibility(View.VISIBLE);
                    toggleSearchButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_up));
                } else {
                    searchFieldsLayout.setVisibility(View.GONE);
                    toggleSearchButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_down));
                }
            }
        });
        return rootView;
    }

    private void addCardView(Employee e) {
        LayoutInflater inflater = LayoutInflater.from(requireContext());
        View cardViewLayout = inflater.inflate(R.layout.fragment_employee_management_card, parentLayout, false);
        TextView tvName=cardViewLayout.findViewById(R.id.textView_employee_name);
        TextView tvEmail=cardViewLayout.findViewById(R.id.textView_employee_email);
        tvName.setText(e.getFirstName()+" "+e.getLastName());
        tvEmail.setText(e.getEmail());
        Button moreDetails = cardViewLayout.findViewById(R.id.button_employee_details);
        moreDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmployeeDetailsFragment nextFragment = new EmployeeDetailsFragment();
                FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.logged_in_fragment_container, nextFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        parentLayout.addView(cardViewLayout);
    }
    private static long lastGeneratedId = 0; // Track the last generated ID
    private static final String[] FIRST_NAMES = {"John", "Alice", "Michael", "Emma", "David", "Sarah", "Daniel", "Olivia"};
    private static final String[] LAST_NAMES = {"Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson"};

    public static List<Employee> generateEmployees() {
        List<Employee> employees = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < 5; i++) {
            long id = ++lastGeneratedId; // Increment the last generated ID for uniqueness
            String firstName = FIRST_NAMES[random.nextInt(FIRST_NAMES.length)];
            String lastName = LAST_NAMES[random.nextInt(LAST_NAMES.length)];
            String email = firstName.toLowerCase() + "." + lastName.toLowerCase() + id + "@example.com";
            String password = "password" + id;
            String confirmPassword = "password" + id;
            String homeAddress = "123 Main Street, City";
            String phoneNumber = "123-456-789" + i;
            // Dummy profile photo (you can replace this with actual photo data)
            byte[] profilePhoto = new byte[0];

            Employee employee = new Employee(id, email, password, confirmPassword, firstName, lastName, homeAddress, phoneNumber, profilePhoto);
            employees.add(employee);
        }

        return employees;
    }
}