package com.example.eventplaner.fragments.event.categories;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.SubcategorieSugestion;
import com.example.eventplaner.model.dto.EventCategoryDto;
import com.example.eventplaner.model.dto.EventSubcategoryDto;
import com.example.eventplaner.model.dto.SubcategorieSuggestionDto;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class SubcategorySugestionEditFragment extends Fragment {

    private static FirebaseFirestore db;

    public SubcategorySugestionEditFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subcategory_sugestion_edit, container, false);
        db = FirebaseFirestore.getInstance();
        Bundle bundle = getArguments();
        SubcategorieSuggestionDto suggestionDto = (SubcategorieSuggestionDto) bundle.getSerializable("suggestion");


        EditText editName = view.findViewById(R.id.edit_suggestion_name);
        EditText editDescription = view.findViewById(R.id.edit_suggestion_description);
        TextView categoryName = view.findViewById(R.id.category_name_headline);
        Button confirmButton = view.findViewById(R.id.confirm_suggestion);

        db.collection("eventCategories")
                .document(suggestionDto.getCategoryId())
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        categoryName.setText(documentSnapshot.getString("name"));
                    }
                });

        editName.setText(suggestionDto.getName());
        editDescription.setText(suggestionDto.getDescription());


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suggestionDto.setName(editName.getText().toString());
                suggestionDto.setDescription(editDescription.getText().toString());

                createNewSubcategory(suggestionDto);

                SubcategorySugestionsFragment nextFragment = new SubcategorySugestionsFragment();
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.navigation_fragment_container, nextFragment);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });


        return view;
    }

    public static void createNewSubcategory(SubcategorieSuggestionDto suggestion){
        EventSubcategoryDto eventSubcategoryDto = new EventSubcategoryDto();
        eventSubcategoryDto.setName(suggestion.getName());
        eventSubcategoryDto.setDescription(suggestion.getDescription());
        eventSubcategoryDto.setCategoryId(suggestion.getCategoryId());
        db.collection("eventSubcategories")
                .add(eventSubcategoryDto)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                        deleteSubcategorySuggestion(suggestion);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });
    }

    private static void deleteSubcategorySuggestion(SubcategorieSuggestionDto suggestion) {
        String documentId = suggestion.getId();

        db.collection("subcategorySuggestions")
                .document(documentId)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully deleted from Firestore");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error deleting document from Firestore", e);
                    }
                });
    }
}