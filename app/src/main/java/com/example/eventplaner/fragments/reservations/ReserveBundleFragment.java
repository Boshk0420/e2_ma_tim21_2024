package com.example.eventplaner.fragments.reservations;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.EmployeeChooserAdapter;
import com.example.eventplaner.adapters.EventChooserAdapter;
import com.example.eventplaner.adapters.ProductReservationAdapter;
import com.example.eventplaner.adapters.ServiceReservationAdapter;
import com.example.eventplaner.model.Budget;
import com.example.eventplaner.model.BudgetSpent;
import com.example.eventplaner.model.ServiceReservation;
import com.example.eventplaner.model.company.Days;
import com.example.eventplaner.model.company.WorkingHours;
import com.example.eventplaner.model.dto.EventDto;
import com.example.eventplaner.model.mocked.BundleMock;
import com.example.eventplaner.model.mocked.ItemMock;
import com.example.eventplaner.model.mocked.ProductMock;
import com.example.eventplaner.model.mocked.ServiceMocked;
import com.example.eventplaner.model.users.UnavailableHours;
import com.example.eventplaner.model.users.User;
import com.example.eventplaner.utils.NotificationSender;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ReserveBundleFragment extends Fragment {

    private BundleMock bundle;
    private RecyclerView eventRecyclerView;
    private RecyclerView productRecyclerView;
    private RecyclerView serviceRecyclerView;
    private EventChooserAdapter eventAdapter;
    private FirebaseFirestore db;
    private EventDto selectedEvent;
    private Budget eventBudget;
    NotificationSender notificationSender;

    private List<ServiceReservation> reservations;
    public ReserveBundleFragment(BundleMock bundle) {
        this.bundle = bundle;
        db = FirebaseFirestore.getInstance();
        this.selectedEvent = null;
        reservations = new ArrayList<>();
        this.notificationSender = new NotificationSender();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reserve_bundle, container, false);


        eventRecyclerView = view.findViewById(R.id.recyclerViewEvent);
        eventRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        fetchEvents();
        productRecyclerView = view.findViewById(R.id.recyclerViewProducts);
        productRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        // Find the LinearLayout for services
        LinearLayout layoutServices = view.findViewById(R.id.layoutServices);

        // Iterate through each service in the bundle and dynamically add views for service items
        for (ServiceMocked service : bundle.getServices()) {
            // Inflate service item layout
            View serviceItemView = LayoutInflater.from(getContext()).inflate(R.layout.service_bundle_view, layoutServices, false);

            // Find views in service item layout
            TextView textViewServiceName = serviceItemView.findViewById(R.id.textViewServiceName);
            TextView textViewServicePrice = serviceItemView.findViewById(R.id.textViewServicePrice);
            RecyclerView recyclerViewEmployees = serviceItemView.findViewById(R.id.recyclerViewEmployees);
            Spinner dateChooser = serviceItemView.findViewById(R.id.spinnerDates);

            // Set service name and price
            textViewServiceName.setText(service.getName());
            textViewServicePrice.setText("Price: " + service.getPrice()); // Assuming getPrice() returns a string or formatted price

            // Set up RecyclerView for employees
            recyclerViewEmployees.setLayoutManager(new LinearLayoutManager(getContext()));
            EmployeeChooserAdapter employeeAdapter = new EmployeeChooserAdapter(service.getEmployees());
            recyclerViewEmployees.setAdapter(employeeAdapter);

            // Populate the spinner with dates
            // Get your list of Date objects here


            dateChooser.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    selectedEvent = eventAdapter.getSelectedEvent();
                    User selectedEmployee = employeeAdapter.getSelectedEmployee();
                    if(selectedEvent != null && selectedEmployee != null)
                    {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            loadDatesIntoSpinner(dateChooser, selectedEmployee, service);
                            //getAvailableDates(selectedEvent, selectedEmployee, service);
                        }
                        return false;
                    }
                    return false;
                }
            });

            dateChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ServiceReservation reservation = new ServiceReservation();
                    User selectedEmployee = employeeAdapter.getSelectedEmployee();
                    reservation.setEmployee(selectedEmployee);
                    reservation.setDate(parseDate((String) parent.getItemAtPosition(position)));
                    reservation.setApproved(false);
                    reservation.setService(service);
                    reservation.setEvent(selectedEvent);

                    for(ServiceReservation res : reservations)
                    {
                        if(res.getService().equals(reservation.getService()))
                        {
                            reservations.add(reservation);
                            reservations.remove(res);
                            return;
                        }
                    }
                    reservations.add(reservation);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            // Add service item view to layoutServices
            layoutServices.addView(serviceItemView);
        }

        ProductReservationAdapter productReservationAdapter = new ProductReservationAdapter(bundle.getProducts());
        productRecyclerView.setAdapter(productReservationAdapter);

        Button reserveButton = view.findViewById(R.id.buttonReserve);
        reserveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(ProductMock productMock : bundle.getProducts())
                {
                    updateBudget(selectedEvent.getId(), productMock);
                }

                for(ServiceReservation reservation : reservations){
                    saveReservation(reservation);
                    sendNotificationToEmployee(reservation.getDate().toString(), reservation.getService(), reservation.getEmployee());
                }

                Toast.makeText(getActivity(), "Successfuly reserved", Toast.LENGTH_SHORT).show();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        return view;
    }
    private void sendNotificationToEmployee(String selectedDate, ItemMock service, User selectedEmployee){
        String title = "New service reservation for " + service.getName();

        String body = "You have new reservation for event called " + selectedEvent.getName() + " on the " + selectedDate;

        notificationSender.sendNotificationToUser(selectedEmployee.getMessageToken(), body, title);
    }

    private Date parseDate(String date){
        String selectedDateStr = date;
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy"); // Adjust format as needed

        try {
            Date selectedDate = dateFormat.parse(selectedDateStr);
            return selectedDate;
        } catch (ParseException e) {
            e.printStackTrace();
            // Handle parsing error
        }

        return null;
    }

    private void loadDatesIntoSpinner(Spinner spinner, User selectedEmployee, ServiceMocked service) {
        List<Date> dates = new ArrayList<>();
        dates = getAvailableDates(selectedEvent, selectedEmployee, service);
        List<String> dateStrings = new ArrayList<>();
        for (Date date : dates) {
            dateStrings.add(date.toString());
        }

        ArrayAdapter<String> dateAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, dateStrings);
        dateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dateAdapter);
    }
    private List<Date> getAvailableDates(EventDto event, User employee, ServiceMocked service){
        List<Date> availableDates = new ArrayList<>();
        Date currentDate = new Date();

        long deadlineInMillis = (long) (service.getReservationDeadline() * 24 * 60 * 60 * 1000);
        Date dayAfterDeadline = new Date(currentDate.getTime() + deadlineInMillis);

        if(event.getDate().after(dayAfterDeadline)){
            for(WorkingHours workingHour : employee.getWorksFor().getWorkingHours()) {
                if(getDayOfTheWeek(event.getDate()).equals(workingHour.getDay())) {
                    if(event.getDate().getHours() > workingHour.getEndHour() || event.getDate().getHours() < workingHour.getStartHour()) {
                    } else {
                        for (int i = workingHour.getStartHour(); i < workingHour.getEndHour(); i += (int) service.getDuration()) {
                            boolean isAvailable = true;

                            for (UnavailableHours unavailableHour : employee.getUnavailableHours()) {
                                if(unavailableHour.getDateFrom().getDate() == event.getDate().getDate()){
                                    if (unavailableHour.getDateFrom().getHours() <= i && i < unavailableHour.getDateTo().getHours() ||
                                            unavailableHour.getDateFrom().getHours() <= i + (int) service.getDuration() && i + (int) service.getDuration() < unavailableHour.getDateTo().getHours()) {
                                        isAvailable = false;
                                        break;
                                    }
                                }
                            }

                            if (isAvailable) {
                                Date availableDateFrom = new Date(event.getDate().getTime());
                                Date availableDateTo = event.getDate();
                                availableDateFrom.setHours(i);
                                availableDateFrom.setMinutes(0);
                                availableDateTo.setHours(i + (int) service.getDuration());
                                availableDateTo.setMinutes(0);
                                availableDates.add(availableDateFrom);
                            }
                        }
                    }
                }
            }
            return availableDates;
        } else {
            return null;
        }
    }
    public String getSelectedDateFromSpiner(Spinner spinner) {
        return spinner.getSelectedItem().toString();
    }
    private Days getDayOfTheWeek(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        switch (dayOfWeek) {
            case Calendar.SUNDAY:
                return Days.SUNDAY;
            case Calendar.MONDAY:
                return Days.MONDAY;
            case Calendar.TUESDAY:
                return Days.TUESDAY;
            case Calendar.WEDNESDAY:
                return Days.WEDNESDAY;
            case Calendar.THURSDAY:
                return Days.THURSDAY;
            case Calendar.FRIDAY:
                return Days.FRIDAY;
            case Calendar.SATURDAY:
                return Days.SATURDAY;
        }

        return Days.MONDAY;

    }
    private void fetchEvents() {
        db.collection("events")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<EventDto> events = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String eventId = document.getId();
                            EventDto event = document.toObject(EventDto.class);
                            event.setId(eventId);
                            events.add(event);
                        }
                        eventAdapter = new EventChooserAdapter(events);
                        eventRecyclerView.setAdapter(eventAdapter);
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }

    private void updateBudget(String eventId, ItemMock product) {
        db.collection("budget")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<Budget> budgets = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String budgetId = document.getId();
                            Budget budget = document.toObject(Budget.class);
                            budget.setId(budgetId);
                            budgets.add(budget);
                        }

                        for(Budget budget : budgets)
                        {
                            if(budget.getEventId().equals(eventId))
                            {
                                eventBudget = budget;
                                break;
                            }
                        }

                        List<BudgetSpent> spentBudget = eventBudget.getSpentBudget();

                        for(BudgetSpent spent : spentBudget)
                        {
                            if(spent.getSubCategory().equals(product.getSubcategory().getName())){
                                List<ItemMock> items = spent.getItemList();
                                items.add(product);
                                spent.setItemList(items);
                                spent.setSpent(spent.getSpent() + product.getPrice());
                                updateEventBudget(eventBudget);
                                return;
                            }
                        }
                        BudgetSpent newSpentBudget = new BudgetSpent();
                        newSpentBudget.setSpent(product.getPrice());
                        newSpentBudget.setSubCategory(product.getSubcategory().getName());
                        List<ItemMock> items = new ArrayList<>();
                        items.add(product);
                        newSpentBudget.setItemList(items);

                        List<BudgetSpent> budgetSpents = eventBudget.getSpentBudget();
                        budgetSpents.add(newSpentBudget);

                        eventBudget.setSpentBudget(budgetSpents);
                        updateEventBudget(eventBudget);

                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }

    private void updateEventBudget(Budget budget){
        db.collection("budget")
                .document(budget.getId())
                .update("spentBudget", budget.getSpentBudget())
                .addOnSuccessListener(aVoid -> {
                    Log.d("BudgetUpdate", "Budget updated");
                })
                .addOnFailureListener(e -> {
                    Log.d("NotificationAdapter", "Error updating document", e);
                });
    }

    private void saveReservation(ServiceReservation reservation) {
        // Get a new document reference
        db.collection("reservations")
                .add(reservation)
                .addOnSuccessListener(documentReference -> {
                    Log.d("Firestore", "Reservation added with ID: " + documentReference.getId());
                })
                .addOnFailureListener(e -> {
                    Log.w("Firestore", "Error adding reservation", e);
                });
    }

}