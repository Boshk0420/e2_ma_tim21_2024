package com.example.eventplaner.fragments.pup;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.eventplaner.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EmployeeDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmployeeDetailsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_employee_details, container, false);
        Button workingHours=rootView.findViewById(R.id.buttonWorkHours);
        Button workingCalendar=rootView.findViewById(R.id.buttonViewCalendar);

        workingCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WorkCalendarEmployeeFragment nextFragment = new WorkCalendarEmployeeFragment();
                FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.logged_in_fragment_container, nextFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        workingHours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WorkHoursEmployeeFragment nextFragment = new WorkHoursEmployeeFragment();
                FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.logged_in_fragment_container, nextFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        return rootView;
    }
}