package com.example.eventplaner.fragments.utils;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.NotificationAdapter;
import com.example.eventplaner.adapters.ReadNotificationAdapter;
import com.example.eventplaner.model.util.Notification;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public class ReadNotificationsFragment extends Fragment {


    public ReadNotificationsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private List<Notification> readNotifications = new ArrayList<>();
    private FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    private ReadNotificationAdapter adapter;
    private RecyclerView recyclerView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_read_notifications, container, false);
        recyclerView = view.findViewById(R.id.recyclerViewRead);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        fetchReadNotifications(currentUser.getUid());
        return view;
    }

    private void fetchReadNotifications(String userId) {
        db.collection("notifications")
                .whereEqualTo("userId", userId)
                .whereEqualTo("read", true)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Notification notification = document.toObject(Notification.class);
                            notification.setId(document.getId());
                            readNotifications.add(notification);
                        }
                        // Populate RecyclerView with fetched data
                        adapter = new ReadNotificationAdapter(readNotifications);
                        recyclerView.setAdapter(adapter);
                    } else {
                        // Log error or handle failure
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }
}