package com.example.eventplaner.fragments.budget_planer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.DialogFragment;

import com.example.eventplaner.R;

public class NewItemDialogFragment extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_display, container, false);

        getChildFragmentManager().beginTransaction()
                .replace(R.id.layoutDialog, new CreateBudgetItemFragment())
                .commit();

        return view;
    }

}
