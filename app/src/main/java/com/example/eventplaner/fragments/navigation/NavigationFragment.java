package com.example.eventplaner.fragments.navigation;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplaner.CompanyInfoFragment;
import com.example.eventplaner.LoggedInActivity;
import com.example.eventplaner.NotLogedInActivity;
import com.example.eventplaner.ReportedCommentsFragment;
import com.example.eventplaner.NotLogedInActivity;
import com.example.eventplaner.fragments.MyProfileFragment;
import com.example.eventplaner.fragments.items.FavoriteItemsFragment;
import com.example.eventplaner.fragments.login_and_registration.ConfirmCompanyRegistration;
import com.example.eventplaner.fragments.management.FragmentPackageManagement;
import com.example.eventplaner.fragments.management.FragmentPricelist;
import com.example.eventplaner.fragments.management.FragmentProductManagement;
import com.example.eventplaner.fragments.management.FragmentServiceManagement;
import com.example.eventplaner.fragments.event.categories.HandleEventCategoriesFragment;
import com.example.eventplaner.fragments.event.types.HandleEventTypesFragment;
import com.example.eventplaner.fragments.items.ItemListFragment;
import com.example.eventplaner.R;
import com.example.eventplaner.fragments.event.CreateEventFragment;
import com.example.eventplaner.fragments.pup.EmployeeManagementFragment;
import com.example.eventplaner.fragments.pup.PupRegistrationFragment;
import com.example.eventplaner.fragments.utils.NotificationsFragment;
import com.example.eventplaner.model.users.UserRole;
import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;

public class NavigationFragment extends Fragment {
    FirebaseUser currentUser;
    FirebaseAuth mAuth;
    public NavigationFragment(){
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_navigation, container, false);

        NavigationView navigationView = view.findViewById(R.id.nav_view);
        DrawerLayout drawerLayout = view.findViewById(R.id.drawer_layout);
        NavigationBarView bottomMenu = view.findViewById(R.id.bottom_navigation);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();


        FirebaseFirestore db = FirebaseFirestore.getInstance();


        DocumentReference userRef = db.collection("users").document(currentUser.getUid());
        ListenerRegistration registration = userRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    return;
                }
                navigationView.getMenu().findItem(R.id.drawer_menu_notifications).setVisible(true);

                if (documentSnapshot != null && documentSnapshot.exists()) {
                    String role = documentSnapshot.getString("role");
                    if (role != null && role.equals("Admin")) {

                        navigationView.getMenu().findItem(R.id.drawer_menu_search_event_categories_handler).setVisible(true);
                        navigationView.getMenu().findItem(R.id.drawer_menu_event_types_handler).setVisible(true);
                        navigationView.getMenu().findItem(R.id.drawer_menu_registration_requests).setVisible(true);
                        navigationView.getMenu().findItem(R.id.drawer_menu_package).setVisible(false);
                        navigationView.getMenu().findItem(R.id.drawer_menu_product).setVisible(false);
                        navigationView.getMenu().findItem(R.id.drawer_menu_employee_management).setVisible(false);
                        navigationView.getMenu().findItem(R.id.drawer_menu_service).setVisible(false);
                        navigationView.getMenu().findItem(R.id.drawer_menu_search_services).setVisible(false);
                        bottomMenu.findViewById(R.id.bottom_menu_new_event).setVisibility(View.INVISIBLE);

                    }
                    else if(role != null && role.equals("OD")){
                        navigationView.getMenu().findItem(R.id.drawer_menu_search_event_categories_handler).setVisible(false);
                        navigationView.getMenu().findItem(R.id.drawer_menu_event_types_handler).setVisible(false);
                        navigationView.getMenu().findItem(R.id.drawer_menu_registration_requests).setVisible(false);
                    }
                    else if(role != null && role.equals("PUP"))
                    {
                        navigationView.getMenu().findItem(R.id.drawer_menu_search_event_categories_handler).setVisible(false);
                        navigationView.getMenu().findItem(R.id.drawer_menu_event_types_handler).setVisible(false);
                        navigationView.getMenu().findItem(R.id.drawer_menu_registration_requests).setVisible(false);
                        bottomMenu.findViewById(R.id.bottom_menu_new_event).setVisibility(View.INVISIBLE);
                    }

                }
            }
        });


        RegisterBottomMenuClicks(bottomMenu,drawerLayout);
        RegisterDrawerMenuClicks(navigationView,drawerLayout);

        return view;
    }


    private void RegisterBottomMenuClicks(NavigationBarView navigationView,DrawerLayout drawerLayout){
        navigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();

                if(id == R.id.bottom_menu_drawer){
                    drawerLayout.openDrawer(GravityCompat.START);
                    return true;
                }else if(id == R.id.bottom_menu_new_event){
                    ShowFragmentFullscreen(new CreateEventFragment());
                    return true;
                }else if (id == R.id.bottom_menu_profile){
                    ShowFragmentFullscreen(new MyProfileFragment());
                    return true;
                }
                else{
                    return false;
                }
            }
        });

    }

    private void RegisterDrawerMenuClicks(NavigationView navigationView,DrawerLayout drawerLayout){
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                int id = menuItem.getItemId();

                if(id == R.id.drawer_menu_search_services){
                    ShowFragment(new ItemListFragment());
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                }else if(id == R.id.drawer_menu_employee_management) {
                    ShowFragment(new EmployeeManagementFragment());
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                } else if (id == R.id.drawer_menu_product) {
                    ShowFragment(new FragmentProductManagement());
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                } else if (id == R.id.drawer_menu_service) {
                    ShowFragment(new FragmentServiceManagement());
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                } else if (id == R.id.drawer_menu_package) {
                    ShowFragment(new FragmentPackageManagement());
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                } else if (id == R.id.drawer_menu_pricelist) {
                    ShowFragment(new FragmentPricelist());
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                }
                else if(id == R.id.drawer_menu_search_event_categories_handler){
                    ShowFragment(new HandleEventCategoriesFragment());
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                }
                else if(id == R.id.drawer_menu_event_types_handler){
                    ShowFragment(new HandleEventTypesFragment());
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                }
                else if(id == R.id.drawer_signout){
                    mAuth.signOut();
                    Intent newIntent = new Intent(getContext(), NotLogedInActivity.class);
                    startActivity(newIntent);
                    return true;
                }
                else if(id == R.id.drawer_menu_registration_requests){
                    ShowFragment(new ConfirmCompanyRegistration());
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                }else if(id == R.id.drawer_menu_favorite_items){
                    ShowFragment(new FavoriteItemsFragment());
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                }
                else if(id == R.id.drawer_menu_notifications){
                    ShowFragment(new NotificationsFragment());
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                }
                else if(id == R.id.drawer_menu_company){
                    ShowFragment(new CompanyInfoFragment());
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                }else if(id == R.id.drawer_menu_reported_comments){
                    ShowFragment(new ReportedCommentsFragment());
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                }
                else{
                    return false;
                }
            }
        });
    }

    private void ShowFragment(Fragment fragment){
        getParentFragmentManager().beginTransaction()
                .replace(R.id.navigation_fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void ShowFragmentFullscreen(Fragment fragment){
        getParentFragmentManager().beginTransaction()
                .replace(R.id.logged_in_fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

}