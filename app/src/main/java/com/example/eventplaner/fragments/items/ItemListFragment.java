package com.example.eventplaner.fragments.items;

import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.ItemViewAdapter;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.SubcategoryType;
import com.example.eventplaner.model.company.Company;
import com.example.eventplaner.model.company.Days;
import com.example.eventplaner.model.company.WorkingHours;
import com.example.eventplaner.model.mocked.BundleMock;
import com.example.eventplaner.model.mocked.ItemMock;
import com.example.eventplaner.model.mocked.ProductMock;
import com.example.eventplaner.model.mocked.ServiceMocked;
import com.example.eventplaner.viewmodels.FavoriteItemViewModel;
import com.example.eventplaner.model.users.UnavailableHours;
import com.example.eventplaner.model.users.User;
import com.example.eventplaner.model.users.UserRole;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.search.SearchView;
import com.google.android.material.slider.RangeSlider;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class ItemListFragment extends Fragment {


    private final List<String> tags = new ArrayList<>();

    private List<ItemMock> itemsFiltered = new ArrayList<>();
    private final List<ItemMock> items = new ArrayList<>();

    private FavoriteItemViewModel viewModel;
    public ItemListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(FavoriteItemViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_item_list, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.item_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        items.addAll(initProduct());
        items.addAll(initService());
        items.addAll(initBundle());

        itemsFiltered.addAll(items);

        recyclerView.setAdapter(new ItemViewAdapter(getContext(),itemsFiltered,viewModel));

        ChipGroup chipGroup = view.findViewById(R.id.chipGroup);
        SearchView searchView = view.findViewById(R.id.search_view_items);
        TextInputEditText dateFrom = view.findViewById(R.id.editTextDateFrom);
        TextInputEditText dateTo = view.findViewById(R.id.editTextDateTo);
        RangeSlider rangeSlider = view.findViewById(R.id.priceRangeSlider);
        Chip serviceChip = view.findViewById(R.id.chip_service);
        Chip productChip = view.findViewById(R.id.chip_item);
        Chip bundleChip = view.findViewById(R.id.chip_bundle);


        onClickShowDatePicker(dateFrom);
        onClickShowDatePicker(dateTo);

        searchView.
                getEditText()
                .setOnEditorActionListener(
                        (v, actionId, event) -> {
            if(!searchView.getText().toString().isEmpty()) {
                addChipToChipGroup(searchView.getText(),chipGroup);
                tags.add(searchView.getText().toString());
                searchView.setText("");
            }
            return false;
        });

        searchView.addTransitionListener(new SearchView.TransitionListener() {
            @Override
            public void onStateChanged(@NonNull SearchView searchView, @NonNull SearchView.TransitionState transitionState, @NonNull SearchView.TransitionState transitionState1) {
                if(transitionState1 == SearchView.TransitionState.HIDING)
                {
                    itemsFiltered.clear();
                    itemsFiltered.addAll(items);

                    List<String> selectedTypes = new ArrayList<>();
                    if (productChip.isChecked()) selectedTypes.add("Product");
                    if (serviceChip.isChecked()) selectedTypes.add("Service");
                    if (bundleChip.isChecked()) selectedTypes.add("Bundle");


                    itemsFiltered = itemsFiltered.stream().filter(item ->
                            item.getPrice() >= (double)rangeSlider.getValues().get(0) &&
                            item.getPrice() <= (double)rangeSlider.getValues().get(1) &&
                            selectedTypes.contains(item.getItemType())
                                ).collect(Collectors.toList())

                            ;


                    if(!tags.isEmpty())
                        itemsFiltered = itemsFiltered
                            .stream()
                            .filter(item ->
                                    tags
                                    .stream()
                                    .anyMatch(tag ->
                                            item.getName().toLowerCase().contains(tag.toLowerCase()) ||
                                            item.getDescription().toLowerCase().contains(tag.toLowerCase()) ||
                                            item.getCategory().getName().toLowerCase().contains(tag.toLowerCase()) ||
                                            item.getSubcategory().getName().toLowerCase().contains(tag.toLowerCase()) ||
                                            item.getItemType().toLowerCase().contains(tag.toLowerCase())
                                    )
                            )
                            .collect(Collectors.toList());


                    recyclerView.setAdapter(new ItemViewAdapter(getContext(),itemsFiltered,viewModel));
                }
            }
        });






        return view;
    }

    private void addChipToChipGroup(Editable chipText, ChipGroup chipGroup){
        Chip chip = new Chip(getContext());
        chip.setText(chipText);
        chip.setClickable(true);
        chip.setCloseIconVisible(true);
        registerChipCloseEvent(chip,chipGroup);
        chipGroup.addView(chip);
    }

    private void registerChipCloseEvent(Chip chip,ChipGroup chipGroup){
        chip.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tags.remove(chip.getText().toString());
                chipGroup.removeView(chip);
            }
        });
    }

    private void onClickShowDatePicker(TextInputEditText eventDate){
        eventDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MaterialDatePicker<Long> datePicker = MaterialDatePicker.Builder.datePicker().build();
                datePicker.addOnPositiveButtonClickListener(selection -> {
                    eventDate.setText(datePicker.getHeaderText());
                });
                datePicker.show(getParentFragmentManager(), "DATE_PICKER");
            }
        });
    }

    private List<ProductMock> initProduct() {
        List<ProductMock> list = new ArrayList<>();

        List<String> images1 = new ArrayList<>();
        images1.add("https://www.artnews.com/wp-content/uploads/2020/09/AdobeStock_1557441.jpeg");

        List<String> images2 = new ArrayList<>();
        images2.add("https://ae01.alicdn.com/kf/HTB1SkTQX._rK1Rjy0Fcq6zEvVXa3/Photo-Album-Scrapbook-Loose-leaf-Photos-Albums-Large-Size-Picture-Album-Wedding-Photograph-Baby-Memory-Albums.jpg");


        list.add(new ProductMock(new ItemMock("",
                "Album sa 50 fotografija",
                "Album sa 50 izradjenih fotografija",
                new EventCategory("","Foto i video", "Fotografije i Albumi", new ArrayList<>()),
                new EventSubcategory("","Fotografije i Albumi","Fotografije i Albumi", SubcategoryType.PROIZVOD,""),
                images1,
                new ArrayList<>(),
                2000,
                5,
                true,
                true
                )));

        list.add(new ProductMock(new ItemMock("",
                "Foto book",
                "Foto book sa vasim fotografijama",
                new EventCategory("","Foto i video", "Fotografije i Albumi", new ArrayList<>()),
                new EventSubcategory("","Fotografije i Albumi","Fotografije i Albumi",SubcategoryType.PROIZVOD,""),
                images2,
                new ArrayList<>(),
                5000,
                0,
                true,
                true
                )));

        return list;
    }

    private List<ServiceMocked> initService(){
        List<ServiceMocked> list = new ArrayList<>();

        List<String> images1 = new ArrayList<>();
        images1.add("https://blogs.icrc.org/law-and-policy/wp-content/uploads/sites/102/2022/03/Drone-image-1096x620.jpg");
        List<String> images2 = new ArrayList<>();
        images2.add("https://cdn.tamaggo.com/1667003511745.jpg");

        ItemMock product1 = new ItemMock("",
                "Snimanje dronom",
                "Ovo je snimanje iz vazduha sa dronom",
                new EventCategory("","Foto i video", "Fotografije i Albumi", new ArrayList<>()),
                new EventSubcategory("","Snimanje dronom","Fotografije i Albumi",SubcategoryType.USLUGA,""),
                images1,
                new ArrayList<>(),
                6000,
                0,
                true,
                true
        );

        WorkingHours workingHourMonday1 = new WorkingHours(12, 0, 18, 0, Days.MONDAY);
        WorkingHours workingHourTuesday1 = new WorkingHours(12, 0, 18, 0, Days.TUESDAY);
        WorkingHours workingHourWednesday1 = new WorkingHours(9, 0, 21, 0, Days.WEDNESDAY);
        WorkingHours workingHourThursday1 = new WorkingHours(10, 0, 20, 0, Days.THURSDAY);
        WorkingHours workingHourFriday2 = new WorkingHours(8, 0, 18, 0, Days.FRIDAY);
        WorkingHours workingHourTuesday2 = new WorkingHours(8, 0, 18, 0, Days.TUESDAY);
        WorkingHours workingHourWednesday2 = new WorkingHours(8, 0, 18, 0, Days.WEDNESDAY);
        WorkingHours workingHourThursday2 = new WorkingHours(10, 0, 20, 0, Days.THURSDAY);
        List<WorkingHours> workingHours1 = new ArrayList<>();
        List<WorkingHours> workingHours2 = new ArrayList<>();

        workingHours1.add(workingHourMonday1);
        workingHours1.add(workingHourTuesday1);
        workingHours1.add(workingHourWednesday1);
        workingHours1.add(workingHourThursday1);

        workingHours2.add(workingHourFriday2);
        workingHours2.add(workingHourTuesday2);
        workingHours2.add(workingHourWednesday2);
        workingHours2.add(workingHourThursday2);

        Company company = new Company("asd", "das", "asd", "ads", "asd", null);
        company.setWorkingHours(workingHours1);


        User user1 = new User("Pera", "Peric", "123123", "Ulica123", UserRole.PUPZ, null, null, "foYyIIa3R8Kfnuvk5U6w65:APA91bHL8jkUNqANyTHe9iXSB70Vz177Lq4Y0qxJ4lBE-2rF3ld7-I__b--qTVCY8LvsEKjWJUFoVti95W4MfEUrbiQ2awDSYMb4CIyxKbeaYTBliME77OWyw40aNCRL79LVItMH3a3x");
        UnavailableHours unavailableHour1 = new UnavailableHours(new Date(2024, 6, 26, 17, 0), new Date(2024, 6, 26, 19, 0));
        UnavailableHours unavailableHour2 = new UnavailableHours(new Date(2024, 6, 26, 12, 0), new Date(2024, 6, 26, 13, 0));
        UnavailableHours unavailableHour3 = new UnavailableHours(new Date(2024, 6, 26, 9, 0), new Date(2024, 6, 26, 10, 0));
        UnavailableHours unavailableHour4 = new UnavailableHours(new Date(2024, 6, 26, 15, 0), new Date(2024, 6, 26, 16, 0));
        List<UnavailableHours> unavailableHours1 = new ArrayList<>();
        List<UnavailableHours> unavailableHours2 = new ArrayList<>();

        unavailableHours1.add(unavailableHour1);
        unavailableHours1.add(unavailableHour2);
        unavailableHours2.add(unavailableHour3);
        unavailableHours2.add(unavailableHour4);
        user1.setUnavailableHours(unavailableHours1);
        user1.setWorksFor(company);
        User user2 = new User("Nikola", "Nikolic", "123123", "Ulica123", UserRole.PUPZ, null, null, "foYyIIa3R8Kfnuvk5U6w65:APA91bHL8jkUNqANyTHe9iXSB70Vz177Lq4Y0qxJ4lBE-2rF3ld7-I__b--qTVCY8LvsEKjWJUFoVti95W4MfEUrbiQ2awDSYMb4CIyxKbeaYTBliME77OWyw40aNCRL79LVItMH3a3x");
        user2.setUnavailableHours(unavailableHours2);
        user2.setWorksFor(company);

        List<User> employees = new ArrayList<>();
        employees.add(user1);
        employees.add(user2);

        list.add(new ServiceMocked(product1,"ne radimo praznicima",6000,2,"Novi Sad",employees,12,2,true));

        ItemMock product2 = new ItemMock("",
                "Snimanje kamerom 4k",
                "Ovo je snimanje u 4k rezoluciji",
                new EventCategory("","Foto i video", "Fotografije i Albumi", new ArrayList<>()),
                new EventSubcategory("","Videografija","Fotografije i Albumi",SubcategoryType.USLUGA,""),
                images2,
                new ArrayList<>(),
                7000,
                0,
                true,
                true
        );

        list.add(new ServiceMocked(product2,"/",5000,1,"Novi Sad",employees,12,2,true));

        return list;
    }

    private List<BundleMock> initBundle(){
        List<BundleMock> list = new ArrayList<>();

        List<String> images1 = new ArrayList<>();
        images1.add("https://www.artnews.com/wp-content/uploads/2020/09/AdobeStock_1557441.jpeg");

        ItemMock item1 = new ItemMock("",
                "Snimanje dogadaja",
                "Ovaj paket ukljucuje sve potrebne stavke za dekoraciju vaseg vencanja",
                new EventCategory("","Foto i video", "Fotografije i Albumi", new ArrayList<>()),
                new EventSubcategory("","Videografija","Fotografije i Albumi",SubcategoryType.USLUGA,""),
                images1,
                new ArrayList<>(),
                18000,
                10,
                true,
                true
        );

        list.add(new BundleMock(item1,initProduct(),initService()));

        return list;
    }

}