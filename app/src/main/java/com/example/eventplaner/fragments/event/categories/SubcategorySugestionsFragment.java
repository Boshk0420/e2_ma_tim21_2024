package com.example.eventplaner.fragments.event.categories;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.EventCategoryHandlerAdapter;
import com.example.eventplaner.adapters.SubcategorieSugestionAdapter;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.SubcategorieSugestion;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public class SubcategorySugestionsFragment extends Fragment {

    private RecyclerView recyclerView;
    private SubcategorieSugestionAdapter adapter;
    private List<SubcategorieSugestion> sugestions;

    private FirebaseFirestore db;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subcategory_sugestions, container, false);
        db = FirebaseFirestore.getInstance();
        recyclerView = view.findViewById(R.id.subcategorie_sugestion_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        fetchSuggestions();
        return view;
    }

    private void fetchSuggestions() {
        db.collection("subcategorySuggestions")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<SubcategorieSugestion> suggestions = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String suggestionId = document.getId();
                            SubcategorieSugestion suggestion = document.toObject(SubcategorieSugestion.class);
                            suggestion.setId(suggestionId);
                            suggestions.add(suggestion);
                        }
                        // Populate RecyclerView with fetched data
                        adapter = new SubcategorieSugestionAdapter(suggestions);
                        recyclerView.setAdapter(adapter);
                    } else {
                        // Log error or handle failure
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }


}