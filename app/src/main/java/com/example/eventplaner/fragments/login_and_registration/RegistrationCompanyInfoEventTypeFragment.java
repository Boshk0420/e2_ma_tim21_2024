package com.example.eventplaner.fragments.login_and_registration;

import static android.content.ContentValues.TAG;

import android.media.metrics.Event;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.EventCategoryListAdapter;
import com.example.eventplaner.adapters.EventTypeHandlerAdapter;
import com.example.eventplaner.adapters.EventTypeListAdapter;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventType;
import com.example.eventplaner.model.dto.CompnyRegistrationDto;
import com.example.eventplaner.model.dto.UserRegistrationDto;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;


public class RegistrationCompanyInfoEventTypeFragment extends Fragment {

    private RecyclerView recyclerView;
    private EventTypeListAdapter adapter;
    private List<EventType> types;
    private FirebaseFirestore db;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_registration_company_info_event_type, container, false);
        db = FirebaseFirestore.getInstance();
        recyclerView = view.findViewById(R.id.recycler_event_type_view);

        fetchEventTypes();

        Button nextButton = view.findViewById(R.id.event_type_selection_next_button);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<EventType> selectedEventTypes = adapter.getSelectedEventTypes();

                Bundle bundle = getArguments();
                UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
                CompnyRegistrationDto companyRegistrationDto = new CompnyRegistrationDto();

                if (bundle != null) {
                    userRegistrationDto= (UserRegistrationDto) bundle.getSerializable("userRegistration");
                    companyRegistrationDto = (CompnyRegistrationDto) bundle.getSerializable("companyRegistration");
                }
                companyRegistrationDto.setEventTypes(selectedEventTypes);
                Bundle newDataBundle = new Bundle();
                newDataBundle.putSerializable("userRegistration", userRegistrationDto);
                newDataBundle.putSerializable("companyRegistration", companyRegistrationDto);


                RegistrationCompanyScheduleFragment nextFragment = new RegistrationCompanyScheduleFragment();
                nextFragment.setArguments(newDataBundle);
                FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.notLogedInLayout, nextFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        return view;
    }



    private void fetchEventTypes() {
        db.collection("eventTypes")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<EventType> eventCategories = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String typeId = document.getId();
                            EventType type = document.toObject(EventType.class);
                            type.setId(typeId);
                            eventCategories.add(type);
                        }
                        // Populate RecyclerView with fetched data
                        adapter = new EventTypeListAdapter(eventCategories);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        recyclerView.setAdapter(adapter);
                    } else {
                        // Log error or handle failure
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }
}