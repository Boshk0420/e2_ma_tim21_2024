package com.example.eventplaner.fragments.event.guest_list;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.AgendaViewAdapter;
import com.example.eventplaner.adapters.GuestViewAdapter;
import com.example.eventplaner.viewmodels.GuestViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


public class GuestListFragment extends Fragment {

    private GuestViewModel viewModel;
    private NewGuestListDialogFragment dialogFragment;

    public GuestListFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(GuestViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_guest_list, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.guest_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        viewModel.getData().observe(getViewLifecycleOwner(),guestList ->{
            recyclerView.setAdapter(new GuestViewAdapter(getContext(),guestList));
        });

        FloatingActionButton addGuest = view.findViewById(R.id.fab_add_guest);

        addGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNewGuestDialog();
            }
        });

        return view;
    }

    private void showNewGuestDialog(){
        dialogFragment = new NewGuestListDialogFragment();
        dialogFragment.show(getParentFragmentManager(),"NewGuestDialog");
    }
}