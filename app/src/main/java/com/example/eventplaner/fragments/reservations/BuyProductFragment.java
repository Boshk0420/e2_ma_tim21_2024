package com.example.eventplaner.fragments.reservations;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.EventCategoryHandlerAdapter;
import com.example.eventplaner.adapters.EventChooserAdapter;
import com.example.eventplaner.model.Budget;
import com.example.eventplaner.model.BudgetSpent;
import com.example.eventplaner.model.Event;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.dto.EventDto;
import com.example.eventplaner.model.mocked.ItemMock;
import com.example.eventplaner.model.mocked.ProductMock;
import com.example.eventplaner.model.util.Notification;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public class BuyProductFragment extends Fragment {

    private ProductMock product;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private EventChooserAdapter adapter;
    private RecyclerView recyclerView;
    private Budget eventBudget;
    public BuyProductFragment(ProductMock product) {
        this.product = product;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buy_product, container, false);
        TextView productName = view.findViewById(R.id.product_name);
        TextView originalPrice = view.findViewById(R.id.product_price);
        Button buyButton = view.findViewById(R.id.button_buy);

        recyclerView = view.findViewById(R.id.recycler_view_events);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        fetchEvents();

        productName.setText(product.getName());
        String priceStr = (int)(product.getPrice() - product.getPrice()*product.getDiscount()/100) + "$";
        originalPrice.setText(priceStr);


        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBudgetByEventId(adapter.getSelectedEvent().getId());
                Toast.makeText(getActivity(), "Successfuly bought", Toast.LENGTH_SHORT).show();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });


        return view;
    }


    private void fetchEvents() {
        db.collection("events")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<EventDto> events = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String eventId = document.getId();
                            EventDto event = document.toObject(EventDto.class);
                            event.setId(eventId);
                            events.add(event);
                        }
                        // Populate RecyclerView with fetched data
                        adapter = new EventChooserAdapter(events);
                        recyclerView.setAdapter(adapter);
                    } else {
                        // Log error or handle failure
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }

    private void getBudgetByEventId(String eventId) {
        db.collection("budget")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<Budget> budgets = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String budgetId = document.getId();
                            Budget budget = document.toObject(Budget.class);
                            budget.setId(budgetId);
                            budgets.add(budget);
                        }

                        for(Budget budget : budgets)
                        {
                            if(budget.getEventId().equals(eventId))
                            {
                                eventBudget = budget;
                                break;
                            }
                        }

                        List<BudgetSpent> spentBudget = eventBudget.getSpentBudget();

                        for(BudgetSpent spent : spentBudget)
                        {
                            if(spent.getSubCategory().equals(product.getSubcategory().getName())){
                                List<ItemMock> items = spent.getItemList();
                                items.add(product);
                                spent.setItemList(items);
                                spent.setSpent(spent.getSpent() + product.getPrice());
                                updateEventBudget(eventBudget);
                                return;
                            }
                        }
                        BudgetSpent newSpentBudget = new BudgetSpent();
                        newSpentBudget.setSpent(product.getPrice());
                        newSpentBudget.setSubCategory(product.getSubcategory().getName());
                        List<ItemMock> items = new ArrayList<>();
                        items.add(product);
                        newSpentBudget.setItemList(items);

                        List<BudgetSpent> budgetSpents = eventBudget.getSpentBudget();
                        budgetSpents.add(newSpentBudget);

                        eventBudget.setSpentBudget(budgetSpents);
                        updateEventBudget(eventBudget);

                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }

    private void updateEventBudget(Budget budget){
        db.collection("budget")
                .document(budget.getId())
                .update("spentBudget", budget.getSpentBudget())
                .addOnSuccessListener(aVoid -> {
                    Log.d("BudgetUpdate", "Budget updated");
                })
                .addOnFailureListener(e -> {
                    Log.d("NotificationAdapter", "Error updating document", e);
                });
    }
}