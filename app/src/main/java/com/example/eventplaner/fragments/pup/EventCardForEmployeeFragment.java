package com.example.eventplaner.fragments.pup;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eventplaner.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EventCardForEmployeeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventCardForEmployeeFragment extends Fragment {
    private static final String ARG_EVENT_NAME = "eventName";
    private static final String ARG_EVENT_DATE = "eventDate";
    private static final String ARG_EVENT_DURATION = "eventDuration";
    private static final String ARG_EVENT_TYPE = "eventType";

    private String eventName;
    private String eventDate;
    private String eventDuration;
    private String eventType;

    public EventCardForEmployeeFragment() {
        // Required empty public constructor
    }

    public static EventCardForEmployeeFragment newInstance(String eventName, String eventDate, String eventDuration, String eventType) {
        EventCardForEmployeeFragment fragment = new EventCardForEmployeeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_EVENT_NAME, eventName);
        args.putString(ARG_EVENT_DATE, eventDate);
        args.putString(ARG_EVENT_DURATION, eventDuration);
        args.putString(ARG_EVENT_TYPE, eventType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eventName = getArguments().getString(ARG_EVENT_NAME);
            eventDate = getArguments().getString(ARG_EVENT_DATE);
            eventDuration = getArguments().getString(ARG_EVENT_DURATION);
            eventType = getArguments().getString(ARG_EVENT_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event_card_for_employee, container, false);

        TextView tvName = rootView.findViewById(R.id.eventNameTextView);
        TextView tvDate = rootView.findViewById(R.id.eventDateTextView);
        TextView tvDuration = rootView.findViewById(R.id.eventDurationTextView);
        TextView tvType = rootView.findViewById(R.id.eventTypeTextView);

        tvName.setText(eventName);
        tvDate.setText(eventDate);
        tvDuration.setText(eventDuration);
        tvType.setText(eventType);

        return rootView;
    }
}