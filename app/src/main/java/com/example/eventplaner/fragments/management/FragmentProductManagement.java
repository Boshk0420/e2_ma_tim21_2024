package com.example.eventplaner.fragments.management;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.ProductAdapter;
import com.example.eventplaner.model.Product;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class FragmentProductManagement extends Fragment {

    private FirebaseFirestore mDatabase;
    private RecyclerView mRecyclerView;
    private ProductAdapter mAdapter;
    private EditText mEditTextSearch;
    private Button mButtonSearch;
    private Spinner spinnerCategory;
    private Spinner spinnerSubcategory;
    //private Spinner spinnerAvailability;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = FirebaseFirestore.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_management, container, false);

        mEditTextSearch = view.findViewById(R.id.editTextSearch);
        mButtonSearch = view.findViewById(R.id.buttonSearch);
        mRecyclerView = view.findViewById(R.id.recyclerViewProducts);

        // Popunjavanje kategorija
        spinnerCategory = view.findViewById(R.id.spinnerCategory);
        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, new ArrayList<>());
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(categoryAdapter);

        // Popunjavanje podkategorija
        spinnerSubcategory = view.findViewById(R.id.spinnerSubcategory);
        ArrayAdapter<String> subcategoryAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, new ArrayList<>());
        subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSubcategory.setAdapter(subcategoryAdapter);

        /*spinnerAvailability = view.findViewById(R.id.spinnerAvailability);
        ArrayAdapter<CharSequence> availabilityAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.availability_array, android.R.layout.simple_spinner_item);
        availabilityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAvailability.setAdapter(availabilityAdapter);*/

        mButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchProducts();
            }
        });

        Button buttonAddProduct = view.findViewById(R.id.buttonAddProduct);
        buttonAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragmentAddProduct = new FragmentAddProduct();
                FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
                transaction.replace(R.id.navigation_fragment_container, fragmentAddProduct);
                transaction.commit();
            }
        });

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new ProductAdapter(new ArrayList<Product>());
        mRecyclerView.setAdapter(mAdapter);

        loadProducts();
        loadCategories();
        loadSubcategories();

        return view;
    }

    private void loadProducts() {
        mDatabase.collection("products")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<Product> productList = new ArrayList<>();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            Product product = document.toObject(Product.class);
                            productList.add(product);
                        }
                        mAdapter.updateProductList(productList);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to load products: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void searchProducts() {
        String searchQuery = mEditTextSearch.getText().toString().trim().toLowerCase();
        String selectedCategory = spinnerCategory.getSelectedItem().toString();
        String selectedSubcategory = spinnerSubcategory.getSelectedItem().toString();
        //String selectedAvailability = spinnerAvailability.getSelectedItem().toString();

        // Query Firestore to filter products by name, category, and subcategory
        mDatabase.collection("products")
                .whereEqualTo("name", searchQuery)
                .whereEqualTo("category", selectedCategory)
                .whereEqualTo("subcategory", selectedSubcategory)
                //.whereEqualTo("availability", selectedAvailability)

                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<Product> productList = new ArrayList<>();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            Product product = document.toObject(Product.class);
                            productList.add(product);
                        }
                        mAdapter.updateProductList(productList);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to search products: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void loadCategories() {
        // Dohvati referencu na kolekciju 'categories' iz Firestore-a
        mDatabase.collection("eventCategories")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<String> categoryList = new ArrayList<>();
                        // Iteriraj kroz rezultate upita i dodaj kategorije u listu
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            String category = document.getString("name");
                            categoryList.add(category);
                        }
                        // Ažuriraj spinner sa listom kategorija
                        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, categoryList);
                        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerCategory.setAdapter(categoryAdapter);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Prikazivanje poruke u slučaju neuspjeha
                        Toast.makeText(getActivity(), "Failed to load categories: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void loadSubcategories() {
        // Dohvati referencu na kolekciju 'subcategories' iz Firestore-a
        mDatabase.collection("eventSubcategories")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<String> subcategoryList = new ArrayList<>();
                        // Iteriraj kroz rezultate upita i dodaj podkategorije u listu
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            String subcategory = document.getString("name");
                            subcategoryList.add(subcategory);
                        }
                        // Ažuriraj spinner sa listom podkategorija
                        ArrayAdapter<String> subcategoryAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, subcategoryList);
                        subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerSubcategory.setAdapter(subcategoryAdapter);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Prikazivanje poruke u slučaju neuspjeha
                        Toast.makeText(getActivity(), "Failed to load subcategories: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }



}

