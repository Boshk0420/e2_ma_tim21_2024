package com.example.eventplaner.fragments.reservations;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.EmployeeChooserAdapter;
import com.example.eventplaner.adapters.EventChooserAdapter;
import com.example.eventplaner.model.Budget;
import com.example.eventplaner.model.BudgetSpent;
import com.example.eventplaner.model.ServiceReservation;
import com.example.eventplaner.model.company.Days;
import com.example.eventplaner.model.company.WorkingHours;
import com.example.eventplaner.model.dto.EventDto;
import com.example.eventplaner.model.mocked.ItemMock;
import com.example.eventplaner.model.mocked.ServiceMocked;
import com.example.eventplaner.model.users.UnavailableHours;
import com.example.eventplaner.model.users.User;
import com.example.eventplaner.utils.NotificationSender;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class ReserveServiceFragment extends Fragment {
    private ServiceMocked service;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private EventChooserAdapter adapter;
    private RecyclerView recyclerView;
    private RecyclerView recyclerViewEmployees;
    private EventDto selectedEvent;
    private User selectedEmployee;
    private TextInputEditText dateFrom;
    private Calendar selectedDate;
    private String selectedDateFromSpiner;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
    private Budget eventBudget;

    NotificationSender notificationSender;
    public ReserveServiceFragment(ServiceMocked service) {
        this.service = service;
        this.selectedEmployee = null;
        this.selectedEvent = null;
        this.notificationSender = new NotificationSender();
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reserve_service, container, false);
        TextView serviceName = view.findViewById(R.id.service_name);
        TextView originalPrice = view.findViewById(R.id.product_price);
        Button reserveButton = view.findViewById(R.id.button_reserve);
        Spinner dateChooser = view.findViewById(R.id.spinner_available_dates);




        recyclerView = view.findViewById(R.id.recycler_view_events);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recyclerViewEmployees = view.findViewById(R.id.recycler_view_employees);
        recyclerViewEmployees.setLayoutManager(new LinearLayoutManager(getContext()));
        EmployeeChooserAdapter employeeAdapter = new EmployeeChooserAdapter(service.getEmployees());
        recyclerViewEmployees.setAdapter(employeeAdapter);

        fetchEvents();

        serviceName.setText(service.getName());
        String priceStr = (int)(service.getPrice() - service.getPrice()*service.getDiscount()/100) + "$";
        originalPrice.setText(priceStr);


        dateChooser.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                selectedEvent = adapter.getSelectedEvent();
                selectedEmployee = employeeAdapter.getSelectedEmployee();
                if(selectedEvent != null && selectedEmployee != null)
                {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        loadDatesIntoSpinner(dateChooser);
                        getAvailableDates(selectedEvent, selectedEmployee);
                    }
                    return false;
                }
                return false;
            }
        });

        reserveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedEvent != null && selectedEmployee != null)
                {
                    String selectedDate = getSelectedDateFromSpiner(dateChooser);
                    Date date;
                    try {
                        date = dateFormat.parse(selectedDate);
                    } catch (ParseException e) {
                        throw new RuntimeException(e);
                    }
                    sendNotificationToEmployee(selectedDate);
                    updateBudget(selectedEvent.getId(), service);
                    ServiceReservation reservation = new ServiceReservation();
                    reservation.setService(service);
                    reservation.setDate(date);
                    reservation.setApproved(service.isAcceptAutomatically());
                    reservation.setEvent(selectedEvent);
                    reservation.setEmployee(selectedEmployee);
                    saveReservation(reservation);
                    Toast.makeText(getActivity(), "Successfuly reserved", Toast.LENGTH_SHORT).show();
                    getActivity().getSupportFragmentManager().popBackStack();

                }


            }
        });

        return view;
    }

    private void saveReservation(ServiceReservation reservation) {
        // Get a new document reference
        db.collection("reservations")
                .add(reservation)
                .addOnSuccessListener(documentReference -> {
                    Log.d("Firestore", "Reservation added with ID: " + documentReference.getId());
                })
                .addOnFailureListener(e -> {
                    Log.w("Firestore", "Error adding reservation", e);
                });
    }
    private void sendNotificationToEmployee(String selectedDate){
        String title = "New service reservation for " + service.getName();

        String body = "You have new reservation for event called " + selectedEvent.getName() + " on the " + selectedDate;

        notificationSender.sendNotificationToUser(selectedEmployee.getMessageToken(), body, title);
    }
    private void loadDatesIntoSpinner(Spinner spinner) {
        List<Date> dates = new ArrayList<>();
        dates = getAvailableDates(selectedEvent, selectedEmployee);
        List<String> dateStrings = new ArrayList<>();
        for (Date date : dates) {
            dateStrings.add(date.toString());
        }

        ArrayAdapter<String> dateAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, dateStrings);
        dateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dateAdapter);
    }
    private List<Date> getAvailableDates(EventDto event, User employee){
        List<Date> availableDates = new ArrayList<>();
        Date currentDate = new Date();

        long deadlineInMillis = (long) (service.getReservationDeadline() * 24 * 60 * 60 * 1000);
        Date dayAfterDeadline = new Date(currentDate.getTime() + deadlineInMillis);

        if(event.getDate().after(dayAfterDeadline)){
            for(WorkingHours workingHour : employee.getWorksFor().getWorkingHours()) {
                if(getDayOfTheWeek(event.getDate()).equals(workingHour.getDay())) {
                    if(event.getDate().getHours() > workingHour.getEndHour() || event.getDate().getHours() < workingHour.getStartHour()) {
                    } else {
                        for (int i = workingHour.getStartHour(); i < workingHour.getEndHour(); i += (int) service.getDuration()) {
                            boolean isAvailable = true;

                            for (UnavailableHours unavailableHour : employee.getUnavailableHours()) {
                                if(unavailableHour.getDateFrom().getDate() == event.getDate().getDate()){
                                    if (unavailableHour.getDateFrom().getHours() <= i && i < unavailableHour.getDateTo().getHours() ||
                                            unavailableHour.getDateFrom().getHours() <= i + (int) service.getDuration() && i + (int) service.getDuration() < unavailableHour.getDateTo().getHours()) {
                                        isAvailable = false;
                                        break;
                                    }
                                }
                            }

                            if (isAvailable) {
                                Date availableDateFrom = new Date(event.getDate().getTime());
                                Date availableDateTo = event.getDate();
                                availableDateFrom.setHours(i);
                                availableDateFrom.setMinutes(0);
                                availableDateTo.setHours(i + (int) service.getDuration());
                                availableDateTo.setMinutes(0);
                                availableDates.add(availableDateFrom);
                            }
                        }
                    }
                }
            }
            return availableDates;
        } else {
            return null;
        }
    }
    public String getSelectedDateFromSpiner(Spinner spinner) {
        return spinner.getSelectedItem().toString();
    }
    private Days getDayOfTheWeek(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        switch (dayOfWeek) {
            case Calendar.SUNDAY:
                return Days.SUNDAY;
            case Calendar.MONDAY:
                return Days.MONDAY;
            case Calendar.TUESDAY:
                return Days.TUESDAY;
            case Calendar.WEDNESDAY:
                return Days.WEDNESDAY;
            case Calendar.THURSDAY:
                return Days.THURSDAY;
            case Calendar.FRIDAY:
                return Days.FRIDAY;
            case Calendar.SATURDAY:
                return Days.SATURDAY;
        }

        return Days.MONDAY;

    }

    private void fetchEvents() {
        db.collection("events")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<EventDto> events = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String eventId = document.getId();
                            EventDto event = document.toObject(EventDto.class);
                            event.setId(eventId);
                            events.add(event);
                        }
                        adapter = new EventChooserAdapter(events);
                        recyclerView.setAdapter(adapter);
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }

    private void updateBudget(String eventId, ItemMock product) {
        db.collection("budget")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<Budget> budgets = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String budgetId = document.getId();
                            Budget budget = document.toObject(Budget.class);
                            budget.setId(budgetId);
                            budgets.add(budget);
                        }

                        for(Budget budget : budgets)
                        {
                            if(budget.getEventId().equals(eventId))
                            {
                                eventBudget = budget;
                                break;
                            }
                        }

                        List<BudgetSpent> spentBudget = eventBudget.getSpentBudget();

                        for(BudgetSpent spent : spentBudget)
                        {
                            if(spent.getSubCategory().equals(product.getSubcategory().getName())){
                                List<ItemMock> items = spent.getItemList();
                                items.add(product);
                                spent.setItemList(items);
                                spent.setSpent(spent.getSpent() + product.getPrice());
                                updateEventBudget(eventBudget);
                                return;
                            }
                        }
                        BudgetSpent newSpentBudget = new BudgetSpent();
                        newSpentBudget.setSpent(product.getPrice());
                        newSpentBudget.setSubCategory(product.getSubcategory().getName());
                        List<ItemMock> items = new ArrayList<>();
                        items.add(product);
                        newSpentBudget.setItemList(items);

                        List<BudgetSpent> budgetSpents = eventBudget.getSpentBudget();
                        budgetSpents.add(newSpentBudget);

                        eventBudget.setSpentBudget(budgetSpents);
                        updateEventBudget(eventBudget);

                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }

    private void updateEventBudget(Budget budget){
        db.collection("budget")
                .document(budget.getId())
                .update("spentBudget", budget.getSpentBudget())
                .addOnSuccessListener(aVoid -> {
                    Log.d("BudgetUpdate", "Budget updated");
                })
                .addOnFailureListener(e -> {
                    Log.d("NotificationAdapter", "Error updating document", e);
                });
    }
}