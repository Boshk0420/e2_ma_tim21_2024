package com.example.eventplaner.fragments.budget_planer;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.Budget.BudgetCategoryAdapter;
import com.example.eventplaner.model.Budget;
import com.example.eventplaner.model.BudgetCategory;
import com.example.eventplaner.model.BudgetSubCategory;
import com.example.eventplaner.viewmodels.BudgetViewModel;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;


public class BudgetPlanerFragment extends Fragment {

    private BudgetViewModel viewModel;

    private Budget budget;

    private final FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public BudgetPlanerFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(BudgetViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_budget_planer, container, false);
        ExpandableListView expandableListView = view.findViewById(R.id.expandableListViewBudget);

        viewModel.getData().observe(getViewLifecycleOwner(), newBudget -> {
            budget = newBudget;
            expandableListView.setAdapter(new BudgetCategoryAdapter(budget.getPlanedBudget(),true));
        });




        return view;
    }




}