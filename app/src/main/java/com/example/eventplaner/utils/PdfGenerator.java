/*package com.example.eventplaner.utils;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Environment;

import com.example.eventplaner.model.Pricelist;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class PdfGenerator {
    public static void generatePdf(List<Pricelist> itemList, String fileName) throws IOException {
        PdfDocument pdfDocument = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(300, 600, 1).create();
        PdfDocument.Page page = pdfDocument.startPage(pageInfo);
        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);

        int y = 50; // Initial Y position
        int x = 50; // X position

        // Add items from the list to the PDF document
        for (Pricelist item : itemList) {
            canvas.drawText(item.getName() + ": " + item.getPrice(), x, y, paint);
            y += 50; // Move to the next line
        }

        pdfDocument.finishPage(page);

        // Save the PDF document to external storage
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName);
        pdfDocument.writeTo(new FileOutputStream(file));
        pdfDocument.close();
    }
}
*/

package com.example.eventplaner.utils;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Environment;

import com.example.eventplaner.model.Pricelist;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class PdfGenerator {

    public static void generatePdf(Pricelist item, String fileName) throws IOException {
        // Creating PDF document, adding page, and setting up canvas and paint
        PdfDocument pdfDocument = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(300, 600, 1).create();
        PdfDocument.Page page = pdfDocument.startPage(pageInfo);
        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);

        // Drawing the item on the canvas
        int y = 50; // Initial Y position
        int x = 50; // X position
        canvas.drawText(item.getName() + ": " + item.getPrice(), x, y, paint);

        // Finishing page and saving the PDF document to external storage
        pdfDocument.finishPage(page);
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName);
        pdfDocument.writeTo(new FileOutputStream(file));
        pdfDocument.close();
    }

}
