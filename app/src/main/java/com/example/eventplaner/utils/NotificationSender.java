package com.example.eventplaner.utils;

import static android.content.ContentValues.TAG;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.eventplaner.R;
import com.example.eventplaner.clients.FirebaseUtils;
import com.example.eventplaner.clients.NotificationService;
import com.example.eventplaner.fragments.event.categories.HandleEventCategoriesFragment;
import com.example.eventplaner.model.dto.NotificationDto;
import com.example.eventplaner.model.dto.NotificationInfoDto;
import com.example.eventplaner.model.util.Notification;
import com.example.eventplaner.model.util.NotificationInfo;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationSender {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    public void sendNotificationToAdmins(String body, String title) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users")
                .whereEqualTo("role", "Admin")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String token = document.getString("messageToken");
                            String userId = document.getId();
                            sendNotification(token, body, title);
                            saveNotification(userId, body, title);
                        }
                    } else {
                        Exception e = task.getException();
                        if (e != null) {
                            System.err.println("Error querying Firestore: " + e.getMessage());
                        }
                    }
                });
    }

    public void sendNotificationToPupv(String body, String title) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users")
                .whereEqualTo("role", "PUP")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String token = document.getString("messageToken");
                            sendNotification(token, body, title);
                        }
                    } else {
                        Exception e = task.getException();
                        if (e != null) {
                            System.err.println("Error querying Firestore: " + e.getMessage());
                        }
                    }
                });
    }

    public void sendNotificationToUser(String token, String body, String title)
    {
        sendNotification(token, body, title);
    }


    private void sendNotification(String token, String body, String title) {
        NotificationDto notificationDto = new NotificationDto(token, new NotificationInfoDto(body, title));
        NotificationService notificationService = FirebaseUtils.productService;

        Call<NotificationDto> call = notificationService.sendNotification(notificationDto);

        call.enqueue(new Callback<NotificationDto>() {
            @Override
            public void onResponse(Call<NotificationDto> call, Response<NotificationDto> response) {
                if (response.isSuccessful()) {
                    Log.d("NOT", "Notification sent");
                } else {
                    Log.d("NOT", "Notification not sent");

                }
            }

            @Override
            public void onFailure(Call<NotificationDto> call, Throwable t) {
                Log.d("NOT", "Notification fail");
            }
        });
    }

    private void saveNotification(String userId, String body, String title){
        Notification notification = new Notification();
        NotificationInfo notificationInfo = new NotificationInfo(body, title);
        notification.setRead(false);
        notification.setNotificationInfo(notificationInfo);
        notification.setUserId(userId);


        db.collection("notifications")
                .add(notification)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });


    }
}
