package com.example.eventplaner.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.eventplaner.model.Agenda;
import com.example.eventplaner.model.mocked.ItemMock;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FavoriteItemViewModel extends ViewModel {

        private final MutableLiveData<List<ItemMock>> favoriteItems = new MutableLiveData<>();

        public void addItem(ItemMock newItem){
            if(favoriteItems.getValue() == null) favoriteItems.setValue(new ArrayList<>());
            List<ItemMock> currentList = favoriteItems.getValue();

            currentList.add(newItem);
            favoriteItems.setValue(currentList);
        }

        public void removeItem(ItemMock item){
            List<ItemMock> currentList = favoriteItems.getValue();
            if(currentList != null){
                Optional<ItemMock> itemToRemove = currentList.stream().filter(x -> x.getName().equals(item.getName())).findFirst();
                itemToRemove.ifPresent(currentList::remove);
            }

            favoriteItems.setValue(currentList);
        }

        public LiveData<List<ItemMock>> getData() {
            if(favoriteItems.getValue() == null) favoriteItems.setValue(new ArrayList<>());
            return favoriteItems;
        }

}
