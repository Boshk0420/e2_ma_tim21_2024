package com.example.eventplaner.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.eventplaner.model.Agenda;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AgendaViewModel extends ViewModel {

    private final MutableLiveData<List<Agenda>> agendaData = new MutableLiveData<List<Agenda>>();

    public void addAgenda(Agenda newAgenda) {
        if(agendaData.getValue() == null ) agendaData.setValue(new ArrayList<>());
        List<Agenda> currentList = agendaData.getValue();
        if (currentList != null) {
            currentList.add(newAgenda);
            agendaData.setValue(currentList);
        }
    }

    public LiveData<List<Agenda>> getData() {
        return agendaData;
    }

}
