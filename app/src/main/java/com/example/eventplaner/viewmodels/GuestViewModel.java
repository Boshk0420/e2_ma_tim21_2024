package com.example.eventplaner.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.eventplaner.model.Agenda;
import com.example.eventplaner.model.Guest;

import java.util.ArrayList;
import java.util.List;

public class GuestViewModel extends ViewModel {

    private final MutableLiveData<List<Guest>> guestData = new MutableLiveData<List<Guest>>();

    public void addGuest(Guest newGuest) {
        if(guestData.getValue() == null ) guestData.setValue(new ArrayList<>());
        List<Guest> currentList = guestData.getValue();
        if (currentList != null) {
            currentList.add(newGuest);
            guestData.setValue(currentList);
        }
    }

    public LiveData<List<Guest>> getData() {
        return guestData;
    }



}
