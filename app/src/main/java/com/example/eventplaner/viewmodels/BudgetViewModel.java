package com.example.eventplaner.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.eventplaner.model.Budget;

import lombok.Getter;
import lombok.Setter;


public class BudgetViewModel extends ViewModel {
    private final MutableLiveData<Budget> budget = new MutableLiveData<Budget>();

    public void setBudget(Budget newBudget){
        budget.setValue(newBudget);
    }

    public LiveData<Budget> getData() {
        return budget;
    }

}
