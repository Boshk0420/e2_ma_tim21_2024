package com.example.eventplaner;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.eventplaner.R;
import com.example.eventplaner.model.Comment;
import com.example.eventplaner.model.CommentReport;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnFailureListener;
import java.util.Date;

public class ReportCommentFragment extends Fragment {

    private static final String ARG_COMMENT = "arg_comment";

    private Comment comment;
    private FirebaseFirestore db;

    public static ReportCommentFragment newInstance(Comment comment) {
        ReportCommentFragment fragment = new ReportCommentFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_COMMENT, comment);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_comment, container, false);

        if (getArguments() != null) {
            comment = getArguments().getParcelable(ARG_COMMENT);
        }

        TextView commentTextView = view.findViewById(R.id.commentTextView);
        EditText reportExplanationEditText = view.findViewById(R.id.reportExplanationEditText);
        Button submitReportButton = view.findViewById(R.id.submitReportButton);

        commentTextView.setText(comment.getCommentText()); // Assuming content is the property to display the comment

        // Initialize FirebaseFirestore instance
        db = FirebaseFirestore.getInstance();

        submitReportButton.setOnClickListener(v -> {
            String explanation = reportExplanationEditText.getText().toString();

            if (explanation.isEmpty()) {
                Toast.makeText(getContext(), "Please provide an explanation.", Toast.LENGTH_SHORT).show();
                return;
            }

            // Create a CommentReport object
            CommentReport commentReport = new CommentReport();
            commentReport.setUsername("dugmence.banja@gmail.com"); // Set the username (replace with actual username)
            commentReport.setReason(explanation);
            commentReport.setComment(comment); // Set the comment
            commentReport.setDate(new Date().toString()); // Set the current date (you can format it as needed)
            commentReport.setStatus("pending"); // Set the status (e.g., pending)
            commentReport.setId(generateUniqueId());

            // Add the CommentReport object to Firestore
            db.collection("comment_reports")
                    .add(commentReport)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Toast.makeText(getContext(), "Report submitted successfully!", Toast.LENGTH_SHORT).show();
                            getParentFragmentManager().popBackStack();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getContext(), "Failed to submit report: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        });

        return view;
    }
    private int generateUniqueId() {
        // Get current timestamp
        long timestamp = System.currentTimeMillis();

        // Convert timestamp to int (you may need a long if your application generates IDs very frequently)
        int uniqueId = (int) timestamp;

        return uniqueId;
    }
}
