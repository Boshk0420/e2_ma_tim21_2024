package com.example.eventplaner.clients;

import com.example.eventplaner.model.dto.NotificationDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface NotificationService {
    @Headers({
            "Authorization: key=AAAAgdoKV9Q:APA91bHZQC4JA6b7rzOTIq1lfkIjTinF56Ta8vfCmB6QIvgHi4YpJPJoU3f0TZfYS3zC3-cS8ibzkMzyp-XclAvrzqbvpzaIs33QFNiyuJLCWqSpcM0gHcHMd2Ril8pcrEEL9vdfTv6N",
            "Content-Type:application/json"
    })
    @POST("send")
    Call<NotificationDto> sendNotification(@Body NotificationDto product);
}
