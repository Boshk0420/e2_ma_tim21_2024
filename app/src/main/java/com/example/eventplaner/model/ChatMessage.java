package com.example.eventplaner.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class ChatMessage {
    private String sender;
    private String receiver;
    private String message;
    private EMessageStatus messageStatus;
    private Date sentAt;
}
