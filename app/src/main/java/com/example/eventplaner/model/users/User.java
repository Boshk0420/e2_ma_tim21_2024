package com.example.eventplaner.model.users;

import android.net.Uri;

import com.example.eventplaner.model.company.Company;
import com.example.eventplaner.model.company.WorkingHours;

import java.util.List;

public class User {
    private String firstName;
    private String lastName;
    private String phone;
    private String street;
    private UserRole role;
    private Uri profilePicture;
    private String messageToken;
    private Company company;
    private Company worksFor;
    private List<UnavailableHours> unavailableHours;

    public User(){}
    public User(String firstName, String lastName, String phone, String street, UserRole role, Uri profilePicture, Company company) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.street = street;
        this.role = role;
        this.profilePicture = profilePicture;
        this.company = company;
    }

    public User(String firstName, String lastName, String phone, String street, UserRole role, Uri profilePicture, Company company, String messageToken) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.street = street;
        this.role = role;
        this.profilePicture = profilePicture;
        this.company = company;
        this.messageToken = messageToken;
    }

    public Company getWorksFor() {
        return worksFor;
    }

    public void setWorksFor(Company worksFor) {
        this.worksFor = worksFor;
    }

    public List<UnavailableHours> getUnavailableHours() {
        return unavailableHours;
    }

    public void setUnavailableHours(List<UnavailableHours> unavailableHours) {
        this.unavailableHours = unavailableHours;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getMessageToken() {
        return messageToken;
    }

    public void setMessageToken(String messageToken) {
        this.messageToken = messageToken;
    }

    public Uri getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(Uri profilePicture) {
        this.profilePicture = profilePicture;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
