package com.example.eventplaner.model;

public class Pricelist {
    private int id;
    private String name;
    private double price;
    private double discount;
    private double priceAfterDiscount;

    public Pricelist() {

    }

    public Pricelist(int id, String name, double price, double discount, double priceAfterDiscount) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.discount = discount;
        this.priceAfterDiscount = priceAfterDiscount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getPriceAfterDiscount() {
        return priceAfterDiscount;
    }

    public void setPriceAfterDiscount(double priceAfterDiscount) {
        this.priceAfterDiscount = priceAfterDiscount;
    }
}
