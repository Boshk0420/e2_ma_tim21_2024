package com.example.eventplaner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import android.os.Parcel;
import android.os.Parcelable;

public class CommentReport implements Parcelable {
    private int id;
    private String username;
    private String reason;
    private Comment comment;
    private String date;
    private String status;

    // Default constructor
    public CommentReport() {
        // Required empty constructor
    }

    // Getter and setter methods for all fields

    // Parcelable interface implementation
    protected CommentReport(Parcel in) {
        id=Integer.parseInt(in.readString());
        username = in.readString();
        reason = in.readString();
        comment = in.readParcelable(Comment.class.getClassLoader());
        date = in.readString();
        status = in.readString();
    }

    public static final Creator<CommentReport> CREATOR = new Creator<CommentReport>() {
        @Override
        public CommentReport createFromParcel(Parcel in) {
            return new CommentReport(in);
        }

        @Override
        public CommentReport[] newArray(int size) {
            return new CommentReport[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(reason);
        dest.writeParcelable(comment, flags);
        dest.writeString(date);
        dest.writeString(status);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
// Other methods
}

