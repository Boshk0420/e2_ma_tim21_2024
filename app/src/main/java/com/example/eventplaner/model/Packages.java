package com.example.eventplaner.model;

import java.util.List;

public class Packages {
    private String name;
    private String description;
    private double discount;
    private boolean visible;
    private boolean available;
    private String category;
    private List<String> productsAndServices;
    private List<String> eventTypes;
    private double price;
    private List<String> images;
    private int bookingDeadlineMonths;
    private int cancellationDeadlineDays;
    private boolean autoAccept;

    public Packages() {
        // Default constructor required for Firebase deserialization
    }

    public Packages(String name, String description, double discount, boolean visible, boolean available, String category, List<String> productsAndServices, List<String> eventTypes, double price, List<String> images, int bookingDeadlineMonths, int cancellationDeadlineDays, boolean autoAccept) {
        this.name = name;
        this.description = description;
        this.discount = discount;
        this.visible = visible;
        this.available = available;
        this.category = category;
        this.productsAndServices = productsAndServices;
        this.eventTypes = eventTypes;
        this.price = price;
        this.images = images;
        this.bookingDeadlineMonths = bookingDeadlineMonths;
        this.cancellationDeadlineDays = cancellationDeadlineDays;
        this.autoAccept = autoAccept;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getProductsAndServices() {
        return productsAndServices;
    }

    public void setProductsAndServices(List<String> productsAndServices) {
        this.productsAndServices = productsAndServices;
    }

    public List<String> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<String> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public int getBookingDeadlineMonths() {
        return bookingDeadlineMonths;
    }

    public void setBookingDeadlineMonths(int bookingDeadlineMonths) {
        this.bookingDeadlineMonths = bookingDeadlineMonths;
    }

    public int getCancellationDeadlineDays() {
        return cancellationDeadlineDays;
    }

    public void setCancellationDeadlineDays(int cancellationDeadlineDays) {
        this.cancellationDeadlineDays = cancellationDeadlineDays;
    }

    public boolean isAutoAccept() {
        return autoAccept;
    }

    public void setAutoAccept(boolean autoAccept) {
        this.autoAccept = autoAccept;
    }
}
