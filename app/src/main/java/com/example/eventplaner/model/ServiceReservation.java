package com.example.eventplaner.model;

import com.example.eventplaner.model.dto.EventDto;
import com.example.eventplaner.model.mocked.ServiceMocked;
import com.example.eventplaner.model.users.User;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ServiceReservation implements Serializable {
    private String id;
    private ServiceMocked service;
    private EventDto event;
    private Date date;
    private boolean approved;
    private User employee;
}
