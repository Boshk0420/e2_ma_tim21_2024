package com.example.eventplaner.model;

import com.example.eventplaner.model.mocked.ItemMock;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor @AllArgsConstructor
public class Budget {
    private String id;
    private String eventId;
    private String userId;
    private List<BudgetCategory> planedBudget;
    private List<BudgetSpent> spentBudget;
}
