package com.example.eventplaner.model.mocked;

import com.example.eventplaner.model.users.User;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class ServiceMocked extends ItemMock{
    private String specifics;
    private double perHourRate;
    private double duration;
    private String location;
    private List<User> employees;
    private double reservationDeadline;
    private double cancelDeadline;
    private boolean acceptAutomatically;

    public ServiceMocked(ItemMock item, String specifics, double perHourRate, double duration, String location, List<User> employees, double reservationDeadline, double cancelDeadline, boolean acceptAutomatically)
    {
        super(item.getId(), item.getName(), item.getDescription(), item.getCategory(),item.getSubcategory(),item.getGallery(),item.getEventTypes(),item.getPrice(),item.getDiscount(), item.isAvailable(), item.isVisible());
        this.specifics = specifics;
        this.perHourRate = perHourRate;
        this.duration = duration;
        this.location = location;
        this.employees = employees;
        this.reservationDeadline = reservationDeadline;
        this.cancelDeadline = cancelDeadline;
        this.acceptAutomatically = acceptAutomatically;
    }

    @Override
    public String getItemType(){
        return "Service";
    }

}


