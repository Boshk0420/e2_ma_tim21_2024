package com.example.eventplaner.model;

import com.example.eventplaner.model.mocked.ItemMock;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor
public class BudgetSpent {

    private String subCategory;
    private List<ItemMock> itemList;
    private double spent;
 }
