package com.example.eventplaner.model.dto;

import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventType;
import com.example.eventplaner.model.company.WorkingHours;

import java.io.Serializable;
import java.util.List;

public class CompnyRegistrationDto implements Serializable {
    private String name;
    private String description;
    private String email;
    private String phone;
    private String street;
    private List<EventCategory> eventCategories;
    private List<EventType> eventTypes;
    private List<WorkingHours> workingHours;
    public CompnyRegistrationDto(){};
    public CompnyRegistrationDto(String name, String description, String email, String phone, String street) {
        this.name = name;
        this.description = description;
        this.email = email;
        this.phone = phone;
        this.street = street;
    }

    public List<WorkingHours> getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(List<WorkingHours> workingHours) {
        this.workingHours = workingHours;
    }

    public List<EventType> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<EventType> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public List<EventCategory> getEventCategories() {
        return eventCategories;
    }

    public void setEventCategories(List<EventCategory> eventCategories) {
        this.eventCategories = eventCategories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
