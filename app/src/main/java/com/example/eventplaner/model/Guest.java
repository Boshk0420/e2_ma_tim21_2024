package com.example.eventplaner.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Guest {

    private String fullName;
    private String age;
    private boolean invited;
    private boolean inviteAccepted;
    private String note;

}
