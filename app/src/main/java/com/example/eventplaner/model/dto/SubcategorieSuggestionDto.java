package com.example.eventplaner.model.dto;

import java.io.Serializable;

public class SubcategorieSuggestionDto implements Serializable {
    private String id;
    private String name;
    private String description;
    private String categoryId;
    private String requesterId;

    public SubcategorieSuggestionDto(String id, String name, String description, String categoryId, String requesterId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.categoryId = categoryId;
        this.requesterId = requesterId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getRequesterId() {
        return requesterId;
    }

    public void setRequesterId(String requesterId) {
        this.requesterId = requesterId;
    }
}
