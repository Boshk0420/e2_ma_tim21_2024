package com.example.eventplaner.model;

import java.io.Serializable;

public class EventSubcategory implements Serializable {
    private String id;
    private String name;
    private String description;
    private String categoryId;
    private SubcategoryType type;

    public EventSubcategory(){}
    public EventSubcategory(String id, String name, String description, SubcategoryType type, String categoryId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        this.categoryId = categoryId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public SubcategoryType getType() {
        return type;
    }

    public void setType(SubcategoryType type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
