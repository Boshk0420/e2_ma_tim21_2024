package com.example.eventplaner.model.dto;

public class NotificationDto {
    private String to;
    private NotificationInfoDto notification;

    public NotificationDto(String to, NotificationInfoDto notification) {
        this.to = to;
        this.notification = notification;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public NotificationInfoDto getNotification() {
        return notification;
    }

    public void setNotification(NotificationInfoDto notification) {
        this.notification = notification;
    }
}
