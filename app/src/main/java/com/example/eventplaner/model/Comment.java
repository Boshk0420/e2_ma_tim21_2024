package com.example.eventplaner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Comment implements Parcelable {
    private int id;
    private String commentText;
    private int rate;
    private String companyName;
    private String date;
    private String username;

    public Comment(int id, String commentText, String date, int rate, String companyName,String username) {
        this.id = id;
        this.commentText = commentText;
        this.rate = rate;
        this.companyName = companyName;
        this.date = date;
        this.username=username;
    }
    protected Comment(Parcel in) {
        id = in.readInt();
        commentText = in.readString();
        rate = in.readInt();
        companyName = in.readString();
        date = in.readString();
        username = in.readString();
    }
    public Comment(Comment other) {
        this.id = other.id;
        this.commentText = other.commentText;
        this.rate = other.rate;
        this.companyName = other.companyName;
        this.date = other.date;
        this.username = other.username;
    }
    public Comment(int id, String commentText, int rate, String companyName) {
        this.id = id;
        this.commentText = commentText;
        this.rate = rate;
        this.companyName = companyName;
    }

    public Comment() {
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Comment(String commentText, String date, int rate) {
        this.commentText = commentText;
        this.rate = rate;
        this.date = date;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public int getRating() {
        return rate;
    }

    public void setRating(int rate) {
        this.rate = rate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(commentText);
        dest.writeInt(rate);
        dest.writeString(companyName);
        dest.writeString(date);
        dest.writeString(username);
    }
}
