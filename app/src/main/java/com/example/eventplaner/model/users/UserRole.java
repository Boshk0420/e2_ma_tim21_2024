package com.example.eventplaner.model.users;

public enum UserRole {
    OD,
    PUP,
    ADMIN,
    PUPZ
}
