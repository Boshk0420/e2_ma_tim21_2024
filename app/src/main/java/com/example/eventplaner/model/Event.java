package com.example.eventplaner.model;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class Event implements Serializable {
    private String id;
    private String userId;
    private String name;
    private String description;
    private int maxAttendees;
    private String location;
    private int locationRadius;
    private Date date;
    private boolean isPrivate;
    private EventType eventType;
}
