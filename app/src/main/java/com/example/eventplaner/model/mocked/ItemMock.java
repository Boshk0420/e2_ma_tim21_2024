package com.example.eventplaner.model.mocked;

import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.EventType;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemMock {
    private String id;
    private String name;
    private String description;
    private EventCategory category;
    private EventSubcategory subcategory;
    private List<String> gallery;
    private List<EventType> eventTypes;
    private double price;
    private double discount;
    private boolean available;
    private boolean visible;

    public String getItemType(){
        return "Item";
    }
}
