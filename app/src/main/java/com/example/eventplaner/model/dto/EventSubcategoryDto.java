package com.example.eventplaner.model.dto;

import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.SubcategoryType;

import java.io.Serializable;

public class EventSubcategoryDto implements Serializable {
    private String id;
    private String name;
    private String description;
    private String categoryId;
    private SubcategoryType type;

    public EventSubcategoryDto(){}

    public EventSubcategoryDto(String id, String name, String description, String categoryId, SubcategoryType type) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.categoryId = categoryId;
        this.type = type;
    }


    public SubcategoryType getType() {
        return type;
    }

    public void setType(SubcategoryType type) {
        this.type = type;
    }

    public EventSubcategoryDto(EventSubcategory eventSubcategory){
        this.id = eventSubcategory.getId();
        this.name = eventSubcategory.getName();
        this.description = eventSubcategory.getDescription();
        this.type = eventSubcategory.getType();
        this.categoryId = eventSubcategory.getCategoryId();
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
