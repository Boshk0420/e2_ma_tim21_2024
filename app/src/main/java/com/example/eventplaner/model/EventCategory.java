package com.example.eventplaner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.io.Serializable;
import java.util.List;

public class EventCategory implements Serializable {
    private String id;
    private String name;
    private String description;
    private List<EventSubcategory> subcategories;

    public EventCategory(){

    }

    public EventCategory(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.subcategories = new ArrayList<>();
    }

    public EventCategory(String id, String name, String description, List<EventSubcategory> subcategories) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.subcategories = subcategories;
    }

    public List<EventSubcategory> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<EventSubcategory> subcategories) {
        this.subcategories = subcategories;
    }

    protected EventCategory(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readString();
        }
        name = in.readString();
        description = in.readString();
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }





}
