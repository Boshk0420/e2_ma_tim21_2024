package com.example.eventplaner.model.util;

import com.example.eventplaner.model.dto.NotificationDto;

import java.io.Serializable;

public class Notification implements Serializable {
    private String id;
    private boolean read;
    private String userId;
    private NotificationInfo notificationInfo;

    public Notification(){}
    public Notification(boolean read, String userId, NotificationInfo notificationInfo) {
        this.read = read;
        this.userId = userId;
        this.notificationInfo = notificationInfo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public NotificationInfo getNotificationInfo() {
        return notificationInfo;
    }

    public void setNotificationInfo(NotificationInfo notificationInfo) {
        this.notificationInfo = notificationInfo;
    }
}
