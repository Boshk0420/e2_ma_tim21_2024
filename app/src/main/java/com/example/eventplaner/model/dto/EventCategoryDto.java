package com.example.eventplaner.model.dto;

import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventSubcategory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EventCategoryDto implements Serializable {
    private String id;
    private String name;
    private String description;
    private List<EventSubcategoryDto> subcategories;

    public EventCategoryDto(){}
    public EventCategoryDto(String id, String name, String description, List<EventSubcategoryDto> subcategories) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.subcategories = subcategories;
    }
    public EventCategoryDto(EventCategory eventCategory){
        this.id = eventCategory.getId();
        this.name = eventCategory.getName();
        this.description = eventCategory.getDescription();
        this.subcategories = new ArrayList<>();
        for (EventSubcategory subcategory:
             eventCategory.getSubcategories()) {
            this.subcategories.add(new EventSubcategoryDto(subcategory));
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<EventSubcategoryDto> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<EventSubcategoryDto> subcategories) {
        this.subcategories = subcategories;
    }
}
