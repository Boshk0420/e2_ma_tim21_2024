package com.example.eventplaner.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class BudgetCategory implements Parcelable {

    private String id;
    private String Category;
    private List<BudgetSubCategory> subCategories;
    private double estimatedPrice;
    
    protected BudgetCategory(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BudgetCategory> CREATOR = new Creator<BudgetCategory>() {
        @Override
        public BudgetCategory createFromParcel(Parcel in) {
            return new BudgetCategory(in);
        }

        @Override
        public BudgetCategory[] newArray(int size) {
            return new BudgetCategory[size];
        }
    };

    @Override
    public String toString(){
        return Category;
    }
}
