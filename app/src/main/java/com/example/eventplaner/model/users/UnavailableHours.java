package com.example.eventplaner.model.users;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UnavailableHours {
    private Date dateFrom;
    private Date dateTo;
}
