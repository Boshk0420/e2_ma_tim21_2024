package com.example.eventplaner.model.company;

import android.net.Uri;

import com.example.eventplaner.model.EventCategory;

import java.util.List;

public class Company {
    private String email;
    private String description;
    private String name;
    private String phone;
    private String street;
    private Uri profilePicture;
    private List<EventCategory> categories;
    private List<WorkingHours> workingHours;

    public Company(){}
    public Company(String email, String description, String name, String phone, String street, Uri profilePicture) {
        this.email = email;
        this.description = description;
        this.name = name;
        this.phone = phone;
        this.street = street;
        this.profilePicture = profilePicture;
    }

    public List<WorkingHours> getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(List<WorkingHours> workingHours) {
        this.workingHours = workingHours;
    }

    public List<EventCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<EventCategory> categories) {
        this.categories = categories;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Uri getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(Uri profilePicture) {
        this.profilePicture = profilePicture;
    }
}


