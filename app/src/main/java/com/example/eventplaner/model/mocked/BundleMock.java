package com.example.eventplaner.model.mocked;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class BundleMock extends ItemMock{

    private List<ProductMock> products;
    private List<ServiceMocked> services;

    public BundleMock(ItemMock item, List<ProductMock> products, List<ServiceMocked> services){
        super(item.getId(), item.getName(), item.getDescription(), item.getCategory(),item.getSubcategory(),item.getGallery(),item.getEventTypes(),item.getPrice(),item.getDiscount(), item.isAvailable(), item.isVisible());
        this.products = products;
        this.services = services;
    }

    @Override
    public String getItemType(){
        return "Bundle";
    }
}
