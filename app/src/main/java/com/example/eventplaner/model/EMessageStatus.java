package com.example.eventplaner.model;

public enum EMessageStatus {
    Sent,
    Delivered,
    Seen
}
