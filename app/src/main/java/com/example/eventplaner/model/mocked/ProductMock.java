package com.example.eventplaner.model.mocked;

import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.EventType;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
public class ProductMock extends ItemMock{

    public ProductMock(ItemMock item){
        super(item.getId(), item.getName(), item.getDescription(), item.getCategory(),item.getSubcategory(),item.getGallery(),item.getEventTypes(),item.getPrice(),item.getDiscount(), item.isAvailable(), item.isVisible());
    }

    @Override
    public String getItemType(){
        return "Product";
    }

}
