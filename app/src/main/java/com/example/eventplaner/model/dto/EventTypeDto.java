package com.example.eventplaner.model.dto;

import com.example.eventplaner.model.EventType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EventTypeDto implements Serializable {
    private String id;
    private String name;
    private String description;
    private boolean active;
    private List<String> suggestedSubcategories;

    public EventTypeDto(){}

    public EventTypeDto(String id, String name, String description, boolean active, List<String> suggestedSubcategories) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.active = active;
        this.suggestedSubcategories = new ArrayList<>();
        this.suggestedSubcategories = suggestedSubcategories;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean isActive) {
        active = isActive;
    }

    public List<String> getSuggestedSubcategories() {
        return suggestedSubcategories;
    }

    public void setSuggestedSubcategories(List<String> suggestedSubcategories) {
        this.suggestedSubcategories = suggestedSubcategories;
    }
}
