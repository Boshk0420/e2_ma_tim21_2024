package com.example.eventplaner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.eventplaner.fragments.login_and_registration.LoginFragment;
import com.google.firebase.FirebaseApp;

public class NotLogedInActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_not_loged_in);
        FirebaseApp.initializeApp(this);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.notLogedInLayout, new LoginFragment())
                .commit();
    }
}