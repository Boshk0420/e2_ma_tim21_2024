package com.example.eventplaner;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.model.Comment;

import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentViewHolder> {

    private List<Comment> comments;
    private FragmentActivity fragmentActivity;

    public CommentsAdapter(FragmentActivity fragmentActivity, List<Comment> comments) {
        this.fragmentActivity = fragmentActivity;
        this.comments = comments;
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_item_comment, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder holder, int position) {
        Comment comment = comments.get(position);
        holder.commentTextView.setText(comment.getCommentText());
        holder.commentDateTextView.setText(comment.getDate());
        holder.commentRatingBar.setRating(comment.getRating());
        holder.commentUsernameTextView.setText(comment.getUsername());

        holder.reportButton.setOnClickListener(v -> {
            // Navigate to ReportCommentFragment
            ReportCommentFragment reportCommentFragment = ReportCommentFragment.newInstance(comment);
            fragmentActivity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.logged_in_fragment_container, reportCommentFragment)
                    .addToBackStack(null)
                    .commit();
        });
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
        notifyDataSetChanged();
    }

    static class CommentViewHolder extends RecyclerView.ViewHolder {

        TextView commentTextView;
        TextView commentDateTextView;
        RatingBar commentRatingBar;
        TextView commentUsernameTextView;
        Button reportButton;

        public CommentViewHolder(@NonNull View itemView) {
            super(itemView);
            commentTextView = itemView.findViewById(R.id.commentTextView);
            commentDateTextView = itemView.findViewById(R.id.commentDateTextView);
            commentRatingBar = itemView.findViewById(R.id.commentRatingBar);
            commentUsernameTextView=itemView.findViewById(R.id.commentUsernameTextView);
            reportButton = itemView.findViewById(R.id.reportButton);
        }
    }
}

