package com.example.eventplaner;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.eventplaner.model.CommentReport;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class ReportedCommentsFragment extends Fragment implements ReportedCommentsAdapter.ButtonClickListener {

    private ListView reportedCommentsListView;
    private List<CommentReport> reportedComments;
    private ReportedCommentsAdapter reportedCommentsAdapter;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    // Reference to the Firestore collection where reported comments are stored
    private CollectionReference reportedCommentsRef = db.collection("comment_reports");

    // Method to fetch reported comments from Firestore


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reported_comments, container, false);

        reportedCommentsListView = view.findViewById(R.id.reportedCommentsListView);
        reportedComments = new ArrayList<>();
        reportedCommentsAdapter = new ReportedCommentsAdapter(getActivity(), reportedComments);
        reportedCommentsListView.setAdapter(reportedCommentsAdapter);
        reportedCommentsAdapter.setButtonClickListener(ReportedCommentsFragment.this);
        // Fetch reported comments
        fetchReportedComments();

        reportedCommentsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Handle item click here
                CommentReport selectedComment = reportedCommentsAdapter.getItem(position);
                if (selectedComment != null) {
                    // Example: Show a toast with the selected comment
                    String message = "Reporter: " + selectedComment.getUsername() + "\nComment: " + selectedComment.getComment().getCommentText();
                    // You can replace this with your desired action
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Set button click listener
        reportedCommentsAdapter.setButtonClickListener(this);

        return view;
    }

    private void fetchReportedComments() {
        reportedCommentsRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                reportedComments.clear(); // Clear existing data before adding new data
                for (DocumentSnapshot document : task.getResult()) {
                    // Convert each document snapshot to a ReportedComment object
                    CommentReport reportedComment = document.toObject(CommentReport.class);
                    reportedComments.add(reportedComment);
                }
                // Notify the adapter that the data set has changed
                reportedCommentsAdapter.notifyDataSetChanged();
            } else {
                // Handle errors
                Toast.makeText(getActivity(), "Failed to fetch reported comments", Toast.LENGTH_SHORT).show();
            }
        });
    }



    @Override
    public void onAcceptButtonClick(int position) {
        // Handle accept button click
        CommentReport acceptedComment = reportedCommentsAdapter.getItem(position);
        if (acceptedComment != null) {
            // Example: Show a toast with the accepted comment
            Toast.makeText(getActivity(), "Accepted: " + acceptedComment.getComment().getCommentText(), Toast.LENGTH_SHORT).show();
            // Log the acceptance
            Log.d("ReportedCommentsFragment", "Accepted comment: " + acceptedComment.getComment().getCommentText());
            acceptedComment.setStatus("Accepted");

            // You can replace this with your desired action
        }
    }

    @Override
    public void onRejectButtonClick(int position) {
        // Handle reject button click
        CommentReport rejectedComment = reportedCommentsAdapter.getItem(position);
        if (rejectedComment != null) {
            // Update the status of the rejected comment
            rejectedComment.setStatus("Rejected");

            // Navigate to the RejectExplanationFragment
            RejectExplanationFragment rejectExplanationFragment = RejectExplanationFragment.newInstance(rejectedComment);
            Bundle args = new Bundle();
            args.putParcelable("rejected_comment", rejectedComment);
            rejectExplanationFragment.setArguments(args);

            // Perform the fragment transaction
            getParentFragmentManager().beginTransaction()
                    .replace(R.id.logged_in_fragment_container, rejectExplanationFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }


}
