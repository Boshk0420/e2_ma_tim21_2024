package com.example.eventplaner.adapters;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.fragments.event.categories.EditEventCategoriesFragment;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.dto.EventCategoryDto;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class EventCategoryHandlerAdapter extends RecyclerView.Adapter<EventCategoryHandlerAdapter.CategoryHandlerViewHolder>{
    private static List<EventCategory> aEventCategories;
    private static FirebaseFirestore db;

    public EventCategoryHandlerAdapter(List<EventCategory> eventCategories){
        aEventCategories = eventCategories;
        db = FirebaseFirestore.getInstance();
    }



    @NonNull
    @Override
    public EventCategoryHandlerAdapter.CategoryHandlerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_category_handler_card, parent, false);
        return new EventCategoryHandlerAdapter.CategoryHandlerViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull EventCategoryHandlerAdapter.CategoryHandlerViewHolder holder, int position) {
        EventCategory category = aEventCategories.get(position);
        holder.categoryName.setText(category.getName());
        holder.category = category;

    }

    @Override
    public int getItemCount() {
        return aEventCategories.size();
    }

    public class CategoryHandlerViewHolder extends RecyclerView.ViewHolder {
        public TextView categoryName;
        public EventCategory category;

        public CategoryHandlerViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryName = itemView.findViewById(R.id.category_type_handler_card_name);
            itemView.findViewById(R.id.event_type_handler_card_edit_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventCategoryDto eventCategoryDto = new EventCategoryDto(category);
                    FragmentManager fragmentManager = ((FragmentActivity)itemView.getContext()).getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("eventCategory", eventCategoryDto);

                    EditEventCategoriesFragment nextFragment = new EditEventCategoriesFragment();
                    nextFragment.setArguments(bundle);
                    transaction.replace(R.id.navigation_fragment_container, nextFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });

            itemView.findViewById(R.id.event_type_handler_card_delete_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    db.collection("eventCategories")
                            .document(category.getId())
                            .delete()
                            .addOnSuccessListener(aVoid -> {
                                // Handle success
                                int position = getAdapterPosition();
                                if (position != RecyclerView.NO_POSITION) {
                                    aEventCategories.remove(position);
                                    notifyItemRemoved(position);
                                }
                            })
                            .addOnFailureListener(e -> {
                                // Handle failure
                            });
                }
            });
        }


    }
    private void deleteEvent(String categoryId) {

    }
}
