package com.example.eventplaner.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.Product;
import com.example.eventplaner.model.mocked.ProductMock;
import com.example.eventplaner.model.mocked.ServiceMocked;
import com.example.eventplaner.model.users.User;

import java.util.List;

public class ServiceReservationAdapter extends RecyclerView.Adapter<ServiceReservationAdapter.ServiceViewHolder>{
    private List<ServiceMocked> serviceList;

    public ServiceReservationAdapter(List<ServiceMocked> serviceList) {
        this.serviceList = serviceList;
    }

    public void updateServiceList(List<ServiceMocked> newServiceList) {
        this.serviceList = newServiceList;
    }

    @NonNull
    @Override
    public ServiceReservationAdapter.ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceView = inflater.inflate(R.layout.service_reserve_card, parent, false);

        return new ServiceReservationAdapter.ServiceViewHolder(serviceView);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceReservationAdapter.ServiceViewHolder holder, int position) {
        ServiceMocked service = serviceList.get(position);
        holder.serviceName.setText(service.getName());
        holder.servicePrice.setText(String.valueOf(service.getPrice()));

        EmployeeChooserAdapter employeeAdapter = new EmployeeChooserAdapter(service.getEmployees());
        holder.employeesRecyclerView.setAdapter(employeeAdapter);
    }

    @Override
    public int getItemCount() {
        return (serviceList != null) ? serviceList.size() : 0;
    }

    public static class ServiceViewHolder extends RecyclerView.ViewHolder {
        public TextView serviceName;
        public TextView servicePrice;
        public RecyclerView employeesRecyclerView;

        public ServiceViewHolder(@NonNull View itemView) {
            super(itemView);
            serviceName = itemView.findViewById(R.id.service_name);
            servicePrice = itemView.findViewById(R.id.service_price);
            employeesRecyclerView = itemView.findViewById(R.id.recycler_view_employees);
            employeesRecyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
        }
    }
}
