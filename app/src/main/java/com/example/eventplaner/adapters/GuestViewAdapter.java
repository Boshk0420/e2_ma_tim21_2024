package com.example.eventplaner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.OptIn;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.Agenda;
import com.example.eventplaner.model.Guest;
import com.google.android.material.badge.ExperimentalBadgeUtils;
import com.google.android.material.chip.Chip;

import java.text.SimpleDateFormat;
import java.util.List;

import lombok.NonNull;

public class GuestViewAdapter extends RecyclerView.Adapter<GuestViewAdapter.ViewHolder>{

    private List<Guest> guestList;
    private Context context;

    public GuestViewAdapter(Context context,List<Guest> guestList){
        this.guestList = guestList;
        this.context = context;
    }

    @NonNull
    @Override
    public GuestViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.guest_card_view, parent, false);
        return new GuestViewAdapter.ViewHolder(view);
    }

    @OptIn(markerClass = ExperimentalBadgeUtils.class)
    @Override
    public void onBindViewHolder(@NonNull GuestViewAdapter.ViewHolder holder, int position) {
        Guest guest = guestList.get(position);

        holder.fullName.setText(guest.getFullName());
        String age = "Age: " + guest.getAge();
        holder.age.setText(age);

        String note = "Note: " + guest.getNote();
        holder.note.setText(note);

        String invited = guest.isInvited() ? "Invited" : "Not Invited";
        holder.invited.setText(invited);

        int visible = guest.isInvited() && guest.isInviteAccepted() ? View.VISIBLE : View.INVISIBLE;
        holder.inviteAccepted.setVisibility(visible);

    }

    @Override
    public int getItemCount() {
        return guestList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView fullName;
        TextView note;
        TextView age;
        Chip invited;
        Chip inviteAccepted;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            fullName = itemView.findViewById(R.id.guest_full_name);
            note = itemView.findViewById(R.id.guest_note);
            age = itemView.findViewById(R.id.guest_age);
            invited = itemView.findViewById(R.id.guest_invited_chip);
            inviteAccepted = itemView.findViewById(R.id.guest_invite_status_chip);
        }
    }

}
