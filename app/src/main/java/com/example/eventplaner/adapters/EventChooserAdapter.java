package com.example.eventplaner.adapters;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.fragments.event.types.EventTypeEditFragment;
import com.example.eventplaner.model.Event;
import com.example.eventplaner.model.EventType;
import com.example.eventplaner.model.dto.EventDto;
import com.example.eventplaner.model.dto.EventTypeDto;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class EventChooserAdapter extends RecyclerView.Adapter<EventChooserAdapter.EventChooserViewHolder> {

    private List<EventDto> aEvents;
    private static FirebaseFirestore db;

    private int selectedPosition = -1; // To keep track of the selected position

    public EventChooserAdapter(List<EventDto> events){
        aEvents = events;
        db = FirebaseFirestore.getInstance();
    }

    @NonNull
    @Override
    public EventChooserAdapter.EventChooserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_chooser_card, parent, false);
        return new EventChooserAdapter.EventChooserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventChooserAdapter.EventChooserViewHolder holder, int position) {
        EventDto event = aEvents.get(position);
        holder.eventName.setText(event.getName());
        holder.checkBox.setChecked(position == selectedPosition);

        holder.checkBox.setOnClickListener(v -> {
            if (selectedPosition != position) {
                notifyItemChanged(selectedPosition); // Uncheck the previously selected checkbox
                selectedPosition = position;
                notifyItemChanged(selectedPosition); // Check the newly selected checkbox
            }
        });

        holder.itemView.setOnClickListener(v -> {
            if (selectedPosition != position) {
                notifyItemChanged(selectedPosition); // Uncheck the previously selected checkbox
                selectedPosition = position;
                notifyItemChanged(selectedPosition); // Check the newly selected checkbox
            }
        });
    }

    @Override
    public int getItemCount() {
        return aEvents.size();
    }

    public EventDto getSelectedEvent() {
        if (selectedPosition != -1) {
            return aEvents.get(selectedPosition);
        }
        return null;
    }

    public static class EventChooserViewHolder extends RecyclerView.ViewHolder {
        public TextView eventName;
        public CheckBox checkBox;

        public EventChooserViewHolder(@NonNull View itemView) {
            super(itemView);
            eventName = itemView.findViewById(R.id.event_name);
            checkBox = itemView.findViewById(R.id.event_checkbox);
        }
    }
}
