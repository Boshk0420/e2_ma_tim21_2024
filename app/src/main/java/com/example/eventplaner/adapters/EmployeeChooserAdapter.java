package com.example.eventplaner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.dto.EventDto;
import com.example.eventplaner.model.users.User;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class EmployeeChooserAdapter extends RecyclerView.Adapter<EmployeeChooserAdapter.EmployeeChooserViewHolder> {

    private List<User> aEmployees;
    private static FirebaseFirestore db;

    private int selectedPosition = -1; // To keep track of the selected position

    public EmployeeChooserAdapter(List<User> employees){
        aEmployees = employees;
        db = FirebaseFirestore.getInstance();
    }

    @NonNull
    @Override
    public EmployeeChooserAdapter.EmployeeChooserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.employee_chooser_card, parent, false);
        return new EmployeeChooserAdapter.EmployeeChooserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeChooserAdapter.EmployeeChooserViewHolder holder, int position) {
        User employee = aEmployees.get(position);
        holder.employeeName.setText(employee.getFirstName() + " " + employee.getLastName());
        holder.checkBox.setChecked(position == selectedPosition);

        holder.checkBox.setOnClickListener(v -> {
            if (selectedPosition != position) {
                notifyItemChanged(selectedPosition); // Uncheck the previously selected checkbox
                selectedPosition = position;
                notifyItemChanged(selectedPosition); // Check the newly selected checkbox
            }
        });

        holder.itemView.setOnClickListener(v -> {
            if (selectedPosition != position) {
                notifyItemChanged(selectedPosition); // Uncheck the previously selected checkbox
                selectedPosition = position;
                notifyItemChanged(selectedPosition); // Check the newly selected checkbox
            }
        });
    }

    @Override
    public int getItemCount() {
        return aEmployees.size();
    }

    public User getSelectedEmployee() {
        if (selectedPosition != -1) {
            return aEmployees.get(selectedPosition);
        }
        return null;
    }

    public static class EmployeeChooserViewHolder extends RecyclerView.ViewHolder {
        public TextView employeeName;
        public CheckBox checkBox;

        public EmployeeChooserViewHolder(@NonNull View itemView) {
            super(itemView);
            employeeName = itemView.findViewById(R.id.employee_name);
            checkBox = itemView.findViewById(R.id.employee_checkbox);
        }
    }
}
