package com.example.eventplaner.adapters;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.fragments.event.categories.EditEventCategoriesFragment;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.dto.EventCategoryDto;
import com.example.eventplaner.model.util.Notification;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder>{

    private static List<Notification> aNotifications;
    private static FirebaseFirestore db;

    public NotificationAdapter(List<Notification> notifications){
        aNotifications = notifications;
        db = FirebaseFirestore.getInstance();
    }



    @NonNull
    @Override
    public NotificationAdapter.NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_card, parent, false);
        return new NotificationAdapter.NotificationViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.NotificationViewHolder holder, int position) {
        Notification notification = aNotifications.get(position);
        holder.notificationTitle.setText(notification.getNotificationInfo().getTitle());
        holder.notificationBody.setText(notification.getNotificationInfo().getBody());
        holder.itemView.setOnClickListener(v -> {
            if (holder.notificationBody.getVisibility() == View.GONE) {
                holder.notificationBody.setVisibility(View.VISIBLE);
                if(!notification.isRead()){
                    updateNotification(notification, position);
                }
            } else {
                holder.notificationBody.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return aNotifications.size();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {

        public TextView notificationTitle;
        public TextView notificationBody;
        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            notificationTitle = itemView.findViewById(R.id.notificationTitle);
            notificationBody = itemView.findViewById(R.id.notificationMessage);

        }


    }

    private void updateNotification(Notification notification, int position){
        db.collection("notifications")
                .document(notification.getId())
                .update("read", true)
                .addOnSuccessListener(aVoid -> {
                    notification.setRead(true);
                    notifyItemChanged(position);
                })
                .addOnFailureListener(e -> {
                    Log.d("NotificationAdapter", "Error updating document", e);
                });
    }
}
