package com.example.eventplaner.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.util.Notification;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class ReadNotificationAdapter extends RecyclerView.Adapter<ReadNotificationAdapter.NotificationViewHolder>{
    private static List<Notification> aNotifications;
    private static FirebaseFirestore db;

    public ReadNotificationAdapter(List<Notification> notifications){
        aNotifications = notifications;
        db = FirebaseFirestore.getInstance();
    }



    @NonNull
    @Override
    public ReadNotificationAdapter.NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_card, parent, false);
        return new ReadNotificationAdapter.NotificationViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ReadNotificationAdapter.NotificationViewHolder holder, int position) {
        Notification notification = aNotifications.get(position);
        holder.notificationTitle.setText(notification.getNotificationInfo().getTitle());
        holder.notificationBody.setText(notification.getNotificationInfo().getBody());
        holder.itemView.setOnClickListener(v -> {
            if (holder.notificationBody.getVisibility() == View.GONE) {
                holder.notificationBody.setVisibility(View.VISIBLE);
            } else {
                holder.notificationBody.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return aNotifications.size();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {

        public TextView notificationTitle;
        public TextView notificationBody;
        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            notificationTitle = itemView.findViewById(R.id.notificationTitle);
            notificationBody = itemView.findViewById(R.id.notificationMessage);

        }


    }

    private void updateNotification(Notification notification, int position){
        db.collection("notifications")
                .document(notification.getId())
                .update("read", true)
                .addOnSuccessListener(aVoid -> {
                    notification.setRead(true);
                    notifyItemChanged(position);
                })
                .addOnFailureListener(e -> {
                    Log.d("NotificationAdapter", "Error updating document", e);
                });
    }
}
