package com.example.eventplaner.adapters;

import static android.content.ContentValues.TAG;

import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.fragments.event.categories.EditEventCategoriesFragment;
import com.example.eventplaner.fragments.event.categories.HandleEventCategoriesFragment;
import com.example.eventplaner.fragments.event.categories.SubcategorySugestionEditFragment;
import com.example.eventplaner.fragments.event.categories.SubcategorySugestionsFragment;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.EventType;
import com.example.eventplaner.model.SubcategorieSugestion;
import com.example.eventplaner.model.dto.EventCategoryDto;
import com.example.eventplaner.model.dto.EventSubcategoryDto;
import com.example.eventplaner.model.dto.SubcategorieSuggestionDto;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class SubcategorieSugestionAdapter extends RecyclerView.Adapter<SubcategorieSugestionAdapter.SubcategorieSugestionViewHolder>{
    private static List<SubcategorieSugestion> aSugestions;
    private static FirebaseFirestore db;
    public SubcategorieSugestionAdapter(List<SubcategorieSugestion> sugestions){
        aSugestions = sugestions;
        db = FirebaseFirestore.getInstance();
    }

    @NonNull
    @Override
    public SubcategorieSugestionAdapter.SubcategorieSugestionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subcategorie_sugestion_card, parent, false);
        return new SubcategorieSugestionAdapter.SubcategorieSugestionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubcategorieSugestionAdapter.SubcategorieSugestionViewHolder holder, int position) {
        SubcategorieSugestion suggestion = aSugestions.get(position);
        holder.sugestionName.setText(suggestion.getName());
        holder.sugestionDescription.setText(suggestion.getDescription());
        holder.suggestion = suggestion;
        db.collection("eventCategories")
                .document(suggestion.getCategoryId())
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        holder.categoryName.setText(documentSnapshot.getString("name"));
                    }
                });

    }

    @Override
    public int getItemCount() {
        return aSugestions.size();
    }

    public class SubcategorieSugestionViewHolder extends RecyclerView.ViewHolder {
        public TextView sugestionName;
        public TextView sugestionDescription;
        public TextView categoryName;
        public ImageButton acceptButton;
        public ImageButton refuseButton;
        public ImageButton editButton;
        public SubcategorieSugestion suggestion;

        public SubcategorieSugestionViewHolder(@NonNull View itemView) {
            super(itemView);
            sugestionName = itemView.findViewById(R.id.subcategory_sugestion_card_name);
            categoryName = itemView.findViewById(R.id.category_sugestion_card_name);
            sugestionDescription = itemView.findViewById(R.id.subcategorie_suggestion_description);
            acceptButton = itemView.findViewById(R.id.subcategorie_sugestion_card_approve);
            refuseButton = itemView.findViewById(R.id.subcategorie_sugestion_card_decline);
            editButton = itemView.findViewById(R.id.subcategorie_sugestion_card_edit);

            acceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createNewSubcategory(suggestion);
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        aSugestions.remove(position);
                        notifyItemRemoved(position);
                    }
                }
            });

            refuseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteSubcategorySuggestion(suggestion);
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        aSugestions.remove(position);
                        notifyItemRemoved(position);
                    }
                }
            });

            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    SubcategorieSuggestionDto subcategorieSuggestionDto = new SubcategorieSuggestionDto(suggestion.getId(), suggestion.getName(), suggestion.getDescription(), suggestion.getCategoryId(), suggestion.getRequesterId());
                    bundle.putSerializable("suggestion", subcategorieSuggestionDto);

                    FragmentManager fragmentManager = ((FragmentActivity)itemView.getContext()).getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();

                    SubcategorySugestionEditFragment nextFragment = new SubcategorySugestionEditFragment();
                    nextFragment.setArguments(bundle);
                    transaction.replace(R.id.navigation_fragment_container, nextFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();

                }
            });
        }
    }

    public static void createNewSubcategory(SubcategorieSugestion suggestion){
        EventSubcategoryDto eventSubcategoryDto = new EventSubcategoryDto();
        eventSubcategoryDto.setName(suggestion.getName());
        eventSubcategoryDto.setDescription(suggestion.getDescription());
        eventSubcategoryDto.setCategoryId(suggestion.getCategoryId());
        db.collection("eventSubcategories")
                .add(eventSubcategoryDto)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());

                        deleteSubcategorySuggestion(suggestion);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });
    }

    private static void deleteSubcategorySuggestion(SubcategorieSugestion suggestion) {
        String documentId = suggestion.getId();

        db.collection("subcategorySuggestions")
                .document(documentId)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully deleted from Firestore");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error deleting document from Firestore", e);
                    }
                });
    }

}
