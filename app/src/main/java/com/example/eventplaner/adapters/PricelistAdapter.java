package com.example.eventplaner.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.eventplaner.R;
import com.example.eventplaner.model.Pricelist;
import java.util.List;

public class PricelistAdapter extends RecyclerView.Adapter<PricelistAdapter.PricelistViewHolder> {
    private List<Pricelist> itemList;

    public PricelistAdapter(List<Pricelist> itemList) {
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public PricelistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_item_pricelist, parent, false);
        return new PricelistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PricelistViewHolder holder, int position) {
        Pricelist item = itemList.get(position);
        holder.textViewName.setText(item.getName());
        holder.textViewPrice.setText("Cena: " + String.valueOf(item.getPrice()));
        holder.textViewDiscount.setText("Popust: " + String.valueOf(item.getDiscount()) + "%");

        holder.textViewDiscount.setTextColor(Color.RED);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void updateItemList(List<Pricelist> newItemList) {
        itemList.clear();
        itemList.addAll(newItemList);
        notifyDataSetChanged();
    }

    static class PricelistViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName;
        TextView textViewPrice;
        TextView textViewDiscount;

        PricelistViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            textViewDiscount = itemView.findViewById(R.id.textViewDiscount);
        }
    }
}


