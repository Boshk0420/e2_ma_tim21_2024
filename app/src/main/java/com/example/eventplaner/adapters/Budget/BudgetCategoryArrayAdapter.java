package com.example.eventplaner.adapters.Budget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.eventplaner.model.Budget;
import com.example.eventplaner.model.BudgetCategory;
import com.example.eventplaner.model.EventType;

import java.util.List;

public class BudgetCategoryArrayAdapter extends ArrayAdapter<BudgetCategory> {

    private LayoutInflater inflater;

    public BudgetCategoryArrayAdapter(Context context, List<BudgetCategory> budgetCategories){
        super(context,0,budgetCategories);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
            holder = new ViewHolder();
            holder.text = convertView.findViewById(android.R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        BudgetCategory budgetCategory = getItem(position);
        if (budgetCategory != null) {
            holder.text.setText(budgetCategory.getCategory());
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView text;
    }

}
