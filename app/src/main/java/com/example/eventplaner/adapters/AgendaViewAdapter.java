package com.example.eventplaner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.OptIn;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.Agenda;
import com.example.eventplaner.model.mocked.ItemMock;
import com.google.android.material.badge.ExperimentalBadgeUtils;
import com.google.android.material.chip.Chip;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;

import lombok.NonNull;

public class AgendaViewAdapter extends RecyclerView.Adapter<AgendaViewAdapter.ViewHolder>{

    private List<Agenda> agendaList;
    private Context context;

    public AgendaViewAdapter(Context context,List<Agenda> agendaList){
        this.context = context;
        this.agendaList = agendaList;
    }

    @NonNull
    @Override
    public AgendaViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.agenda_card_view, parent, false);
        return new AgendaViewAdapter.ViewHolder(view);
    }

    @OptIn(markerClass = ExperimentalBadgeUtils.class)
    @Override
    public void onBindViewHolder(@NonNull AgendaViewAdapter.ViewHolder holder, int position) {
       Agenda agenda = agendaList.get(position);
       holder.name.setText(agenda.getName());
       holder.description.setText(agenda.getDescription());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

       holder.date.setText(dateFormat.format(agenda.getFrom()));

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

       String fromTo = timeFormat.format(agenda.getFrom()) + " - " + timeFormat.format(agenda.getTo());
       holder.toFrom.setText(fromTo);
    }

    @Override
    public int getItemCount() {
        return agendaList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView description;

        TextView date;

        TextView toFrom;




        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.agenda_name);
            description = itemView.findViewById(R.id.agenda_description);
            date = itemView.findViewById(R.id.agenda_date);
            toFrom = itemView.findViewById(R.id.agenda_to_from);
        }
    }
}
