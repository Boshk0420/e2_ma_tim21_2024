package com.example.eventplaner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.EventType;

import java.util.ArrayList;
import java.util.List;

public class EventTypeListAdapter extends RecyclerView.Adapter<EventTypeListAdapter.TypeViewHolder>{

    private List<EventType> aEventTypes;
    private List<EventType> selectedEventTypes;
    public EventTypeListAdapter(List<EventType> eventTypes){
        aEventTypes = eventTypes;
        selectedEventTypes = new ArrayList<>();
    }

    @NonNull
    @Override
    public TypeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_type_selection_card, parent, false);
        return new EventTypeListAdapter.TypeViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull TypeViewHolder holder, int position) {
        EventType type = aEventTypes.get(position);
        holder.typeName.setText(type.getName());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectedEventTypes.add(type);
            }
        });
    }

    @Override
    public int getItemCount() {
        return aEventTypes.size();
    }

    public static class TypeViewHolder extends RecyclerView.ViewHolder {
        public TextView typeName;
        public CheckBox checkBox;

        public TypeViewHolder(@NonNull View itemView) {
            super(itemView);
            typeName = itemView.findViewById(R.id.text_type_name);
            checkBox = itemView.findViewById(R.id.checkbox_event_type);
        }
    }

    public List<EventType> getSelectedEventTypes(){
        return selectedEventTypes;
    }
}
