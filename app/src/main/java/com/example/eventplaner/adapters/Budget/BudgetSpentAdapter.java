package com.example.eventplaner.adapters.Budget;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.BudgetSpent;
import com.example.eventplaner.model.mocked.ItemMock;

import java.util.List;

public class BudgetSpentAdapter extends BaseExpandableListAdapter {

    private final List<BudgetSpent> items;

    private LayoutInflater inflater;

    public BudgetSpentAdapter(List<BudgetSpent> items){
        this.items = items;
    }

    @Override
    public int getGroupCount() {
        return items.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return items.get(groupPosition).getItemList().size();
    }
    @Override
    public Object getGroup(int groupPosition) {
        return items.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return items.get(groupPosition).getItemList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(inflater == null ){
            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView == null){
            convertView = inflater.inflate(R.layout.list_group, null);
        }

        BudgetSpent spent = (BudgetSpent) getGroup(groupPosition);

        TextView spentTitle = convertView.findViewById(R.id.expandedListGroupTitle);
        spentTitle.setText("Sub-Category: " + spent.getSubCategory());
        TextView spentPrice = convertView.findViewById(R.id.expandedListGroupPrice);
        spentPrice.setText("Price: " + Double.toString(spent.getSpent()));


        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item, null);
        }

        ItemMock item = (ItemMock) getChild(groupPosition,childPosition);
        TextView itemTitle = convertView.findViewById(R.id.expandedListItemTitle);
        TextView itemAmount = convertView.findViewById(R.id.expandedListItemPrice);

        itemTitle.setText("Item name:" + item.getName());
        itemTitle.setText("Price: " + Double.toString(item.getPrice()));

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

}
