package com.example.eventplaner.adapters.LoginAndRegistration;

import static android.content.ContentValues.TAG;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.SubcategoryAdapter;
import com.example.eventplaner.fragments.event.categories.EditEventCategoriesFragment;
import com.example.eventplaner.fragments.login_and_registration.RegistrationRequestDetails;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.company.Company;
import com.example.eventplaner.model.dto.CompanyProfileRegistrationDto;
import com.example.eventplaner.model.dto.EventCategoryDto;
import com.example.eventplaner.model.users.User;
import com.example.eventplaner.model.users.UserRole;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class CompanyRegistrationRequestAdapter extends RecyclerView.Adapter<CompanyRegistrationRequestAdapter.RegistrationRequestViewHolder>{

    private static List<CompanyProfileRegistrationDto> requests;
    private static FirebaseFirestore db;
    private static  FirebaseAuth mAuth;

    public CompanyRegistrationRequestAdapter(List<CompanyProfileRegistrationDto> requests) {
        this.requests = requests;
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
    }

    @NonNull
    @Override
    public RegistrationRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.registration_request_card, parent, false);
        return new CompanyRegistrationRequestAdapter.RegistrationRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RegistrationRequestViewHolder holder, int position) {
        CompanyProfileRegistrationDto request = requests.get(position);
        holder.usersName.setText(request.getFirstName());
        holder.usersLastName.setText(request.getLastName());
        holder.usersEmail.setText(request.getEmail());
        holder.companyEmail.setText(request.getCompanyEmail());
        holder.companyName.setText(request.getCompanyName());
        holder.registrationInfo = request;
    }

    @Override
    public int getItemCount()  {
        return requests.size();
    }

    public class RegistrationRequestViewHolder extends RecyclerView.ViewHolder {
        public TextView usersName;
        public TextView usersLastName;
        public TextView usersEmail;
        public TextView companyName;
        public TextView companyEmail;

        public CompanyProfileRegistrationDto registrationInfo;


        public RegistrationRequestViewHolder(@NonNull View itemView) {
            super(itemView);
            usersName = itemView.findViewById(R.id.user_name);
            usersLastName = itemView.findViewById(R.id.user_last_name);
            usersEmail = itemView.findViewById(R.id.user_email);
            companyName = itemView.findViewById(R.id.company_name);
            companyEmail = itemView.findViewById(R.id.company_email);


            itemView.findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Company company = new Company(registrationInfo.getCompanyEmail(), registrationInfo.getCompanyDescription(), registrationInfo.getCompanyName(), registrationInfo.getPhone(), registrationInfo.getCompanyAddress(), null);
                    User newUser = new User(registrationInfo.getFirstName(), registrationInfo.getLastName(), registrationInfo.getPhone(), registrationInfo.getAddress(), UserRole.PUP, null, company);
                    mAuth.createUserWithEmailAndPassword(registrationInfo.getEmail(), registrationInfo.getPassword())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "createUserWithEmail:success");
                                        FirebaseUser user = task.getResult().getUser();
                                        user.sendEmailVerification()
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            deleteRegistrationRequest(registrationInfo.getId());
                                                            Log.d(TAG, "Email sent.");
                                                        }
                                                    }
                                                });
                                        db.collection("users").document(user.getUid())
                                                .set(newUser)
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Log.d(TAG, "User data added successfully");
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Log.w(TAG, "Error adding user data", e);
                                                    }
                                                });

                                    } else {
                                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                    }
                                }
                            });
                }
            });
            itemView.findViewById(R.id.all_details_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fragmentManager = ((FragmentActivity)itemView.getContext()).getSupportFragmentManager();

                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("registrationInfo", registrationInfo);

                    RegistrationRequestDetails nextFragment = new RegistrationRequestDetails();
                    nextFragment.setArguments(bundle);
                    transaction.replace(R.id.navigation_fragment_container, nextFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });

            itemView.findViewById(R.id.deny_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteRegistrationRequest(registrationInfo.getId());
                }
            });
        }

        private void deleteRegistrationRequest(String requestId){
            db.collection("companyRegistrationRequests").document(requestId)
                    .delete()
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            int position = getAdapterPosition();
                            if (position != RecyclerView.NO_POSITION) {
                                requests.remove(position);
                                notifyItemRemoved(position);
                            }
                            Log.d(TAG, "Registration request deleted successfully");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Error deleting registration request", e);
                        }
                    });
        }

    }

}
