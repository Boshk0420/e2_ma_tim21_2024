package com.example.eventplaner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.OptIn;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.fragments.items.ItemDetailsFragment;
import com.example.eventplaner.model.mocked.BundleMock;
import com.example.eventplaner.model.mocked.ItemMock;
import com.example.eventplaner.model.mocked.ProductMock;
import com.example.eventplaner.model.mocked.ServiceMocked;
import com.example.eventplaner.viewmodels.FavoriteItemViewModel;
import com.google.android.material.badge.ExperimentalBadgeUtils;
import com.google.android.material.chip.Chip;
import com.squareup.picasso.Picasso;

import java.util.List;

import lombok.NonNull;

public class ItemViewAdapter extends RecyclerView.Adapter<ItemViewAdapter.ViewHolder> {


    private List<ItemMock> itemList;
    private Context context;

    private FavoriteItemViewModel viewModel;

    public ItemViewAdapter(Context context,List<ItemMock> itemList, FavoriteItemViewModel viewModel) {
        this.context = context;
        this.itemList = itemList;
        this.viewModel = viewModel;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_view, parent, false);
        return new ViewHolder(view);
    }

    @OptIn(markerClass = ExperimentalBadgeUtils.class)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemMock item = itemList.get(position);
        holder.titleTextView.setText(item.getName());
        holder.descriptionTextView.setText(item.getDescription());
        String category = "Category: " + item.getCategory().getName();
        String subcategory = "Subcategory: " + item.getSubcategory().getName();
        holder.category.setText(category);
        holder.subcategory.setText(subcategory);
        Picasso.get().load(item.getGallery().get(0)).into(holder.imageView);
        holder.itemTypeChip.setText(item.getItemType());
        String price = (int)item.getPrice() + "$";
        holder.itemPrice.setText(price);

        holder.priceCrossed.setVisibility( item.getDiscount() != 0.0 ? View.VISIBLE : View.INVISIBLE );
        String discountedPrice = "" +  (item.getDiscount() != 0.0 ? (int)(item.getPrice() - item.getPrice()*item.getDiscount()/100) + "$" : "");
        holder.itemPriceDiscounted.setText(discountedPrice);

        holder.favCheckbox.setChecked(
                viewModel.getData().getValue().stream().anyMatch(x -> x.getName().equals(item.getName()))
        );
        holder.favCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    viewModel.addItem(item);
                }else{
                    viewModel.removeItem(item);
                }
            }
        });

        holder.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (context instanceof FragmentActivity) {

                    FragmentActivity activity = (FragmentActivity) context;


                    ItemDetailsFragment fragment = new ItemDetailsFragment(
                            item.getItemType().equals("Product") ? (ProductMock) item : null,
                            item.getItemType().equals("Service") ? (ServiceMocked) item : null,
                            item.getItemType().equals("Bundle") ? (BundleMock) item : null
                            );

                    activity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.logged_in_fragment_container, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            }
        });

    }



    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        TextView descriptionTextView;

        ImageView imageView;

        TextView category;

        TextView subcategory;

        Chip itemTypeChip;

        TextView itemPrice;

        View priceCrossed;

        TextView itemPriceDiscounted;

        Button details;

        CheckBox favCheckbox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.item_name);
            descriptionTextView = itemView.findViewById(R.id.item_description);
            imageView = itemView.findViewById(R.id.item_image);
            category = itemView.findViewById(R.id.item_category);
            subcategory = itemView.findViewById(R.id.item_subcategory);
            itemTypeChip = itemView.findViewById(R.id.item_chip);
            itemPrice = itemView.findViewById(R.id.item_price);
            priceCrossed = itemView.findViewById(R.id.item_price_cross);
            itemPriceDiscounted = itemView.findViewById(R.id.item_price_dicounted);
            details = itemView.findViewById(R.id.open_details_button);
            favCheckbox = itemView.findViewById(R.id.fav_checkbox);
        }
    }
}
