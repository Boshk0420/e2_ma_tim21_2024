package com.example.eventplaner.adapters.Budget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.BudgetCategory;
import com.example.eventplaner.model.BudgetSubCategory;

import java.util.List;

public class BudgetCategoryAdapter extends BaseExpandableListAdapter {

    private final List<BudgetCategory> categories;
    private LayoutInflater inflater;

    private boolean isCategory = true;

    public BudgetCategoryAdapter(List<BudgetCategory> categories,boolean isCategory) {
        this.categories = categories;
        this.isCategory = isCategory;
    }

    @Override
    public int getGroupCount() {
        return categories.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return categories.get(groupPosition).getSubCategories().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return categories.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return categories.get(groupPosition).getSubCategories().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_group, null);
        }
        BudgetCategory category = (BudgetCategory) getGroup(groupPosition);
        TextView categoryTitle = convertView.findViewById(R.id.expandedListGroupTitle);
        categoryTitle.setText((isCategory ? "Category: " : "Sub-Category: ") + category.getCategory());
        TextView categoryPrice = convertView.findViewById(R.id.expandedListGroupPrice);
        categoryPrice.setText( "Price: " + Double.toString(category.getEstimatedPrice()));


        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item, null);
        }
        BudgetSubCategory subCategory = (BudgetSubCategory) getChild(groupPosition, childPosition);
        TextView subCategoryTitle = convertView.findViewById(R.id.expandedListItemTitle);
        TextView subCategoryAmount = convertView.findViewById(R.id.expandedListItemPrice);
        subCategoryTitle.setText((isCategory ? "Sub-Category: " : "Item name: ") + subCategory.getName());
        subCategoryAmount.setText( "Price: " + String.valueOf(subCategory.getEstimatedPrice()));
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

}
