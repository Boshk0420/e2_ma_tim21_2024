package com.example.eventplaner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.Product;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private List<Product> productList;

    public ProductAdapter(List<Product> productList) {
        this.productList = productList;
    }

    public void updateProductList(List<Product> newProductList) {
        this.productList = newProductList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View productView = inflater.inflate(R.layout.fragment_product_item_layout, parent, false);

        return new ProductViewHolder(productView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {

        if (productList != null && position < productList.size()) {
            Product product = productList.get(position);

            holder.txtName.setText(product.getName());
            holder.txtDescription.setText(product.getDescription());
            holder.txtCategory.setText(product.getCategory());
            holder.txtPrice.setText(String.valueOf(product.getPrice()));
            holder.txtDiscount.setText(String.valueOf(product.getDiscount()));
        }
    }

    @Override
    public int getItemCount() {
        return (productList != null) ? productList.size() : 0; // Return 0 if productList is null
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName;
        public TextView txtDescription;
        public TextView txtCategory;
        public TextView txtPrice;
        public TextView txtDiscount;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.textViewProductName);
            txtDescription = itemView.findViewById(R.id.textViewProductDescription);
            txtCategory = itemView.findViewById(R.id.textViewProductCategory);
            txtPrice = itemView.findViewById(R.id.textViewProductPrice);
            txtDiscount = itemView.findViewById(R.id.textViewProductDiscount);
        }
    }

}
