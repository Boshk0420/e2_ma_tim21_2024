package com.example.eventplaner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.Packages;

import java.util.List;

public class PackageAdapter extends RecyclerView.Adapter<PackageAdapter.PackageViewHolder> {

    private List<Packages> packageList;

    public PackageAdapter(List<Packages> packageList) {
        this.packageList = packageList;
    }

    public void updatePackageList(List<Packages> newPackageList) {
        this.packageList = newPackageList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PackageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View packageView = inflater.inflate(R.layout.fragment_package_item_layout, parent, false);

        return new PackageViewHolder(packageView);
    }

    @Override
    public void onBindViewHolder(@NonNull PackageViewHolder holder, int position) {
        if (packageList != null && position < packageList.size()) {
            Packages packageItem = packageList.get(position);

            holder.txtName.setText(packageItem.getName());
            holder.txtDescription.setText(packageItem.getDescription());
            holder.txtCategory.setText(packageItem.getCategory());
            holder.txtPrice.setText(String.valueOf(packageItem.getPrice()));
            holder.txtDiscount.setText(String.valueOf(packageItem.getDiscount()));
        }
    }

    @Override
    public int getItemCount() {
        return (packageList != null) ? packageList.size() : 0; // Return 0 if packageList is null
    }

    public static class PackageViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName;
        public TextView txtDescription;
        public TextView txtCategory;
        public TextView txtPrice;
        public TextView txtDiscount;

        public PackageViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.textViewPackageName);
            txtDescription = itemView.findViewById(R.id.textViewPackageDescription);
            txtCategory = itemView.findViewById(R.id.textViewPackageCategory);
            txtPrice = itemView.findViewById(R.id.textViewPackagePrice);
            txtDiscount = itemView.findViewById(R.id.textViewPackageDiscount);
        }
    }
}
