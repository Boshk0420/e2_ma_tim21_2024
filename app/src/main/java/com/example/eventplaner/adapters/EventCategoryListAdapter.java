package com.example.eventplaner.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.EventCategory;

import java.util.ArrayList;
import java.util.List;

public class EventCategoryListAdapter extends RecyclerView.Adapter<EventCategoryListAdapter.CategoryViewHolder> {
    private List<EventCategory> aEventCategories;

    private List<EventCategory> selectedEventCategories;


    public EventCategoryListAdapter(List<EventCategory> eventCategories){
        aEventCategories = eventCategories;
        selectedEventCategories = new ArrayList<>();
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_selection_card, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        EventCategory category = aEventCategories.get(position);
        holder.categoryName.setText(category.getName());
        holder.categoryDescription.setText(category.getDescription());

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    selectedEventCategories.add(category);
                }
                else {
                    selectedEventCategories.remove(category);
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return aEventCategories.size();
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        public TextView categoryName;
        public TextView categoryDescription;
        //public RecyclerView subcategoryRecyclerView;
        public CheckBox checkBox;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryName = itemView.findViewById(R.id.text_category_name);
            categoryDescription = itemView.findViewById(R.id.text_category_description);
            checkBox = itemView.findViewById(R.id.checkbox_category);
        }
    }

    public List<EventCategory> getSelectedCategories(){
        return selectedEventCategories;
    }
}

