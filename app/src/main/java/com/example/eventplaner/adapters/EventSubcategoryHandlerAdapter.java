package com.example.eventplaner.adapters;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.fragments.event.categories.EditEventCategoriesFragment;
import com.example.eventplaner.fragments.event.categories.EditEventSubcategorieFragment;
import com.example.eventplaner.model.EventSubcategory;
import com.example.eventplaner.model.dto.EventSubcategoryDto;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;


public class EventSubcategoryHandlerAdapter extends RecyclerView.Adapter<EventSubcategoryHandlerAdapter.SubcategoryHandlerViewHolder> {
    private static List<EventSubcategory> aEventSubcategories;
    private static FirebaseFirestore db;

    public EventSubcategoryHandlerAdapter(List<EventSubcategory> eventSubcategories)
    {
        aEventSubcategories = eventSubcategories;
        db = FirebaseFirestore.getInstance();
    }

    @NonNull
    @Override
    public EventSubcategoryHandlerAdapter.SubcategoryHandlerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_subcategory_handler_card, parent, false);
        return new EventSubcategoryHandlerAdapter.SubcategoryHandlerViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull SubcategoryHandlerViewHolder holder, int position) {
        EventSubcategory subcategory = aEventSubcategories.get(position);
        holder.subcategoryName.setText(subcategory.getName());
        holder.subcategory = subcategory;
    }

    @Override
    public int getItemCount() {
        return aEventSubcategories.size();
    }

    public class SubcategoryHandlerViewHolder extends RecyclerView.ViewHolder {
        public TextView subcategoryName;
        public EventSubcategory subcategory;

        public SubcategoryHandlerViewHolder(@NonNull View itemView) {
            super(itemView);
            subcategoryName = itemView.findViewById(R.id.subcategory_type_handler_card_name);
            itemView.findViewById(R.id.edit_subcategory_handler_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventSubcategoryDto eventSubcategoryDto = new EventSubcategoryDto(subcategory);
                    FragmentManager fragmentManager = ((FragmentActivity)itemView.getContext()).getSupportFragmentManager();

                    FragmentTransaction transaction = fragmentManager.beginTransaction();

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("eventSubcategory", eventSubcategoryDto);

                    EditEventSubcategorieFragment nextFragment = new EditEventSubcategorieFragment();
                    nextFragment.setArguments(bundle);
                    transaction.replace(R.id.navigation_fragment_container, nextFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });

            itemView.findViewById(R.id.delete_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    db.collection("eventSubcategories")
                            .document(subcategory.getId())
                            .delete()
                            .addOnSuccessListener(aVoid -> {
                                int position = getAdapterPosition();
                                if (position != RecyclerView.NO_POSITION) {
                                    aEventSubcategories.remove(position);
                                    notifyItemRemoved(position);
                                }
                            })
                            .addOnFailureListener(e -> {
                                // Handle failure
                            });
                }
            });
        }
    }
}
