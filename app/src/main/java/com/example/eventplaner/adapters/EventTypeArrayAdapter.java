package com.example.eventplaner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.eventplaner.model.Event;
import com.example.eventplaner.model.EventType;

import java.util.List;

public class EventTypeArrayAdapter extends ArrayAdapter<EventType> {

    private LayoutInflater inflater;
    public EventTypeArrayAdapter(Context context, List<EventType> eventTypes) {
        super(context, 0, eventTypes);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
            holder = new ViewHolder();
            holder.text = convertView.findViewById(android.R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        EventType eventType = getItem(position);
        if (eventType != null) {
            holder.text.setText(eventType.getName());
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView text;
    }
}
