package com.example.eventplaner.adapters.Budget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.eventplaner.adapters.EventTypeArrayAdapter;
import com.example.eventplaner.model.BudgetSubCategory;
import com.example.eventplaner.model.EventType;

import java.util.List;

public class BudgetSubCategoryArrayAdapter extends ArrayAdapter<BudgetSubCategory> {

    private LayoutInflater inflater;

    public BudgetSubCategoryArrayAdapter(Context context, List<BudgetSubCategory> subCategories){
        super(context,0,subCategories);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
            holder = new ViewHolder();
            holder.text = convertView.findViewById(android.R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        BudgetSubCategory subCategory = getItem(position);
        if (subCategory != null) {
            holder.text.setText(subCategory.getName());
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView text;
    }

}
