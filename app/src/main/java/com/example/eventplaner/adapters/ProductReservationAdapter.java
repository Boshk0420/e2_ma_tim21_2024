package com.example.eventplaner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.Product;
import com.example.eventplaner.model.mocked.ProductMock;

import java.util.List;

public class ProductReservationAdapter extends RecyclerView.Adapter<ProductReservationAdapter.ProductViewHolder>{
    private List<ProductMock> productList;

    public ProductReservationAdapter(List<ProductMock> productList) {
        this.productList = productList;
    }

    public void updateProductList(List<ProductMock> newProductList) {
        this.productList = newProductList;
    }

    @NonNull
    @Override
    public ProductReservationAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View productView = inflater.inflate(R.layout.product_reserve_card, parent, false);

        return new ProductReservationAdapter.ProductViewHolder(productView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductReservationAdapter.ProductViewHolder holder, int position) {

        if (productList != null && position < productList.size()) {
            ProductMock product = productList.get(position);

            holder.txtName.setText(product.getName());
            holder.txtPrice.setText(String.valueOf(product.getPrice()));
        }
    }

    @Override
    public int getItemCount() {
        return (productList != null) ? productList.size() : 0;
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName;
        public TextView txtPrice;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.textViewProductName);
            txtPrice = itemView.findViewById(R.id.textViewProductPrice);
        }
    }
}
