package com.example.eventplaner.adapters;

import android.media.metrics.Event;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.fragments.event.categories.EditEventCategoriesFragment;
import com.example.eventplaner.fragments.event.types.EventTypeEditFragment;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventType;
import com.example.eventplaner.model.dto.EventTypeDto;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class EventTypeHandlerAdapter extends RecyclerView.Adapter<EventTypeHandlerAdapter.TypeHandlerViewHolder> {
    private List<EventType> aEventTypes;
    private static FirebaseFirestore db;

    public EventTypeHandlerAdapter(List<EventType> eventTypes){
        aEventTypes = eventTypes;
        db = FirebaseFirestore.getInstance();
    }



    @NonNull
    @Override
    public EventTypeHandlerAdapter.TypeHandlerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_type_handler_card, parent, false);
        return new EventTypeHandlerAdapter.TypeHandlerViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull EventTypeHandlerAdapter.TypeHandlerViewHolder holder, int position) {
        EventType type = aEventTypes.get(position);
        if (type != null) {
            holder.typeName.setText(type.getName());
            holder.description.setText(type.getDescription());
            holder.type = type;
            Log.d("EventTypeHandlerAdapter", "Type: " + type);
            if (type != null) {
                Log.d("EventTypeHandlerAdapter", "isActive: " + type.isActive());
                holder.typeActivitySwitch.setChecked(type.isActive());
            } else {
                Log.e("EventTypeHandlerAdapter", "Type is null!");
            }
        }
    }

    @Override
    public int getItemCount() {
        return aEventTypes.size();
    }

    public static class TypeHandlerViewHolder extends RecyclerView.ViewHolder {
        public TextView typeName;
        public TextView description;
        public Switch typeActivitySwitch;
        public EventType type;


        public TypeHandlerViewHolder(@NonNull View itemView) {
            super(itemView);
            typeName = itemView.findViewById(R.id.event_type_handler_card_name);
            description = itemView.findViewById(R.id.event_type_handler_card_description);
            typeActivitySwitch = itemView.findViewById(R.id.event_type_handler_card_switch_button);
            itemView.findViewById(R.id.event_type_handler_card_edit_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventTypeDto eventTypeDto = new EventTypeDto();
                    eventTypeDto.setId(type.getId());
                    eventTypeDto.setName(type.getName());
                    eventTypeDto.setDescription(type.getDescription());
                    eventTypeDto.setSuggestedSubcategories(type.getSuggestedSubcategories());
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("eventType", eventTypeDto);
                    FragmentManager fragmentManager = ((FragmentActivity)itemView.getContext()).getSupportFragmentManager();

                    FragmentTransaction transaction = fragmentManager.beginTransaction();

                    EventTypeEditFragment nextFragment = new EventTypeEditFragment();
                    nextFragment.setArguments(bundle);
                    transaction.replace(R.id.navigation_fragment_container, nextFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });
            typeActivitySwitch.setOnCheckedChangeListener(((buttonView, isChecked) -> {
                type.setActive(isChecked);

                db.collection("eventTypes").document(type.getId())
                        .update("active", isChecked)
                        .addOnSuccessListener(aVoid -> {
                            // Handle success
                        })
                        .addOnFailureListener(e -> {
                            // Handle failure
                        });
            }));
        }
    }

    private void deactivateType(){

    }
}
