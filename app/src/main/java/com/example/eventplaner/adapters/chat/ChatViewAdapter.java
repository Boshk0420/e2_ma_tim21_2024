package com.example.eventplaner.adapters.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.OptIn;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.adapters.GuestViewAdapter;
import com.example.eventplaner.adapters.ItemViewAdapter;
import com.example.eventplaner.model.ChatMessage;
import com.example.eventplaner.model.mocked.ItemMock;
import com.example.eventplaner.viewmodels.FavoriteItemViewModel;
import com.google.android.material.badge.ExperimentalBadgeUtils;
import com.google.android.material.chip.Chip;

import java.util.List;

import lombok.NonNull;

public class ChatViewAdapter extends RecyclerView.Adapter<ChatViewAdapter.ViewHolder> {

    private List<ChatMessage> messages;
    private Context context;


    public ChatViewAdapter(Context context,List<ChatMessage> messages) {
        this.context = context;
        this.messages = messages;
    }

    @NonNull
    @Override
    public ChatViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_card_view, parent, false);
        return new ChatViewAdapter.ViewHolder(view);
    }


    @OptIn(markerClass = ExperimentalBadgeUtils.class)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position){
        ChatMessage chatMessage = messages.get(position);
        holder.message.setText(chatMessage.getMessage());

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView message;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            message = itemView.findViewById(R.id.chat_message);
        }
    }
}
