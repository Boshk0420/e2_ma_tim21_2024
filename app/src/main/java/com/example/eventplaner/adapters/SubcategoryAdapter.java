package com.example.eventplaner.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.EventCategory;
import com.example.eventplaner.model.EventSubcategory;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class SubcategoryAdapter extends RecyclerView.Adapter<SubcategoryAdapter.SubcategoryViewHolder> {
    private List<EventSubcategory> subcategories;
    private List<EventSubcategory> selectedSubcategories;
    private FirebaseFirestore db;

    public SubcategoryAdapter(List<EventSubcategory> subcategories) {
        this.subcategories = subcategories;
        this.selectedSubcategories = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
    }

    public SubcategoryAdapter(List<EventSubcategory> subcategories, List<String> selectedSubcategoryIds){
        this.subcategories = subcategories;
        this.selectedSubcategories = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
    }

    @NonNull
    @Override
    public SubcategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subcategory_selection_card, parent, false);
        return new SubcategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubcategoryViewHolder holder, int position) {
        EventSubcategory subcategory = subcategories.get(position);
        holder.subcategoryName.setText(subcategory.getName());
        holder.subcategoryDescription.setText(subcategory.getDescription());
        for (EventSubcategory selectedSubcategory:
             selectedSubcategories) {
            if(selectedSubcategory.getId().equals(subcategory.getId())){
                holder.checkbox.setChecked(true);
            }
        }
        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    selectedSubcategories.add(subcategory);
                }
                else {
                    removeSubcategory(subcategory);
                }
            }
        });
    }

    private void removeSubcategory(EventSubcategory subcategory){
        selectedSubcategories.removeIf(selectedSubcategory -> subcategory.getId().equals(selectedSubcategory.getId()));
    }

    @Override
    public int getItemCount() {
        return subcategories.size();
    }

    public static class SubcategoryViewHolder extends RecyclerView.ViewHolder {
        public TextView subcategoryName;
        public TextView subcategoryDescription;
        public CheckBox checkbox;

        public SubcategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            subcategoryName = itemView.findViewById(R.id.text_subcategory_name);
            subcategoryDescription = itemView.findViewById(R.id.text_subcategory_description);
            checkbox = itemView.findViewById(R.id.checkbox_subcategory);


        }

    }

    public List<EventSubcategory> getSelectedSubcategories() {
        return this.selectedSubcategories;
    }
    public void setSelectedSubcategories(List<EventSubcategory> eventSubcategories){
        this.selectedSubcategories = eventSubcategories;
    }

}
