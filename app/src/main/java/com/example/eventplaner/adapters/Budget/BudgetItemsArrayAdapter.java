package com.example.eventplaner.adapters.Budget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.eventplaner.model.BudgetCategory;
import com.example.eventplaner.model.mocked.ItemMock;

import java.util.List;

public class BudgetItemsArrayAdapter extends ArrayAdapter<ItemMock> {

    private LayoutInflater inflater;

    public BudgetItemsArrayAdapter(Context context, List<ItemMock> items){
        super(context,0,items);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        BudgetItemsArrayAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
            holder = new BudgetItemsArrayAdapter.ViewHolder();
            holder.text = convertView.findViewById(android.R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (BudgetItemsArrayAdapter.ViewHolder) convertView.getTag();
        }

        ItemMock item = getItem(position);
        if (item != null) {
            holder.text.setText(item.getName());
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView text;
    }


}
