package com.example.eventplaner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplaner.R;
import com.example.eventplaner.model.Service;

import java.util.List;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceViewHolder> {

    private List<Service> serviceList;

    public ServiceAdapter(List<Service> serviceList) {
        this.serviceList = serviceList;
    }

    public void updateServiceList(List<Service> newServiceList) {
        this.serviceList = newServiceList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View serviceView = inflater.inflate(R.layout.fragment_service_item_layout, parent, false);

        return new ServiceViewHolder(serviceView);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceViewHolder holder, int position) {
        if (serviceList != null && position < serviceList.size()) {
            Service service = serviceList.get(position);

            holder.txtName.setText(service.getName());
            holder.txtDescription.setText(service.getDescription());
            holder.txtCategory.setText(service.getCategory());
            holder.txtPrice.setText(String.valueOf(service.getPrice()));
            holder.txtDiscount.setText(String.valueOf(service.getDiscount()));
        }
    }

    @Override
    public int getItemCount() {
        return (serviceList != null) ? serviceList.size() : 0;
    }

    public static class ServiceViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName;
        public TextView txtDescription;
        public TextView txtCategory;
        public TextView txtPrice;
        public TextView txtDiscount;

        public ServiceViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.textViewServiceName);
            txtDescription = itemView.findViewById(R.id.textViewServiceDescription);
            txtCategory = itemView.findViewById(R.id.textViewServiceCategory);
            txtPrice = itemView.findViewById(R.id.textViewServicePrice);
            txtDiscount = itemView.findViewById(R.id.textViewServiceDiscount);
        }
    }
}
