package com.example.eventplaner;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.eventplaner.model.CommentReport;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnFailureListener;

public class RejectExplanationFragment extends Fragment {

    private static final String ARG_COMMENT_ID = "rejected_comment";

    private CommentReport commentId;
    private FirebaseFirestore db;

    public static RejectExplanationFragment newInstance(CommentReport commentId) {
        RejectExplanationFragment fragment = new RejectExplanationFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_COMMENT_ID, commentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reject_explanation, container, false);

        if (getArguments() != null) {
            commentId = getArguments().getParcelable(ARG_COMMENT_ID);
        }


        EditText explanationEditText = view.findViewById(R.id.explanationEditText);
        Button submitButton = view.findViewById(R.id.submitButton);

        // Initialize FirebaseFirestore instance
        db = FirebaseFirestore.getInstance();

        submitButton.setOnClickListener(v -> {
            String explanation = explanationEditText.getText().toString();

            if (explanation.isEmpty()) {
                Toast.makeText(getContext(), "Please provide an explanation.", Toast.LENGTH_SHORT).show();
                return;
            }
            if(commentId!=null)
            {
                Toast.makeText(getContext(), String.valueOf(commentId.getComment().getId()), Toast.LENGTH_SHORT).show();

                db.collection("comment_reports").document(String.valueOf(commentId.getComment().getId()))
                        .update("status", "rejected", "explanation", explanation)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(getContext(), "Comment rejected successfully!", Toast.LENGTH_SHORT).show();
                                // Close the fragment and go back to the previous one
                                getParentFragmentManager().popBackStack();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getContext(), "Failed to reject comment: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
            }
            // Update the status and explanation of the rejected comment in Firestore

        });

        return view;
    }
}
