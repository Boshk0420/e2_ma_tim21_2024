package com.example.eventplaner;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplaner.fragments.navigation.NavigationFragment;

public class LoggedInActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_in);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.logged_in_fragment_container, new NavigationFragment())
                .commit();
    }
}