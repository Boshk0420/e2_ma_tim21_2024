package com.example.eventplaner;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.eventplaner.model.CommentReport;

import java.util.List;

public class ReportedCommentsAdapter extends ArrayAdapter<CommentReport> {

    private ButtonClickListener buttonClickListener;

    public ReportedCommentsAdapter(Context context, List<CommentReport> comments) {
        super(context, 0, comments);
    }

    public void setButtonClickListener(ButtonClickListener listener) {
        this.buttonClickListener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_item_reported_comment, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.reporterNameTextView = convertView.findViewById(R.id.reporterNameTextView);
            viewHolder.commentTextView = convertView.findViewById(R.id.commentTextView);
            viewHolder.ratingTextView = convertView.findViewById(R.id.ratingTextView);
            viewHolder.reportDateTextView = convertView.findViewById(R.id.reportDateTextView);
            viewHolder.explanationTextView = convertView.findViewById(R.id.explanationTextView);
            viewHolder.statusTextView = convertView.findViewById(R.id.statusTextView);
            viewHolder.acceptButton = convertView.findViewById(R.id.acceptButton);
            viewHolder.rejectButton = convertView.findViewById(R.id.rejectButton);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Now you can set values to these TextViews if needed
        // For example:
        CommentReport comment = getItem(position);
        if (comment != null) {
            viewHolder.reporterNameTextView.setText("Reporter: " + comment.getUsername());
            viewHolder.commentTextView.setText("Comment: " + comment.getComment().getCommentText());
            viewHolder.ratingTextView.setText("Rating: " + comment.getComment().getRating());
            viewHolder.reportDateTextView.setText("Report Date: " + comment.getDate());
            //viewHolder.explanationTextView.setText("Explanation: " + comment.getReason());
            viewHolder.statusTextView.setText("Status: " + comment.getStatus());
        }

// Inside the onClick listeners for acceptButton and rejectButton
        viewHolder.acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (buttonClickListener != null) {
                    buttonClickListener.onAcceptButtonClick(position);
                    Log.d("ReportedCommentsAdapter", "Accept button clicked at position: " + position);
                }
            }
        });

        viewHolder.rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (buttonClickListener != null) {
                    buttonClickListener.onRejectButtonClick(position);
                    Log.d("ReportedCommentsAdapter", "Reject button clicked at position: " + position);
                }
            }
        });

        return convertView;
    }


    private static class ViewHolder {
        TextView reporterNameTextView;
        TextView commentTextView;
        TextView ratingTextView;
        TextView reportDateTextView;
        TextView explanationTextView;
        TextView statusTextView;
        Button acceptButton;
        Button rejectButton;
    }

    public interface ButtonClickListener {
        void onAcceptButtonClick(int position);
        void onRejectButtonClick(int position);
    }

}
