package com.example.eventplaner;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.eventplaner.fragments.commenting.CommentsFragmentWithFilters;
import com.example.eventplaner.fragments.commenting.Comments_and_reviews_for_company;

public class CompanyInfoFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_company_info, container, false);

        TextView companyName = view.findViewById(R.id.companyName);
        TextView companyDescription = view.findViewById(R.id.companyDescription);
        Button leaveCommentButton = view.findViewById(R.id.leaveCommentButton);
        Button viewCommentsButton = view.findViewById(R.id.viewCommentsButton);

        // Set company information
        companyName.setText("Firma1");
        companyDescription.setText("Opis firme");

        leaveCommentButton.setOnClickListener(v -> {
            // Navigate to LeaveCommentFragment
            getParentFragmentManager().beginTransaction()
                    .replace(R.id.logged_in_fragment_container, new Comments_and_reviews_for_company())
                    .addToBackStack(null)
                    .commit();
        });

        viewCommentsButton.setOnClickListener(v -> {
            // Navigate to CommentsFragment
            getParentFragmentManager().beginTransaction()
                    .replace(R.id.logged_in_fragment_container, new CommentsFragmentWithFilters())
                    .addToBackStack(null)
                    .commit();
        });

        return view;
    }
}
