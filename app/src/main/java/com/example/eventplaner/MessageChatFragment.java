package com.example.eventplaner;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplaner.adapters.chat.ChatViewAdapter;
import com.example.eventplaner.model.ChatMessage;

import java.util.ArrayList;
import java.util.List;


public class MessageChatFragment extends Fragment {

    private final List<ChatMessage> messageList = new ArrayList<>();



    public MessageChatFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_chat, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.chat_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new ChatViewAdapter(getContext(),messageList));

        return view;
    }
}