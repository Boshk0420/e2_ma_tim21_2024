# Е2_MA_Тim21_2024


## Members:

- Uroš Jevtić,      RA 142/2020
- Strahinja Banjanac,   RA 3/2020
- Filip Đokić,     RA 204/2020
- Marko Bošković, RA 169/2020


Uputstvo za pokretanje projekta: 
1. Otvoriti projekat u android studiju
2. Proveriti da li imate odgovarajucu verziju SDK-a instaliranu u android studiju. Ukoliko nemate prvo je potrebno to podesiti
3. Podesiti emulator
4. Kompajlirati i pokrenuti projekat klikom na dugme "Run"
